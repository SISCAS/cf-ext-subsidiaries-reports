<?php
/**
 * Subsidiaries Reports - Reports Edit
 *
 * @package Coordinator\Modules\SubsidiariesReports
 * @company Cogne Acciai Speciali s.p.a
 *
 * @var strApplication $app
 */

// check authorizations
api_checkAuthorization("subsidiaries_reports-edit_all","dashboard");
// get objects
$report_obj=new cSubsidiariesReportsReport($_REQUEST["idReport"]);
// check for exists
if($report_obj->exists()){api_alerts_add(api_text("cSubsidiariesReportsReport-alert-error"),"danger");api_redirect(api_url(["scr"=>"reports_view","idReport"=>$report_obj->id]));}
// include module report
require_once(MODULE_PATH."template.inc.php");
// set application title
$app->setTitle(api_text("reports_edit-new"));
// get form
$form=$report_obj->form_edit(["return"=>api_return(["scr"=>"reports_view"])]);
$form->addControl("button",api_text("form-fc-cancel"),api_url(["scr"=>"reports_list"]));
// build grid
$grid=new strGrid();
$grid->addRow();
$grid->addCol($form->render(),"col-xs-12");
// add content to application
$app->addContent($grid->render());
// renderize application
$app->render();
// debug
api_dump($report_obj,"report");
