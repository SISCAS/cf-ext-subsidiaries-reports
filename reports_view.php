<?php
/**
 * Subsidiaries Reports - Reports View
 *
 * @package Coordinator\Modules\SubsidiariesReports
 * @company Cogne Acciai Speciali s.p.a
 *
 * @var strApplication $app
 */

// check authorizations
api_checkAuthorization(["subsidiaries_reports-usage","subsidiaries_reports-view_all"],"dashboard");
// get objects
$report_obj=new cSubsidiariesReportsReport($_REQUEST["idReport"]);
// check objects
if(!$report_obj->exists()){api_alerts_add(api_text("cSubsidiariesReportsReport-alert-exists"),"danger");api_redirect(api_url(["scr"=>"reports_list"]));}
// check authorization
if(!$report_obj->viewable()){api_alerts_add(api_text("cSubsidiariesReportsReport-alert-denied"),"warning");api_redirect(api_url(["scr"=>"reports_list","idReport"=>$report_obj->id]));}
// deleted alert
if($report_obj->deleted){api_alerts_add(api_text("cSubsidiariesReportsReport-warning-deleted"),"warning");}
// include module report
require_once(MODULE_PATH."template.inc.php");
// set application title
$app->setTitle(api_text("reports_view",$report_obj->getLabel()));
// build description list
$left_dl=new strDescriptionList("br","dl-horizontal");
$left_dl->addElement(api_text("cSubsidiariesReportsReport-property-fkSubsidiary"),$report_obj->getSubsidiary()->name);
$left_dl->addElement(api_text("reports_view-dl-name"),api_tag("strong",$report_obj->getLabel(false)));
// build right description list
$right_dl=new strDescriptionList("br","dl-horizontal");
if($report_obj->getTemplate()->description){$right_dl->addElement(api_text("reports_view-dl-description"),nl2br($report_obj->getTemplate()->description));}
// check for tab
if(!defined(TAB)){define("TAB","entries");}  /** @todo cambiare qui e nei vari return */
/**
 * Entries
 *
 * @var strTable $entries_table
 * @var cSubsidiariesReportsReportEntry $selected_entry_obj
 */
require_once(MODULE_PATH."reports_view-entries.inc.php");
// build tabs
$tab=new strTab();
$tab->addItem(api_icon("fa-list")." ".api_text("reports_view-tab-entries"),$entries_table->render(),("entries"==TAB?"active":null));
$tab->addItem(api_icon("fa-file-text-o")." ".api_text("reports_view-tab-logs"),api_logs_table($report_obj->getLogs((!$_REQUEST["all_logs"]?10:null)))->render(),("logs"==TAB?"active":null));
// build grid
$grid=new strGrid();
$grid->addRow();
$grid->addCol($left_dl->render(),"col-xs-12 col-md-5");
$grid->addCol($right_dl->render(),"col-xs-12 col-md-7");
$grid->addRow();
$grid->addCol($tab->render(),"col-xs-12");
// add content to application
$app->addContent($grid->render());
// renderize application
$app->render();
// debug
if($selected_entry_obj){api_dump($selected_entry_obj,"selected entry");}
api_dump($report_obj,"report");
