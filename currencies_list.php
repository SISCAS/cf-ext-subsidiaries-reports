<?php
/**
 * Subsidiaries Reports - Currencies List
 *
 * @package Coordinator\Modules\SubsidiariesReports
 * @company Cogne Acciai Speciali s.p.a
 *
 * @var strApplication $app
 */

// check authorizations
api_checkAuthorization(["subsidiaries_reports-usage","subsidiaries_reports-view_all"],"dashboard");
// include module template
require_once(MODULE_PATH."template.inc.php");
// set application title
$app->setTitle(api_text("currencies_list"));
// definitions
$currencies_array=array();
// build table
$table=new strTable(api_text("currencies_list-tr-unvalued"));
$table->addHeader("&nbsp;");
$table->addHeader(api_text("cSubsidiariesReportsCurrency"),null,"100%");
$table->addHeader(api_text("cSubsidiariesReportsCurrencyExchangeRate-property-".strtolower(date("M"))),"nowrap text-right");
$table->addHeader(api_text("cSubsidiariesReportsCurrencyExchangeRate-property-avg"),"nowrap text-right");
// cycle all currencies
foreach(api_sortObjectsArray(cSubsidiariesReportsCurrency::availables(api_checkAuthorization("subsidiaries_reports-manage")),"code") as $currency_fobj){
  // make table row class
  $tr_class_array=array();
  if($currency_fobj->id==$_REQUEST["idCurrency"]){$tr_class_array[]="currentrow";}
  if($currency_fobj->deleted){$tr_class_array[]="deleted";}
  // make row
  $table->addRow(implode(" ",$tr_class_array));
  $table->addRowFieldAction(api_url(["scr"=>"currencies_view","tab"=>"exchangerates","idCurrency"=>$currency_fobj->id]),"fa-search",api_text("table-td-view"));
  $table->addRowField($currency_fobj->getLabel(),"truncate-ellipsis");
  $table->addRowField(api_number_format($currency_fobj->getYearExchangeRates(date("Y"))->{strtolower(date("M"))},4)." EUR","nowrap text-right");
  $table->addRowField(api_number_format($currency_fobj->getYearExchangeRates(date("Y"))->avg,4)." EUR","nowrap text-right");
}
// build grid
$grid=new strGrid();
$grid->addRow();
$grid->addCol($table->render(),"col-xs-12");
// add content to application
$app->addContent($grid->render());
// renderize application
$app->render();
