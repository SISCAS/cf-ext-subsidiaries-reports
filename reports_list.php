<?php
/**
 * Subsidiaries Reports - Reports List
 *
 * @package Coordinator\Modules\SubsidiariesReports
 * @company Cogne Acciai Speciali s.p.a
 *
 * @var strApplication $app
 */

// check authorizations
api_checkAuthorization(["subsidiaries_reports-usage","subsidiaries_reports-view_all"],"dashboard");
// include module template
require_once(MODULE_PATH."template.inc.php");
// set application title
$app->setTitle(api_text("reports_list"));
// definitions
$reports_array=array();
// build filter
$filter=new strFilter();
//$filter->addSearch(["year"]);
$years=[];
for($y=2020;$y<=date('Y');$y++){$years[$y]=$y;}
$filter->addItem(api_text("reports_list-filter-year"),$years,"year");
$filter->addItem(api_text("reports_list-filter-subsidiaries"),api_transcodingsFromObjects(cSubsidiariesSubsidiary::availables()),"fkSubsidiary");
$filter->addItem(api_text("reports_list-filter-template"),api_transcodingsFromObjects(cSubsidiariesReportsTemplate::availables()),"fkTemplate");
// get query where
$query_where=$filter->getQueryWhere();
// check for permissions
if(!api_checkAuthorization("subsidiaries_reports-view_all") && !api_checkAuthorization("subsidiaries_reports-edit_all")){
  $own_subsidiaries=array();
  foreach(cSubsidiariesSubsidiary::availables() as $subsidiary_fobj){
    foreach($subsidiary_fobj->getMembers() as $member_fobj){
      if($member_fobj->fkUser==$GLOBALS['session']->user->id){
        $own_subsidiaries[]=$subsidiary_fobj->id;
      }
    }
  }
  if(count($own_subsidiaries)){
    if(strlen($query_where)){$query_where.=" AND ";}
    $query_where.="( `fkSubsidiary` IN ('".implode("','",$own_subsidiaries)."') )";
  }
}
// build query
$query=new cQuery("subsidiaries_reports__reports",$query_where);
$query->addQueryOrderField("year","desc");
$query->addQueryOrderField("fkSubsidiary");
$query->addQueryOrderField("fKTemplate");
// build pagination
$pagination=new strPagination($query->getRecordsCount());
// cycle all results
foreach($query->getRecords($pagination->getQueryLimits()) as $result_f){$reports_array[$result_f->id]=new cSubsidiariesReportsReport($result_f);}
// build table
$table=new strTable(api_text("reports_list-tr-unvalued"));
$table->addHeader($filter->link(api_icon("fa-filter",api_text("filters-modal-link"),"hidden-link")),"text-center",16);
$table->addHeader(api_text("cSubsidiariesReportsReport-property-fkSubsidiary"),"nowrap");
$table->addHeader(api_text("reports_list-th-name"),null,"100%");
// cycle all reports
foreach($reports_array as $report_fobj){
  // make table row class
  $tr_class_array=array();
  if($report_fobj->id==$_REQUEST["idReport"]){$tr_class_array[]="currentrow";}
  if($report_fobj->deleted){$tr_class_array[]="deleted";}
  // make row
  $table->addRow(implode(" ",$tr_class_array));
  $table->addRowFieldAction(api_url(["scr"=>"reports_view","tab"=>"entries","idReport"=>$report_fobj->id]),"fa-search",api_text("table-td-view"));
  $table->addRowField($report_fobj->getSubsidiary()->name,"nowrap");
  $table->addRowField($report_fobj->getLabel(false),"truncate-ellipsis");
}
// build grid
$grid=new strGrid();
$grid->addRow();
$grid->addCol($filter->render(),"col-xs-12");
$grid->addRow();
$grid->addCol($table->render(),"col-xs-12");
$grid->addRow();
$grid->addCol($pagination->render(),"col-xs-12");
// add content to application
$app->addContent($grid->render());
// renderize application
$app->render();
// debug
api_dump($query,"query");
