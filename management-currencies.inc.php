<?php
/**
 * Subsidiaries Reports - Management (Currencies)
 *
 * @package Coordinator\Modules\Subsidiaries
 * @company Cogne Acciai Speciali s.p.a
 *
 * @var strApplication $app
 */

// build currencies table
$currencies_table=new strTable(api_text("management-currencies-tr-unvalued"));
$currencies_table->addHeader(api_text("cSubsidiariesReportsCurrency-property-code"),"nowrap");
$currencies_table->addHeader(api_text("cSubsidiariesReportsCurrency-property-name"),null,"100%");
$currencies_table->addHeaderAction(api_url(["scr"=>"management","tab"=>"currencies","act"=>"currency_add"]),"fa-plus",api_text("table-td-add"),null,"text-right");

// cycle all currencies
foreach(cSubsidiariesReportsCurrency::availables(true) as $currency_fobj){
  // build operation button
  $ob=new strOperationsButton();
  $ob->addElement(api_url(["scr"=>"management","tab"=>"currencies","act"=>"currency_view","idCurrency"=>$currency_fobj->id]),"fa-info-circle",api_text("table-td-view"));
  $ob->addElement(api_url(["scr"=>"management","tab"=>"currencies","act"=>"currency_edit","idCurrency"=>$currency_fobj->id]),"fa-pencil",api_text("table-td-edit"),(api_checkAuthorization("subsidiaries_reports-manage")));
  if($currency_fobj->deleted){$ob->addElement(api_url(["scr"=>"controller","act"=>"undelete","obj"=>"cSubsidiariesReportsCurrency","idCurrency"=>$currency_fobj->id,"return"=>["scr"=>"management","tab"=>"currencies"]]),"fa-trash-o",api_text("table-td-undelete"),true,api_text("cSubsidiariesReportsCurrency-confirm-undelete"));}
  else{$ob->addElement(api_url(["scr"=>"controller","act"=>"remove","obj"=>"cSubsidiariesReportsCurrency","idCurrency"=>$currency_fobj->id,"return"=>["scr"=>"management","tab"=>"currencies"]]),"fa-trash",api_text("table-td-remove"),true,api_text("cSubsidiariesReportsCurrency-confirm-remove"));}
  // make table row class
  $tr_class_array=array();
  if($currency_fobj->id==$_REQUEST["idCurrency"]){$tr_class_array[]="currentrow";}
  if($currency_fobj->deleted){$tr_class_array[]="deleted";}
  // make subsidiaries row
  $currencies_table->addRow(implode(" ",$tr_class_array));
  $currencies_table->addRowField($currency_fobj->code,"nowrap");
  $currencies_table->addRowField($currency_fobj->name,"truncate-ellipsis");
  $currencies_table->addRowField($ob->render(),"nowrap text-right");
}

// check for view action
if(ACTION=="currency_view"){
  // get selected currency
  $selected_currency_obj=new cSubsidiariesReportsCurrency($_REQUEST["idCurrency"]);
  // build description list
  $dl=new strDescriptionList("br","dl-horizontal");
  $dl->addElement(api_text("cSubsidiariesReportsCurrency-property-code"),$selected_currency_obj->code);
  $dl->addElement(api_text("cSubsidiariesReportsCurrency-property-name"),api_tag("strong",$selected_currency_obj->name));
  // build modal
  $modal=new strModal(api_text("management-currencies-modal-title"),null,"management-currencies-view");
  $modal->setBody($dl->render()."<hr>".api_logs_table($selected_currency_obj->getLogs((!$_REQUEST["all_logs"]?10:null)))->render());
  // add modal to application
  $app->addModal($modal);
  // modal scripts
  $app->addScript("$(function(){\$('#modal_management-currencies-view').modal();});");
}

// check for add or edit actions
if(in_array(ACTION,["currency_add","currency_edit"]) && api_checkAuthorization("subsidiaries_reports-manage")){
  // get selected currency
  $selected_currency_obj=new cSubsidiariesReportsCurrency($_REQUEST["idCurrency"]);
  // get form
  $form=$selected_currency_obj->form_edit(["return"=>["scr"=>"management","tab"=>"currencies"]]);
  // additional controls
  $form->addControl("button",api_text("form-fc-cancel"),"#",null,null,null,"data-dismiss='modal'");
  if($selected_currency_obj->exists()){$form->addControl("button",api_text("form-fc-remove"),api_url(["scr"=>"controller","act"=>"remove","obj"=>"cSubsidiariesReportsCurrency","idCurrency"=>$selected_currency_obj->id,"return"=>["scr"=>"management","tab"=>"currencies"]]),"btn-danger",api_text("cSubsidiariesReportsCurrency-confirm-remove"));}
  // build modal
  $modal=new strModal(api_text("management-currencies-modal-title"),null,"management-currencies-edit");
  $modal->setBody($form->render(1));
  // add modal to application
  $app->addModal($modal);
  // modal scripts
  $app->addScript("$(function(){\$('#modal_management-currencies-edit').modal({show:true,backdrop:'static',keyboard:false});});");
}
