<?php
/**
 * Subsidiaries Reports
 *
 * @package Coordinator\Modules\SubsidiariesReports
 * @company Cogne Acciai Speciali s.p.a
 * @authors Manuel Zavatta <manuel.zavatta@cogne.com>
 */
// definitions
$module_name="subsidiaries_reports";
$module_repository_url="https://bitbucket.org/SISCAS/cf-ext-subsidiaries-reports/";
$module_repository_version_url="https://bitbucket.org/SISCAS/cf-ext-subsidiaries-reports/raw/master/VERSION.txt";
$module_required_modules=array("subsidiaries");
