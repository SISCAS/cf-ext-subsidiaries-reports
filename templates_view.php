<?php
/**
 * Subsidiaries Reports - Templates View
 *
 * @package Coordinator\Modules\SubsidiariesReports
 * @company Cogne Acciai Speciali s.p.a
 *
 * @var strApplication $app
 */

// check authorizations
api_checkAuthorization("subsidiaries_reports-manage","dashboard");
// get objects
$template_obj=new cSubsidiariesReportsTemplate($_REQUEST["idTemplate"]);
// check objects
if(!$template_obj->exists()){api_alerts_add(api_text("cSubsidiariesReportsTemplate-alert-exists"),"danger");api_redirect(api_url(["scr"=>"management","tab"=>"templates"]));}
// deleted alert
if($template_obj->deleted){api_alerts_add(api_text("cSubsidiariesReportsTemplate-warning-deleted"),"warning");}
// include module template
require_once(MODULE_PATH."template.inc.php");
// set application title
$app->setTitle(api_text("templates_view",$template_obj->name));
// build description list
$dl=new strDescriptionList("br","dl-horizontal");
$dl->addElement(api_text("cSubsidiariesReportsTemplate-property-name"),api_tag("strong",$template_obj->name));
if($template_obj->description){$dl->addElement(api_text("cSubsidiariesReportsTemplate-property-description"),nl2br($template_obj->description));}
// check for tab
if(!defined(TAB)){define("TAB","entries");}
/**
 * Entries
 *
 * @var strTable $entries_table
 * @var cSubsidiariesReportsTemplateEntry $selected_entry_obj
 */
require_once(MODULE_PATH."templates_view-entries.inc.php");
// build tabs
$tab=new strTab();
$tab->addItem(api_icon("fa-list")." ".api_text("templates_view-tab-entries"),$entries_table->render(),("entries"==TAB?"active":null));
$tab->addItem(api_icon("fa-file-text-o")." ".api_text("templates_view-tab-logs"),api_logs_table($template_obj->getLogs((!$_REQUEST["all_logs"]?10:null)))->render(),("logs"==TAB?"active":null));
// build grid
$grid=new strGrid();
$grid->addRow();
$grid->addCol($dl->render(),"col-xs-12");
$grid->addRow();
$grid->addCol($tab->render(),"col-xs-12");
// add content to application
$app->addContent($grid->render());
// renderize application
$app->render();
// debug
if($selected_entry_obj){api_dump($selected_entry_obj,"selected entry");}
api_dump($template_obj,"template");
