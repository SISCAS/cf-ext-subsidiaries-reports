<?php
/**
 * Subsidiaries Reports - Currencies View
 *
 * @package Coordinator\Modules\SubsidiariesReports
 * @company Cogne Acciai Speciali s.p.a
 *
 * @var strApplication $app
 */

// check authorizations
api_checkAuthorization(["subsidiaries_reports-usage","subsidiaries_reports-view_all"],"dashboard");
// get objects
$currency_obj=new cSubsidiariesReportsCurrency($_REQUEST["idCurrency"]);
// check objects
if(!$currency_obj->exists()){api_alerts_add(api_text("cSubsidiariesReportsCurrency-alert-exists"),"danger");api_redirect(api_url(["scr"=>"currencies_list"]));}
// deleted alert
if($currency_obj->deleted){api_alerts_add(api_text("cSubsidiariesReportsCurrency-warning-deleted"),"warning");}
// include module currency
require_once(MODULE_PATH."template.inc.php");
// set application title
$app->setTitle(api_text("currencies_view",$currency_obj->getLabel()));
// build description list
$left_dl=new strDescriptionList("br","dl-horizontal");
$left_dl->addElement(api_text("cSubsidiariesReportsCurrency-property-code"),api_tag("strong",$currency_obj->code));
$left_dl->addElement(api_text("cSubsidiariesReportsCurrency"),$currency_obj->name);
// build right description list
$right_dl=new strDescriptionList("br","dl-horizontal");
/**
 * Exchange Rates
 *
 * @var strTable $exchangerate_table
 * @var cSubsidiariesReportsCurrencyExchangeRate $selected_exchangerate_obj
 */
require_once(MODULE_PATH."currencies_view-exchangerates.inc.php");
// build tabs
$tab=new strTab();
$tab->addItem(api_icon("fa-list")." ".api_text("currencies_view-tab-exchangerates"),$exchangerate_table->render(),("exchangerates"==TAB?"active":null));
$tab->addItem(api_icon("fa-file-text-o")." ".api_text("currencies_view-tab-logs"),api_logs_table($currency_obj->getLogs((!$_REQUEST["all_logs"]?10:null)))->render(),("logs"==TAB?"active":null));
// build grid
$grid=new strGrid();
$grid->addRow();
$grid->addCol($left_dl->render(),"col-xs-12 col-md-5");
$grid->addCol($right_dl->render(),"col-xs-12 col-md-7");
$grid->addRow();
$grid->addCol($tab->render(),"col-xs-12");
// add content to application
$app->addContent($grid->render());
// renderize application
$app->render();
// debug
if($selected_exchangerate_obj){api_dump($selected_exchangerate_obj,"selected entry");}
api_dump($currency_obj,"currency");
