<?php
/**
 * Subsidiaries Reports - Management
 *
 * @package Coordinator\Modules\Subsidiaries
 * @company Cogne Acciai Speciali s.p.a
 *
 * @var strApplication $app
 */

// check authorizations
api_checkAuthorization("subsidiaries_reports-manage","dashboard");
// include module template
require_once(MODULE_PATH."template.inc.php");
// set application title
$app->setTitle(api_text("management"));
// check for tab
if(!defined(TAB)){define("TAB","templates");}
// build navigation
$nav=new strNav("nav-pills");
$nav->addItem(api_text("management-nav-templates"),api_url(["scr"=>"management","tab"=>"templates"]));
$nav->addItem(api_text("management-nav-units"),api_url(["scr"=>"management","tab"=>"units"]));
$nav->addItem(api_text("management-nav-currencies"),api_url(["scr"=>"management","tab"=>"currencies"]));
// build grid
$grid=new strGrid();
$grid->addRow();
$grid->addCol($nav->render(false),"col-xs-12");
// switch tab
switch(TAB){
  /**
   * Templates
   *
   * @var strTable $templates_table
   */
  case "templates":
    // include tab
    require_once(MODULE_PATH."management-templates.inc.php");
    $grid->addRow();
    $grid->addCol($templates_table->render(),"col-xs-12");
    break;
  /**
   * Units
   *
   * @var strTable $units_table
   * @var cSubsidiariesReportsUnit $selected_unit_obj
   */
  case "units":
    // include tab
    require_once(MODULE_PATH."management-units.inc.php");
    $grid->addRow();
    $grid->addCol($units_table->render(),"col-xs-12");
    break;
  /**
   * Currencies
   *
   * @var strTable $currencies_table
   * @var cSubsidiariesReportsUnit $selected_unit_obj
   */
  case "currencies":
    // include tab
    require_once(MODULE_PATH."management-currencies.inc.php");
    $grid->addRow();
    $grid->addCol($currencies_table->render(),"col-xs-12");
    break;
}
// add content to application
$app->addContent($grid->render());
// renderize application
$app->render();
// debug
if($selected_unit_obj){api_dump($selected_unit_obj,"selected unit");}
