<?php
/**
 * Subsidiaries Reports - Templates Edit
 *
 * @package Coordinator\Modules\SubsidiariesReports
 * @company Cogne Acciai Speciali s.p.a
 *
 * @var strApplication $app
 */

// check authorizations
api_checkAuthorization("subsidiaries_reports-manage","dashboard");
// get objects
$template_obj=new cSubsidiariesReportsTemplate($_REQUEST["idTemplate"]);
// include module template
require_once(MODULE_PATH."template.inc.php");
// set application title
$app->setTitle(($template_obj->exists()?api_text("templates_edit",$template_obj->name):api_text("templates_edit-new")));
// get form
$form=$template_obj->form_edit(["return"=>api_return(["scr"=>"templates_view","tab"=>"entries"])]);
// additional controls
if($template_obj->exists()){
  $form->addControl("button",api_text("form-fc-cancel"),api_return_url(["scr"=>"templates_view","idTemplate"=>$template_obj->id]));
  if(!$template_obj->deleted){
    $form->addControl("button",api_text("form-fc-delete"),api_url(["scr"=>"controller","act"=>"delete","obj"=>"cSubsidiariesReportsTemplate","idTemplate"=>$template_obj->id]),"btn-danger",api_text("cSubsidiariesReportsTemplate-confirm-delete"));
  }else{
    $form->addControl("button",api_text("form-fc-undelete"),api_url(["scr"=>"controller","act"=>"undelete","obj"=>"cSubsidiariesReportsTemplate","idTemplate"=>$template_obj->id,"return"=>["scr"=>"templates_view","tab"=>"entries"]]),"btn-warning");
    $form->addControl("button",api_text("form-fc-remove"),api_url(["scr"=>"controller","act"=>"remove","obj"=>"cSubsidiariesReportsTemplate","idTemplate"=>$template_obj->id]),"btn-danger",api_text("cSubsidiariesReportsTemplate-confirm-remove"));
  }
}else{$form->addControl("button",api_text("form-fc-cancel"),api_url(["scr"=>"management","tab"=>"templates"]));}
// build grid
$grid=new strGrid();
$grid->addRow();
$grid->addCol($form->render(),"col-xs-12");
// add content to application
$app->addContent($grid->render());
// renderize application
$app->render();
// debug
api_dump($template_obj,"template");
