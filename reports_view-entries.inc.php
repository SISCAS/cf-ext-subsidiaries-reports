<?php
/**
 * Subsidiaries Reports - Reports View (Entries)
 *
 * @package Coordinator\Modules\SubsidiariesReports
 * @company Cogne Acciai Speciali s.p.a
 *
 * @var strApplication $app
 * @var cSubsidiariesReportsReport $report_obj
 */

// get report datas
$datas_array=$report_obj->getDatas();
//api_dump($datas_array,"datas array");
// build entries table
$entries_table=new strTable(api_text("reports_view-entries-tr-unvalued"));
$entries_table->addHeader(api_text("reports_view-entries-th-name"),"nowrap");
$entries_table->addHeader(api_text("reports_view-entries-th-unit"),"nowrap text-right");
//$entries_table->addHeader(api_text("reports_view-entries-th-total")."<br>".api_text("reports_view-entries-th-budget")." | ".api_text("reports_view-entries-th-value"),"nowrap text-center",null,null,"colspan='2'");
foreach(api_period_range($report_obj->year."01",$report_obj->year."12") as $period_f){
  $entries_table->addHeader($period_f."<br>".api_text("reports_view-entries-th-budget")." | ".api_text("reports_view-entries-th-value"),"nowrap text-center",null,null,"colspan='2'");
  $entries_table->addHeader("&nbsp;");
}
// cycle all entries
foreach($report_obj->getEntries() as $key=>$entry_fobj){
  // calculate totals
  /*$report_entry_values_totals_array=array();
  foreach($datas_array[$entry_fobj->fkTemplateEntry] as $period=>$values_fobj){
    $report_entry_values_totals_array["budget"]+=$values_fobj->budget;
    $report_entry_values_totals_array["value"]+=$values_fobj->value;
  }*/
  // make table row class
  $tr_class_array=array();
  if($entry_fobj->id==$_REQUEST["idEntry"]){$tr_class_array[]="currentrow";}     // verificare
  if($entry_fobj->deleted){$tr_class_array[]="deleted";}
  // make strong class
  if($entry_fobj->isCalculated()){$strong_class=" text-strong";}else{$strong_class=null;}
  // make row
  $entries_table->addRow(implode(" ",$tr_class_array));
  $entries_table->addRowField($entry_fobj->getName(false,true,true),"nowrap".$strong_class);
  $units=[$entry_fobj->getUnit()->getLabel(true)];
  if($entry_fobj->getUnit_2()){$units[]=$entry_fobj->getUnit_2()->getLabel(true);}
  if($entry_fobj->getUnit_3()){$units[]=$entry_fobj->getUnit_3()->getLabel(true);}
  if($entry_fobj->getUnit_4()){$units[]=$entry_fobj->getUnit_4()->getLabel(true);}
  $entries_table->addRowField(implode("<br>",$units),"nowrap text-right".$strong_class);
  // cycle all periods
  foreach(api_period_range($report_obj->year."01",$report_obj->year."12") as $period_value=>$period_label){
    if($entry_fobj->getUnit()->code=='%'){$decimals=2;}else{$decimals=0;}
    $budgets=[api_number_format($datas_array[$entry_fobj->fkTemplateEntry][$period_value]->budget,$decimals,false,true,true,"-")];
    $values=[api_number_format($datas_array[$entry_fobj->fkTemplateEntry][$period_value]->value,$decimals,false,true,true,"-")];
    if($entry_fobj->getUnit_2()){
      if($entry_fobj->getUnit_2()->code=='%'){$decimals=2;}else{$decimals=0;}
      $budgets[]=api_number_format($datas_array[$entry_fobj->fkTemplateEntry][$period_value]->budget_2,$decimals,false,true,true,"-");
      $values[]=api_number_format($datas_array[$entry_fobj->fkTemplateEntry][$period_value]->value_2,$decimals,false,true,true,"-");
    }
    if($entry_fobj->getUnit_3()){
      if($entry_fobj->getUnit_3()->code=='%'){$decimals=2;}else{$decimals=0;}
      $budgets[]=api_number_format($datas_array[$entry_fobj->fkTemplateEntry][$period_value]->budget_3,$decimals,false,true,true,"-");
      $values[]=api_number_format($datas_array[$entry_fobj->fkTemplateEntry][$period_value]->value_3,$decimals,false,true,true,"-");
    }
    if($entry_fobj->getUnit_4()){
      if($entry_fobj->getUnit_4()->code=='%'){$decimals=2;}else{$decimals=0;}
      $budgets[]=api_number_format($datas_array[$entry_fobj->fkTemplateEntry][$period_value]->budget_4,$decimals,false,true,true,"-");
      $values[]=api_number_format($datas_array[$entry_fobj->fkTemplateEntry][$period_value]->value_4,$decimals,false,true,true,"-");
    }
    $entries_table->addRowField(implode("<br>",$budgets),"nowrap text-right text-italic".$strong_class);
    $entries_table->addRowField(implode("<br>",$values),"nowrap text-right".$strong_class);
    if($entry_fobj->isFillable() && api_checkAuthorization("subsidiaries_reports-edit_all")){
      $entries_table->addRowFieldAction(api_url(["scr"=>"reports_view","tab"=>"entries","act"=>"value_edit","idReport"=>$report_obj->id,"idReportEntry"=>$entry_fobj->id,"idReportEntryValue"=>$datas_array[$entry_fobj->fkTemplateEntry][$period_value]->id,"period"=>$period_value]),"fa-edit",api_text("table-td-edit"));
    }else{
      $entries_table->addRowField("&nbsp;");
    }
  }
}

// check for fill action
if(in_array(ACTION,["fill"])){
  // check authorization
  if(!$report_obj->fillable()){api_alerts_add(api_text("cSubsidiariesReportsReport-alert-denied"),"warning");api_redirect(api_url(["scr"=>"reports_view","idReport"=>$report_obj->id]));}
  // get period
  $g_period=$_GET["period"];
  if(!$g_period){$g_period=date("Ym",strtotime("-1 month"));}
  // build form
  $form=new strForm(api_url(["mod"=>"subsidiaries_reports","scr"=>"controller","act"=>"fill","obj"=>"cSubsidiariesReportsReport","idReport"=>$report_obj->id]),"POST",null,null,"subsidiaries_reports__report-fill_form");
  // fields
  $form->addField("select","period",api_text("cSubsidiariesReportsReportEntryValue-property-period"),$g_period,api_text("cSubsidiariesReportsReportEntryValue-placeholder-period"),null,null,null,"required");
  foreach(api_period_range($report_obj->year."01",$report_obj->year."12") as $value=>$label){$form->addFieldOption($value,$label);}
  // cycle all report entries
  foreach($report_obj->getEntries(false) as $report_entry_fobj){
    /** @var cSubsidiariesReportsTemplateEntry $template_entry_obj */
    $template_entry_obj=$report_entry_fobj->getTemplateEntry();
    // check for typology
    if($template_entry_obj->typology!="fillable"){continue;}
    // load report entry value
    $report_entry_value=new cSubsidiariesReportsReportEntryValue();
    $report_entry_value->loadFromEntryPeriod($report_entry_fobj->id,$g_period);
    // check for multiple fields
    if(!$template_entry_obj->getUnit_2() && !$template_entry_obj->getUnit_3() && !$template_entry_obj->getUnit_4()){
      // add single entry field
      $form->addField("number","entries[".$report_entry_fobj->id."]",$report_entry_fobj->getName(),$report_entry_value->value,$report_entry_fobj->getName(),null,null,null,"step='0.01' required");
      $form->addFieldAddon(api_number_format($report_entry_value->budget,2,false,null,true,"-"),"prepend");
      $form->addFieldAddon(substr($report_entry_fobj->getUnit()->code,0,1),"append");
    }else{
      // add multiple entry fields
      $id=$report_entry_fobj->id;
      $name=$report_entry_fobj->getName(false,true);
      $size=12/count(array_filter([1,$template_entry_obj->getUnit_2(),$template_entry_obj->getUnit_3(),$template_entry_obj->getUnit_4()]));
      $entries=[1=>(object)[
        "value"=>$report_entry_value->value,
        "budget"=>api_number_format($report_entry_value->budget,2,false,null,true,"-"),
        "unit"=>mb_substr($template_entry_obj->getUnit()->code,0,1)
      ]];
      for($i=2;$i<=4;$i++){
        if($template_entry_obj->{"getUnit_".$i}()){
          $entries[$i]=(object)[
            "value"=>$report_entry_value->{"value_".$i},
            "budget"=>api_number_format($report_entry_value->{"budget_".$i},2,false,null,true,"-"),
            "unit"=>mb_substr($template_entry_obj->{"getUnit_".$i}()->code,0,1)
          ];
        }
      }
      $custom_field=<<<EOS
<div class="form-group" id="form_subsidiaries_reports__report-fill_form_input_entries_1[{$id}]_form_group">
  <label for="form_subsidiaries_reports__report-fill_form_input_entries[{$id}]" class="control-label col-sm-3">{$name}</label>
  <div class="col-sm-9">
    <div class="row">

EOS;
      foreach($entries as $key=>$entry):
        if($key==1){$fn="";}else{$fn="_".$key;}
        if(!$entry->value){$entry->value=0;}
        $custom_field.=<<<EOS
      <div class="col-sm-{$size}">
        <div class="input-group">
          <div class="input-group-addon">{$entry->budget}</div>
          <input type="number" class="form-control " name="entries{$fn}[{$id}]" id="form_subsidiaries_reports__report-fill_form_input_entries[{$id}]" value="{$entry->value}" step='0.01' required>
          <div class="input-group-addon">{$entry->unit}</div>
        </div><!-- input-group -->
      </div><!-- /col-sm-{$size} -->

EOS;
      endforeach;
      $custom_field.=<<<EOS
    </div><!-- /row --> 
  </div><!-- /col-sm-9 --> 
</div><!-- /form-group -->

EOS;
      $form->addCustomField($custom_field);
    }
  }
  // controls
  $form->addControl("submit",api_text("form-fc-save"));
  $form->addControl("button",api_text("form-fc-cancel"),"#",null,null,null,"data-dismiss='modal'");
  // build modal
  $modal=new strModal($report_obj->getLabel(true),null,"reports_view-entries-fill");
  $modal->setSize("large");
  $modal->setBody($form->render(1));
  // add modal to application
  $app->addModal($modal);
  // modal scripts
  $app->addScript("$(function(){\$('#modal_reports_view-entries-fill').modal({show:true,backdrop:'static',keyboard:false});});");
  // refresh page if period change
  $app->addScript("$(function(){\$('#form_subsidiaries_reports__report-fill_form_input_period').change(function(){window.location.href=window.location.href+'&period='+this.value;});});");
}

// check for value edit action
if(in_array(ACTION,["value_edit"]) && api_checkAuthorization("subsidiaries_reports-edit_all")){
  // get selected entry
  $selected_entry_obj=new cSubsidiariesReportsReportEntry($_REQUEST["idReportEntry"]);
  // get selected entry value
  $selected_entry_value_obj=new cSubsidiariesReportsReportEntryValue($_REQUEST["idReportEntryValue"]);
  // get form
  $form=$selected_entry_value_obj->form_edit(["return"=>["scr"=>"reports_view","tab"=>"entries","idReport"=>$report_obj->id]],$selected_entry_obj);
  // replace fkReportEntry
  $form->removeField("fkReportEntry");
  $form->addField("hidden","fkReportEntry",null,$selected_entry_obj->id);
  // replace period
  $form->removeField("period");
  $form->addField("hidden","period",null,$_REQUEST["period"]);
  // additional controls
  $form->addControl("button",api_text("form-fc-cancel"),"#",null,null,null,"data-dismiss='modal'");
  // build modal
  $modal=new strModal($report_obj->getLabel(true)." - ".$selected_entry_obj->getName(),null,"reports_view-entries-values");
  $modal->setBody($form->render(1));
  // add modal to application
  $app->addModal($modal);
  // modal scripts
  $app->addScript("$(function(){\$('#modal_reports_view-entries-values').modal({show:true,backdrop:'static',keyboard:false});});");
}

// check for import action
if(in_array(ACTION,["import"]) && api_checkAuthorization("subsidiaries_reports-edit_all")){
  // check for download
  if($_GET["download"]){
    // include xlsx library
    require_once(DIR."helpers/simplexlsx/php/SimpleXLSXGen.php");
    // make file name
    $filename=strtolower(preg_replace("/\s+/","_",str_replace("-","",$report_obj->getLabel(true))));
    // make template
    $header_array=array("ID","ENTRY","UNIT");
    for($y=1;$y<=12;$y++){$header_array[]=$report_obj->year.str_pad($y,2,"0",STR_PAD_LEFT);}
    $header_array[]="WARNING, DON'T MOVE OR DELETE COLUMNS AND ROWS";
    $template_array=array($header_array);
    // cycle all report entries and add into template
    foreach($report_obj->getEntries(false) as $report_entry_fobj){
      if($report_entry_fobj->getTemplateEntry()->typology!="fillable"){continue;}
      $template_array[]=array($report_entry_fobj->id,$report_entry_fobj->getName(false,true),$report_entry_fobj->getUnit()->code);
      if($report_entry_fobj->getUnit_2()){$template_array[]=array($report_entry_fobj->id,$report_entry_fobj->getName(false,true),$report_entry_fobj->getUnit_2()->code);}
      if($report_entry_fobj->getUnit_3()){$template_array[]=array($report_entry_fobj->id,$report_entry_fobj->getName(false,true),$report_entry_fobj->getUnit_3()->code);}
      if($report_entry_fobj->getUnit_4()){$template_array[]=array($report_entry_fobj->id,$report_entry_fobj->getName(false,true),$report_entry_fobj->getUnit_4()->code);}
    }
    // build xlsx from array
    $xlsx=new SimpleXLSXGen();
    $xlsx->addSheet($template_array,"Import");
    // download xlsx
    $xlsx->downloadAs($filename.".xlsx");
  }
  // build form
  $form=new strForm(api_url(["mod"=>"subsidiaries_reports","scr"=>"controller","act"=>"import","obj"=>"cSubsidiariesReportsReport","idReport"=>$report_obj->id]),"POST",null,null,"subsidiaries_reports__report-import_form");
  // fields
  $form->addField("static",null,api_text("reports_view-entries-fl-template"),api_link(api_url(["scr"=>"reports_view","tab"=>"entries","act"=>"import","download"=>true,"idReport"=>$report_obj->id]),api_text("reports_view-entries-fv-template")));
  $form->addField("radio","field",api_text("reports_view-entries-fl-field"),null,null,null,"radio-inline",null,"required");
  foreach(array("budget","value") as $field){$form->addFieldOption($field,api_text("reports_view-entries-fo-".$field));}
  $form->addField("file","file",api_text("reports_view-entries-fl-file"),null,null,null,null,null,"required");
  // controls
  $form->addControl("submit",api_text("form-fc-save"));
  $form->addControl("button",api_text("form-fc-cancel"),"#",null,null,null,"data-dismiss='modal'");
  // build modal
  $modal=new strModal($report_obj->getLabel(true),null,"reports_view-entries-fill");
  $modal->setBody($form->render(2));  /** @todo verificare lo scale factor */
  // add modal to application
  $app->addModal($modal);
  // modal scripts
  $app->addScript("$(function(){\$('#modal_reports_view-entries-fill').modal({show:true,backdrop:'static',keyboard:false});});");
  // refresh page if period change
  $app->addScript("$(function(){\$('#form_subsidiaries_reports__report-fill_form_input_period').change(function(){window.location.href=window.location.href+'&period='+this.value;});});");
}
