<?php
/**
 * Subsidiaries Reports - COGE Dossier Export
 *
 * @package Coordinator\Modules\SubsidiariesReports
 * @company Cogne Acciai Speciali s.p.a
 *
 * @var array $data
 * @var cSettings $settings
 */
// check authorizations
api_checkAuthorization(["subsidiaries_reports-usage","subsidiaries_reports-view_all"],"dashboard");
// check objects
if(!is_array($data)){api_alerts_add(api_text('crm_alert-dataNotFound'),'danger');api_redirect('?mod='.MODULE.'&scr=dossier');}
// include pdf library
require_once(DIR.'helpers/tcpdf/php/tcpdf.php');
// include math library
require_once(DIR."helpers/evalmath/php/EvalMath.php");
// extend tcpdf class
class TCPDFX extends TCPDF{
  /**
   * Custom Properties
   */
  public $border_debug='';
  public $data=array();
  public $fixedX=null;
  /*
   * Custom Methods
   */
  public function LnFixedX($h=null,$cell=false){
    $this->Ln($h,$cell);
    if($this->fixedX){$this->setX($this->fixedX);}
  }
  public function resetDefaultStyle(){
    $this->SetFont('helvetica','',9);
    $this->setTextColor(0,0,0);
  }
  public function TableCell($width,$height,$content='',$border='',$align='L',$class=null){ //,$font_style='',$font_size=10
    if(!strlen($content)||!$content){$content='-';}
    // default style
    $this->resetDefaultStyle();
    $fill=false;
    // switch class
    switch($class){
      case "title":
        $this->SetFont('helvetica','B',11);
        break;
      case "footer":
        $this->SetFont('helvetica','',7);
        break;
      case "header":
        $this->SetFont('helvetica','I',9);
        break;
      case "total":
        $this->SetFont('helvetica','B',9);
        $this->setTextColor(255,255,255);
        $this->setFillColor(119,119,119);
        $this->setFillColor(119,119,119);
        $fill=true;
        break;
      case "actual":
        $this->SetFont('helvetica','B',9);
        $this->setTextColor(255,255,255);
        $this->setFillColor(51,122,183);
        $this->setFillColor(40,96,144);
        $fill=true;
        break;
      case "budget":
        $this->SetFont('helvetica','B',9);
        $this->setTextColor(255,255,255);
        $this->setFillColor(217,83,79);
        $this->setFillColor(171,4,52);
        $fill=true;
        break;
    }
    $this->MultiCell($width,$height,$content,$border,$align,$fill,0,null,null,true,0,false,false,0,'M',true);
    $this->resetDefaultStyle();
  }
  /**
   * Print Cover
   */
  public function printCover(){
    $this->Image(DIR.'/modules/subsidiaries_reports/images/cover.png',0,0,297,210,'PNG',null,'T');
    $this->SetFont('helvetica','B',22);
    $this->SetTextColor(230);
    $this->MultiCell(120,40,"Subsidiary Reports\n\n".$this->data['month_name'].' '.$this->data['year'],$this->border_debug,'L',false,0,15,10,true,0,false,false,0,'M',true);
    $this->resetDefaultStyle();
  }
  /**
   * Print Page Header
   */
  public function printPageHeader($title,$subtitle){
    // check for subsidiary logo
    $logo_path=DIR.'modules/subsidiaries_reports/images/logo.png';     // @todo creare logo filiali nel modulo filiali con upload
    $this->border_debug='';
    $logo_size=getimagesize($logo_path);
    $logo_width=$logo_size[0];
    $logo_height=$logo_size[1];
    $logo_width_resized=round($logo_width*10/$logo_height);
    $this->Image($logo_path,10,10,'',10,'PNG','http://www.cogne.com','T');
    $this->SetFont('helvetica','B',18);
    $this->MultiCell(120,10,$title,$this->border_debug,'S',false,0,$logo_width_resized+12,10,true,0,false,false,0,'M',true);
    $this->SetFont('helvetica','',18);
    $this->MultiCell(0,10,$subtitle,$this->border_debug,'R',false,0,10,10,true,0,false,false,0,'M',true);
    $this->resetDefaultStyle();
  }
  /**
   * Print Page Footer
   */
  public function printPageFooter(){
    // set xy and fix
    $this->setXY(10,-10);
    // print title
    $this->TableCell(92,3,"CDG Management Control",$this->border_debug,'L','footer');
    $this->TableCell(92,3,date('Y-m-d'),$this->border_debug,'C','footer');
    $this->TableCell(97,3,'Page '.$this->PageNo().' of '.rtrim($this->getAliasNbPages()),$this->border_debug,'R','footer');
    $this->resetDefaultStyle();
  }
  /**
   * Print Subsidiary Monthly Table
   */
  public function printSubsidiaryMonthlyTable($startX,$startY,$width,$title,$entryTitle,$idSubsidiary,$idTemplate,$idEntries,$styles=true,$budget=true){
    //api_dump($startX.':'.$startY.' - '.$title.' ('.$idSubsidiary.','.$idTemplate.')');
    // set xy and fix
    $this->setXY($startX,$startY);
    $this->fixedX=$startX;
    // print title
    $this->TableCell($width,8,$title,$this->border_debug,'L','title');
    $this->LnFixedX();
    // calculate columns width
    $columns_width=$width-35;
    if($budget){$columns_width-=20;}
    // print headers
    $this->TableCell($columns_width,5,$entryTitle,$this->border_debug,'L','header');
    $this->TableCell(15,5,'UOM',$this->border_debug,'C','header');
    $this->TableCell(20,5,'Actual',$this->border_debug,'R','header');
    if($budget){$this->TableCell(20,5,'Budget',$this->border_debug,'R','header');}
    $this->LnFixedX();
    // print entries
    foreach($idEntries as $idEntry){
      $entry=$this->data['templates_entries'][$idTemplate][$idEntry];
      $values=$this->data['reports_entries'][$idSubsidiary][$idTemplate][$idEntry][$this->data['period']];
      //api_dump($this->GetX().':'.$this->GetY().' '.$entry->name.' '.$entry->unit.' '.$values->actual.' | '.$values->budget.' '.$entry->typology);
      // make decimals
      if($entry->unit=='%'){$decimals=2;}else{$decimals=0;}
      // check for 100% errors
      if($entry->unit=='%' && round($values->actual)==100){$values->actual=null;}
      if($entry->unit=='%' && round($values->budget)==100){$values->budget=null;}
      // print entry
      $this->TableCell($columns_width,6,$entry->name,'1','L',($styles?($entry->typology=='calculated'?'total':null):null));
      $this->TableCell(15,6,$entry->unit,'1','C',($styles?($entry->typology=='calculated'?'total':null):null));
      $this->TableCell(20,6,api_number_format($values->actual,$decimals),'1','R',($styles?($entry->typology=='calculated'?'actual':null):null));
      if($budget){$this->TableCell(20,6,api_number_format($values->budget,$decimals),'1','R',($styles?($entry->typology=='calculated'?'budget':null):null));}
      $this->LnFixedX();
    }
    $this->resetDefaultStyle();
    $this->fixedX=null;
  }
  /**
   * Print Subsidiary Monthly Table
   */
  public function printSubsidiaryYtdTable($startX,$startY,$width,$title,$entryTitle,$idSubsidiary,$idTemplate,$periods,$idEntries,$styles=true,$budget=true){
    //api_dump($startX.':'.$startY.' - '.$title.' ('.$idSubsidiary.','.$idTemplate.')');
    // calculate periods and totals
    $calculated_data=$this->calculateTotals($idTemplate,$idEntries,['ALL'=>[$idSubsidiary]],$periods);
    // set xy and fix
    $this->setXY($startX,$startY);
    $this->fixedX=$startX;
    // print title
    $this->TableCell($width,8,$title,$this->border_debug,'L','title');
    $this->LnFixedX();
    // calculate columns width
    $columns_width=$width-35;
    if($budget){$columns_width-=20;}
    // print headers
    $this->TableCell($columns_width,5,$entryTitle,$this->border_debug,'L','header');
    $this->TableCell(15,5,'UOM',$this->border_debug,'C','header');
    $this->TableCell(20,5,'Actual',$this->border_debug,'R','header');
    if($budget){$this->TableCell(20,5,'Budget',$this->border_debug,'R','header');}
    $this->LnFixedX();
    // print entries
    foreach($idEntries as $idEntry){
      $entry=$this->data['templates_entries'][$idTemplate][$idEntry];
      //$values=$this->data['reports_entries'][$idSubsidiary][$idTemplate][$idEntry][$period];
      $values=$calculated_data[$idEntry]['ALL'][$idSubsidiary];
      //api_dump($this->GetX().':'.$this->GetY().' '.$entry->name.' '.$entry->unit.' '.$values->actual.' | '.$values->budget.' '.$entry->typology);
      // make decimals
      if($entry->unit=='%'){$decimals=2;}else{$decimals=0;}
      // check for 100% errors
      if($entry->unit=='%' && round($values->actual)==100){$values->actual=null;}
      if($entry->unit=='%' && round($values->budget)==100){$values->budget=null;}
      // print entry
      $this->TableCell($columns_width,6,$entry->name,'1','L',($styles?($entry->typology=='calculated'?'total':null):null));
      $this->TableCell(15,6,$entry->unit,'1','C',($styles?($entry->typology=='calculated'?'total':null):null));
      $this->TableCell(20,6,api_number_format($values->actual,$decimals),'1','R',($styles?($entry->typology=='calculated'?'actual':null):null));
      if($budget){$this->TableCell(20,6,api_number_format($values->budget,$decimals),'1','R',($styles?($entry->typology=='calculated'?'budget':null):null));}
      $this->LnFixedX();
    }
    $this->resetDefaultStyle();
    $this->fixedX=null;
  }
  /**
   * Print Subsidiary Monthly Year Table
   */
  function printSubsidiaryMonthlyYearTable($startX,$startY,$width,$idSubsidiary,$idTemplate,$idEntry){
    //api_dump($startX.':'.$startY.' '.$idSubsidiary.' - '.$idTemplate);
    $this->fixedX=$startX;
    $this->setXY($startX,$startY);
    // calculate columns width
    $columns_width=($width/15);
    // print headers
    $this->TableCell($columns_width,6,'UOM',$this->border_debug,'C','header');
    foreach(array(($this->data['year']-2),($this->data['year']-1)) as $period_year){
      $this->TableCell($columns_width,6,'µ '.$period_year,$this->border_debug,'C','header');
    }
    foreach(api_period_range($this->data['year'].'01',$this->data['year'].'12') as $period_name){
      $this->TableCell($columns_width,6,substr($period_name,0,3)." '".substr($period_name,-2),$this->border_debug,'C','header');
    }
    $this->LnFixedX();
    // make decimals
    if($this->data['templates_entries'][$idTemplate][$idEntry]->unit=='%'){$decimals=2;}else{$decimals=0;}
    // print entries
    $this->SetFont('helvetica','',10);
    $this->TableCell($columns_width,6,$this->data['templates_entries'][$idTemplate][$idEntry]->unit,'1','C','');
    foreach(array(($this->data['year']-2),($this->data['year']-1)) as $period_code){
      $this->TableCell($columns_width,6,api_number_format($this->data['reports_entries'][$idSubsidiary][$idTemplate][$idEntry][$period_code]->actual,$decimals),'1','C','');
    }
    foreach(array_keys(api_period_range($this->data['year'].'01',$this->data['year'].'12')) as $period_code){
      $this->TableCell($columns_width,6,api_number_format($this->data['reports_entries'][$idSubsidiary][$idTemplate][$idEntry][$period_code]->actual,$decimals),'1','C','');
    }
    $this->resetDefaultStyle();
    $this->fixedX=null;
  }
  /**
   * Print Multi-Subsidiary Monthly Table
   */
  public function printMultiSubsidiaryMonthlyTable($startX,$startY,$width,$entryTitle,$periods,$idSubsidiaries,$idTemplate,$idEntries,$budget=true,$headers=true){
    // calculate periods and totals
    $calculated_data=$this->calculateTotals($idTemplate,$idEntries,[$idSubsidiaries],$periods);
    //api_dump($calculated_data,'calculated_data[]');
    // set xy and fix
    $this->setXY($startX,$startY);
    $this->fixedX=$startX;
    // calculate width
    $cols_count=count($idSubsidiaries)+1;
    if($budget){$cols_count+=1;}
    $cell_width=(($width-75)/$cols_count);
    // print headers
    if($headers){
      $this->TableCell(60,5,$entryTitle,$this->border_debug,'L','header');
      $this->TableCell(15,5,'UOM',$this->border_debug,'C','header');
      foreach($idSubsidiaries as $idSubsidiary){$this->TableCell($cell_width,5,$this->data['subsidiaries'][$idSubsidiary]->short,$this->border_debug,'R','header');}
      $this->TableCell($cell_width,5,'Total Actual',$this->border_debug,'R','header');
      if($budget){$this->TableCell($cell_width,5,'Total Budget',$this->border_debug,'R','header');}
      $this->LnFixedX();
    }
    // print entries
    foreach($idEntries as $idEntry){
      // check for null
      if(is_null($idEntry)){$this->LnFixedX();continue;}
      // get entry
      $entry=$this->data['templates_entries'][$idTemplate][$idEntry];
      // make decimals
      if($entry->unit=='%'){$decimals=2;}else{$decimals=0;}
      // print entry
      $this->TableCell(60,6,$entry->name,'1','L',($entry->typology=='calculated'?'total':null));
      $this->TableCell(15,6,$entry->unit,'1','C',($entry->typology=='calculated'?'total':null));
      foreach($idSubsidiaries as $idSubsidiary){$this->TableCell($cell_width,6,api_number_format($calculated_data[$idEntry][0][$idSubsidiary]->actual,$decimals),'1','R',($entry->typology=='calculated'?'actual':null));}
      $this->TableCell($cell_width,6,api_number_format($calculated_data[$idEntry][0]['TOTALS']->actual,$decimals),'1','R',($entry->typology=='calculated'?'actual':null));
      if($budget){$this->TableCell($cell_width,6,api_number_format($calculated_data[$idEntry][0]['TOTALS']->budget,$decimals),'1','R',($entry->typology=='calculated'?'budget':null));}
      $this->LnFixedX();
    }
    $this->resetDefaultStyle();
    $this->fixedX=null;
  }
  /**
   * Print Multi-Subsidiary Totals Monthly Table
   */
  public function printMultiSubsidiaryTotalsMonthlyTable($startX,$startY,$width,$entryTitle,$periods,$subsidiariesGroups,$idTemplate,$idEntries,$budget=true,$headers=true){
    // calculate periods and totals
    $calculated_data=$this->calculateTotals($idTemplate,$idEntries,$subsidiariesGroups,$periods);
    //api_dump($calculated_data);
    // set xy and fix
    $this->setXY($startX,$startY);
    $this->fixedX=$startX;
    // calculate width
    $cols_count=count($subsidiariesGroups)+1;
    if($budget){$cols_count+=1;}
    $cell_width=(($width-100)/$cols_count);
    // print headers
    if($headers){
      $this->TableCell(85,5,$entryTitle,$this->border_debug,'L','header');
      $this->TableCell(15,5,'UOM',$this->border_debug,'C','header');
      foreach(array_keys($subsidiariesGroups) as $group){$this->TableCell($cell_width,5,$group,$this->border_debug,'R','header');}
      $this->TableCell($cell_width,5,'Total Actual',$this->border_debug,'R','header');
      if($budget){$this->TableCell($cell_width,5,'Total Budget',$this->border_debug,'R','header');}
      $this->LnFixedX();
    }
    // print entries
    foreach($idEntries as $idEntry){
      // check for null
      if(is_null($idEntry)){
        $this->LnFixedX();
        continue;
      }
      // get entry
      $entry=$this->data['templates_entries'][$idTemplate][$idEntry];
      // make decimals
      if($entry->unit=='%'){$decimals=2;}else{$decimals=0;}
      // print entry
      $this->TableCell(85,6,$entry->name,'1','L',($entry->typology=='calculated'?'total':null));
      $this->TableCell(15,6,$entry->unit,'1','C',($entry->typology=='calculated'?'total':null));
      foreach($subsidiariesGroups as $group=>$idSubsidiaries){
        //foreach($idSubsidiaries as $idSubsidiary){$this->TableCell($cell_width,6,api_number_format($calculated_data[$idEntry][$group][$idSubsidiary]->actual,$decimals),'1','R',($entry->typology=='calculated'?'actual':null));}
        $this->TableCell($cell_width,6,api_number_format($calculated_data[$idEntry][$group]['TOTALS']->actual,$decimals),'1','R',($entry->typology=='calculated'?'actual':null));
      }
      $this->TableCell($cell_width,6,api_number_format($calculated_data[$idEntry]['TOTALS']['TOTALS']->actual,$decimals),'1','R',($entry->typology=='calculated'?'actual':null));
      if($budget){$this->TableCell($cell_width,6,api_number_format($calculated_data[$idEntry]['TOTALS']['TOTALS']->budget,$decimals),'1','R',($entry->typology=='calculated'?'budget':null));}
      $this->LnFixedX();
    }
    $this->resetDefaultStyle();
    $this->fixedX=null;
  }
  /*
   * Calculate Total for Periods and Subsidiaries groups
   *
   * @param int $idTemplate Template id [2]
   * @param array $idEntries Array of entries [18,19,...34]
   * @param array $groupsSubsidiaries Array of groups of subsidiaries ['COM'=>[1,2,...5],'PRD'=>6,7,...11]
   * @param array $periods Array of period to sum [202101,202102,...,202112]
   * @return array Array with keys [idEntry][idGroup]
   *
   * +-Group---------+---------+-------------------------Commercial-+-------------------------Productive-+----Totals-+
   * | December 2021 |   UOM   | Cogne FRA | Cogne DEU | TOTALS COM | Cogne CSB | Cogne DCS | TOTALS PRD |    TOTALS |
   * | 1.Stock Sales |  €/000  |    36.534 |    39.506 |   {76.040} |    52.534 |    89.506 |  {142.040} | {218.080} |
   * | 2.Mill Sales  |  €/000  |     1.903 |       669 |    {2.572} |     9.903 |     7.669 |   {17.572} |  {20.144} |
   * | 3.Percentage  |    %    |       {5} |       {2} |        {3} |      {18} |       {8} |       {12} |       {9} |
   * +---------------+---------+------------------------------------+------------------------------------+-----------+
   *
   *  return[entry][group]
   *
   *  return[1][COM][FRA]=36.534
   *  return[1][COM][DEU]=39.506
   *  return[1][COM][TOT]=76.040   ( 1[COM][FRA] + 1[COM][DEU] )
   *  return[1][PRD][CSB]=52.534
   *  return[1][PRD][DCS]=89.506
   *  return[1][PRD][TOT]=142.040  ( 1[PRD][CSB] + 1[PRD][DCS] )
   *  return[1][TOT][TOT]=218.080  ( 1[COM][TOT] + 1[PRD][TOT] )
   *
   *  return[2][COM][FRA]=1.903
   *  return[2][COM][DEU]=669
   *  return[2][COM][TOT]=2.572    ( 1[COM][FRA] + 1[COM][DEU] )
   *  return[2][PRD][CSB]=9.903
   *  return[2][PRD][DCS]=7.669
   *  return[2][PRD][TOT]=17.572   ( 1[PRD][CSB] + 1[PRD][DCS] )
   *  return[2][TOT][TOT]=20.144   ( 1[COM][TOT] + 1[PRD][TOT] )
   *
   *  return[3][COM][FRA]=5
   *  return[3][COM][DEU]=2
   *  return[3][COM][TOT]=3        ( 1[COM][FRA] + 1[COM][DEU] )
   *  return[3][PRD][CSB]=18
   *  return[3][PRD][DCS]=8
   *  return[3][PRD][TOT]=12       ( 1[PRD][CSB] + 1[PRD][DCS] )
   *  return[3][TOT][TOT]=9        ( 1[COM][TOT] + 1[PRD][TOT] )
   */
  public function calculateTotals($idTemplate,$idEntries,$groupsSubsidiaries,$periods){
    // definitions
    $return=array();
    $entries_array=array();
    $formulas_array=array();
    // get all entries objects
    foreach($idEntries as $idEntry){$entries_array[$idEntry]=new cSubsidiariesReportsTemplateEntry($idEntry);}
    // temporary array with key {idFormula} for formula replace
    $formulas_replace_array=array();
    // cycle all calculate entries and store formulas
    foreach($entries_array as $idEntry=>$entry_obj){
      if($entry_obj->typology!='calculated'){continue;}
      $formulas_replace_array["{".$entry_obj->id."}"]=" ( ".$entry_obj->formula." ) ";
    }
    // cycle all calculate entries and recursively replace formulas
    foreach($entries_array as $idEntry=>$entry_obj){
      if($entry_obj->typology!='calculated'){continue;}
      $formula=$entry_obj->formula;
      // replace formulas recursively (max 100 times)
      for($i=0;$i<100;$i++){$formula=str_replace(array_keys($formulas_replace_array),$formulas_replace_array,$formula);}
      $formulas_array[$idEntry]=$formula;
    }
    //api_dump($formulas_array,'formulas_array');
    // temporary array with key [$idSubsidiary][{idEntry}] for formula replace
    $formulas_data_actual=array();
    $formulas_data_budget=array();
    // cycle all un-calculated entries
    foreach($idEntries as $idEntry){
      if($entries_array[$idEntry]->typology=='calculated'){continue;}
      // temporary field for groups totals sum
      $entry_group_total_actual=0;
      $entry_group_total_budget=0;
      // cycle all groups
      foreach($groupsSubsidiaries as $group=>$idSubsidiaries){
        // temporary field for subsidiaries totals sum
        $entry_subsidiaries_total_actual=0;
        $entry_subsidiaries_total_budget=0;
        // cycle all subsidiaries
        foreach($idSubsidiaries as $idSubsidiary){
          // temporary field for periods sum
          $entry_periods_total_actual=0;
          $entry_periods_total_budget=0;
          foreach($periods as $period){
            // increment periods sum
            $entry_periods_total_actual+=$this->data['reports_entries'][$idSubsidiary][$idTemplate][$idEntry][$period]->actual;
            $entry_periods_total_budget+=$this->data['reports_entries'][$idSubsidiary][$idTemplate][$idEntry][$period]->budget;
          }
          // increment subsidiaries totals
          $entry_subsidiaries_total_actual+=$entry_periods_total_actual;
          $entry_subsidiaries_total_budget+=$entry_periods_total_budget;
          // set periods total as formula data for replace
          $formulas_data_actual[$group][$idSubsidiary]['{'.$idEntry.'}']=$entry_periods_total_actual;
          $formulas_data_budget[$group][$idSubsidiary]['{'.$idEntry.'}']=$entry_periods_total_budget;
          // build subsidiary values object
          $subsidiary_values=new stdClass();
          $subsidiary_values->actual=$entry_periods_total_actual;
          $subsidiary_values->budget=$entry_periods_total_budget;
          // add subsidiary values to return
          $return[$idEntry][$group][$idSubsidiary]=$subsidiary_values;
        }
        // increment group totals
        $entry_group_total_actual+=$entry_subsidiaries_total_actual;
        $entry_group_total_budget+=$entry_subsidiaries_total_budget;
        // set subsidiaries total as formula data for replace
        $formulas_data_actual[$group]['TOTALS']['{'.$idEntry.'}']=$entry_subsidiaries_total_actual;
        $formulas_data_budget[$group]['TOTALS']['{'.$idEntry.'}']=$entry_subsidiaries_total_budget;
        // build total values object
        $subsidiaries_total_values=new stdClass();
        $subsidiaries_total_values->actual=$entry_subsidiaries_total_actual;
        $subsidiaries_total_values->budget=$entry_subsidiaries_total_budget;
        // add totals values to return
        $return[$idEntry][$group]['TOTALS']=$subsidiaries_total_values;
      }
      // set group total as formula data for replace
      $formulas_data_actual['TOTALS']['TOTALS']['{'.$idEntry.'}']=$entry_group_total_actual;
      $formulas_data_budget['TOTALS']['TOTALS']['{'.$idEntry.'}']=$entry_group_total_budget;
      // build group total values object
      $group_total_values=new stdClass();
      $group_total_values->actual=$entry_group_total_actual;
      $group_total_values->budget=$entry_group_total_budget;
      // add totals values to return
      $return[$idEntry]['TOTALS']['TOTALS']=$group_total_values;
    }
    //api_dump($formulas_data_actual,'formulas_data_actual');
    //api_dump($formulas_data_budget,'formulas_data_budget');
    // cycle all calculated entries
    foreach($idEntries as $idEntry){
      if($entries_array[$idEntry]->typology!='calculated'){continue;}
      // add total to groups array
      $groupsSubsidiaries['TOTALS']=array();
      // cycle all groups
      foreach($groupsSubsidiaries as $group=>$idSubsidiaries){
        // add total to subsidiaries array
        $idSubsidiaries[]='TOTALS';
        // cycle all subsidiaries
        foreach($idSubsidiaries as $idSubsidiary){
          //api_dump($group.'-'.$idSubsidiary);
          // build values object
          $totals_values=new stdClass();
          $totals_values->actual=0;
          $totals_values->budget=0;
          // replace formulas items with datas
          $formula_evaluated_actual=str_replace(array_keys($formulas_data_actual[$group][$idSubsidiary]),$formulas_data_actual[$group][$idSubsidiary],$formulas_array[$idEntry]);
          $formula_evaluated_budget=str_replace(array_keys($formulas_data_budget[$group][$idSubsidiary]),$formulas_data_budget[$group][$idSubsidiary],$formulas_array[$idEntry]);
          // try to evaluate formulas
          try{$totals_values->actual=(new EvalMath())->evaluate($formula_evaluated_actual);}catch(Exception $e){$totals_values->actual='NC';}
          try{$totals_values->budget=(new EvalMath())->evaluate($formula_evaluated_budget);}catch(Exception $e){$totals_values->budget='NC';}
          // add subsidiary calculated values to return
          $return[$idEntry][$group][$idSubsidiary]=$totals_values;
        }
      }
    }
    //api_dump($return,'return');
    return $return;
  }
}
// settings
global $pdf;
global $border;
global $settings;
// create new pdf document
$pdf=new TCPDFX('L','mm','A4',true,'UTF-8',false,true);
// set properties
$pdf->SetCreator('TCPDF');
$pdf->SetAuthor($settings->owner);
$pdf->SetTitle('Subsidiaries Reports');
$pdf->SetSubject('Subsidiaries Reports '.ucfirst(api_calendar_months()[$data['month']]).' '.$data['year']);
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);
$pdf->SetFont('helvetica','',10);
$pdf->SetDefaultMonospacedFont('freemono');
$pdf->SetMargins(10,10);
$pdf->SetAutoPageBreak(false,10);
$pdf->setImageScale(1.25);
$pdf->setFontSubsetting(true);
// custom properties
$pdf->data=$data;
$pdf->border_debug='';
/**
 * Cover
 */
if(count($data['subsidiaries'])>1){
  $pdf->AddPage();
  $pdf->printCover();
}
/**
 * EX CDI Montly and YTD Report
 */
foreach(array_keys($data['subsidiaries']) as $idSubsidiary){
  // only CDI
  if($data['subsidiaries'][$idSubsidiary]->short!='EX CDI'){continue;}
  // add new page
  $pdf->AddPage();
  // header
  $pdf->printPageHeader($data['subsidiaries'][$idSubsidiary]->name,'Monthly and YTD Report');
  // economics kpi montlhy
  $pdf->printSubsidiaryMonthlyTable(10,30,130,'Economics KPI Monthly',$data['month_name'].' '.$data['year'],$idSubsidiary,4,array(
    52, // Stock Sales
    53, // Mill Sales
    54, // Total sales
    57, // Ton sold from Stock
    58, // Ton sold from Mill
    59, // Total ton sold
    60, // Ton directly from Mill
    55, // Other variable costs (-)
    56, // Personnel cost (-)
    62, // Employees
    63, // Unit Expenses
  ));
  // economics kpi montlhy
  $pdf->printSubsidiaryMonthlyTable(10,120,130,'Balance KPI Monthly',$data['month_name'].' '.$data['year'],$idSubsidiary,5,array(
    64, // Inventory
    61, // Inventory in tons
    65, // Trade receivables
  ));
  // economics kpi ytd
  $pdf->printSubsidiaryYtdTable(157,30,130,'Economics KPI YTD',$data['month_name'].' '.$data['year'],$idSubsidiary,4,array_keys(api_period_range($data['year'].'01',$data['period'])),array(
    52, // Stock Sales
    53, // Mill Sales
    54, // Total sales
    57, // Ton sold from Stock
    58, // Ton sold from Mill
    59, // Total ton sold
    60, // Ton directly from Mill
    55, // Other variable costs (-)
    56, // Personnel cost (-)
    63, // Unit Expenses
  ));
  // sales year
  $pdf->printSubsidiaryMonthlyYearTable(10,170,277,$idSubsidiary,4,59); // Total ton sold

}
/**
 * Subsidiary Monthly Report
 */
foreach(array_keys($data['subsidiaries']) as $idSubsidiary){
  // skip CDI
  if($data['subsidiaries'][$idSubsidiary]->short=='EX CDI'){continue;}
  // add new page
  $pdf->AddPage();
  // header
  $pdf->printPageHeader($data['subsidiaries'][$idSubsidiary]->name,'Monthly Report');
  // profit and loss
  $pdf->printSubsidiaryMonthlyTable(10,23,130,'Profit & Loss',$data['month_name'].' '.$data['year'],$idSubsidiary,2,array(
    19, // stock sales
    20, // mill sales
    21, // total sales
    22, // Other revenues / expense
    23, // Total Incomes
    24, // Cost of raw materials
    45, // Depreciation fund
    25, // First Margin
    26, // Other variable costs
    27, // Personnel cost
    28, // Extraodinary incomes / losses
    29, // EBITDA
    30, // Depreciation / Rent / Leasing
    31, // Interests
    32, // Exchange losses / gain
    33, // EBT
    34, // Tax
    35 // Net result
  ));
  // balance sheet
  $pdf->printSubsidiaryMonthlyTable(157,23,130,'Balance sheet',$data['month_name'].' '.$data['year'],$idSubsidiary,1,array(
    1,  // Tangible / Intangible fixed assets
    2,  // Financial fixed assets
    3,  // Fixed assets
    4,  // Inventories
    5,  // Trade receivables
    6,  // of which overdue receivables
    7,  // Trade payables
    8,  // Debts towards CAS
    9,  // Operating working capital
    10, // Other receivables
    11, // Other payables
    12, // Net working capital
    13, // Severance Indemnity
    14, // Other provisions
    15, // Net invested capital
    16, // Equity
    17, // Net Financial Position
    18  // Source of financing
  ),true,false);
  // sales year
  $pdf->printSubsidiaryMonthlyYearTable(10,146,277,$idSubsidiary,2,38); // Total ton sold
  // sales
  $pdf->printSubsidiaryMonthlyTable(10,159,130,'Sales',$data['month_name'].' '.$data['year'],$idSubsidiary,2,array(
    36, // Ton sold from Stock
    37, // Ton sold from Mill
    38, // Total ton sold
    39  // Ton directly from Mill
  ));
  // kpi
  $pdf->printSubsidiaryMonthlyTable(157,159,130,'KPI',$data['month_name'].' '.$data['year'],$idSubsidiary,2,array(
    41, // First Margin / Total Incomes
    40, // EBITDA / Total Incomes          @todo invertiti
    42, // Inventory in tons
    43  // Employees
    //43  // Unit Expenses
  ),false);
  $pdf->printPageFooter();
}
/**
 * Multi-Subsidiary Commercial Profit And Loss Montly Report
 */
if(count($data['subsidiaries'])>1){
  $pdf->AddPage();
  $pdf->printPageHeader('Commercial Subsidiaries','Profit & Loss Monthly Report');
  $pdf->printMultiSubsidiaryMonthlyTable(10,25,277,$data['month_name'].' '.$data['year'],[$data['period']],$data['subsidiaries_commercial'],2,array(
    19, // stock sales
    20, // mill sales
    21, // total sales
    22, // Other revenues / expense
    23, // Total Incomes
    24, // Cost of raw materials
    45, // Depreciation fund
    25, // First Margin
    26, // Other variable costs
    27, // Personnel cost
    28, // Extraodinary incomes / losses
    29, // EBITDA
    30, // Depreciation / Rent / Leasing
    31, // Interests
    32, // Exchange losses / gain
    33, // EBT
    34, // Tax
    35, // Net result
    null,
    36, // Ton sold from Stock
    37, // Ton sold from Mill
    38, // Total ton sold
    39, // Ton directly from Mill
    40, // EBITDA / Total Incomes
    41, // First Margin / Total Incomes
    42, // Inventory in tons
    43  // Employees
    //44  // Unit Expenses
  ));
  $pdf->printPageFooter();
}
/**
 * Multi-Subsidiary Productive Profit And Loss Montly Report
 */
if(count($data['subsidiaries'])>1){
  $pdf->AddPage();
  $pdf->printPageHeader('Productive Subsidiaries','Profit & Loss Monthly Report');
  $pdf->printMultiSubsidiaryMonthlyTable(10,25,277,$data['month_name'].' '.$data['year'],[$data['period']],$data['subsidiaries_productive'],2,array(
    19, // stock sales
    20, // mill sales
    21, // total sales
    22, // Other revenues / expense
    23, // Total Incomes
    24, // Cost of raw materials
    45, // Depreciation fund
    25, // First Margin
    26, // Other variable costs
    27, // Personnel cost
    28, // Extraodinary incomes / losses
    29, // EBITDA
    30, // Depreciation / Rent / Leasing
    31, // Interests
    32, // Exchange losses / gain
    33, // EBT
    34, // Tax
    35, // Net result
    null,
    36, // Ton sold from Stock
    37, // Ton sold from Mill
    38, // Total ton sold
    39, // Ton directly from Mill
    40, // EBITDA / Total Incomes
    41, // First Margin / Total Incomes
    42, // Inventory in tons
    43  // Employees
    //44  // Unit Expenses
  ));
  $pdf->printPageFooter();
}
/**
 * Multi-Subsidiary Totals Profit And Loss Monthly Report
 */
if(count($data['subsidiaries'])>1){
  $pdf->AddPage();
  $pdf->printPageHeader('Totals','Profit & Loss Monthly Report');
  $pdf->printMultiSubsidiaryTotalsMonthlyTable(10,25,277,$data['month_name'].' '.$data['year'],[$data['period']],array('Commercials'=>$data['subsidiaries_commercial'],'Productives'=>$data['subsidiaries_productive']),2,array(
    19, // stock sales
    20, // mill sales
    21, // total sales
    22, // Other revenues / expense
    23, // Total Incomes
    24, // Cost of raw materials
    45, // Depreciation fund
    25, // First Margin
    26, // Other variable costs
    27, // Personnel cost
    28, // Extraodinary incomes / losses
    29, // EBITDA
    30, // Depreciation / Rent / Leasing
    31, // Interests
    32, // Exchange losses / gain
    33, // EBT
    34, // Tax
    35, // Net result
    null,
    36, // Ton sold from Stock
    37, // Ton sold from Mill
    38, // Total ton sold
    39, // Ton directly from Mill
    40, // EBITDA / Total Incomes
    41, // First Margin / Total Incomes
    42, // Inventory in tons
    43  // Employees
    //44  // Unit Expenses
  ));
  $pdf->printPageFooter();
}
/**
 * Multi-Subsidiary Commercial Profit And Loss YTD Report
 */
if(count($data['subsidiaries'])>1){
  $pdf->AddPage();
  $pdf->printPageHeader('Commercial Subsidiaries','Profit & Loss YTD Report');
  $pdf->printMultiSubsidiaryMonthlyTable(10,24,277,$data['month_name'].' '.$data['year'],array_keys(api_period_range($data['year'].'01',$data['period'])),$data['subsidiaries_commercial'],2,array(
    19, // stock sales
    20, // mill sales
    21, // total sales
    22, // Other revenues / expense
    23, // Total Incomes
    24, // Cost of raw materials
    45, // Depreciation fund
    25, // First Margin
    26, // Other variable costs
    27, // Personnel cost
    28, // Extraodinary incomes / losses
    29, // EBITDA
    30, // Depreciation / Rent / Leasing
    31, // Interests
    32, // Exchange losses / gain
    33, // EBT
    34, // Tax
    35, // Net result
    null,
    36, // Ton sold from Stock
    37, // Ton sold from Mill
    38, // Total ton sold
    39, // Ton directly from Mill
    40, // EBITDA / Total Incomes
    41  // First Margin / Total Incomes
    //42, // Inventory in tons
    //43, // Employees
    //44  // Unit Expenses
  ));
  $pdf->printMultiSubsidiaryMonthlyTable(10,185,277,$data['month_name'].' '.$data['year'],[$data['period']],$data['subsidiaries_commercial'],2,array(
    42, // Inventory in tons
    43  // Employees
  ),true,false);
  $pdf->printPageFooter();
}
/**
 * Multi-Subsidiary Productive Profit And Loss YTD Report
 */
if(count($data['subsidiaries'])>1){
  $pdf->AddPage();
  $pdf->printPageHeader('Productive Subsidiaries','Profit & Loss YTD Report');
  $pdf->printMultiSubsidiaryMonthlyTable(10,25,277,$data['month_name'].' '.$data['year'],array_keys(api_period_range($data['year'].'01',$data['period'])),$data['subsidiaries_productive'],2,array(19, // stock sales
    20, // mill sales
    21, // total sales
    22, // Other revenues / expense
    23, // Total Incomes
    24, // Cost of raw materials
    45, // Depreciation fund
    25, // First Margin
    26, // Other variable costs
    27, // Personnel cost
    28, // Extraodinary incomes / losses
    29, // EBITDA
    30, // Depreciation / Rent / Leasing
    31, // Interests
    32, // Exchange losses / gain
    33, // EBT
    34, // Tax
    35, // Net result
    null,
    36, // Ton sold from Stock
    37, // Ton sold from Mill
    38, // Total ton sold
    39, // Ton directly from Mill
    40, // EBITDA / Total Incomes
    41  // First Margin / Total Incomes
    //42, // Inventory in tons
    //43, // Employees
    //44  // Unit Expenses
  ));
  $pdf->printMultiSubsidiaryMonthlyTable(10,185,277,$data['month_name'].' '.$data['year'],[$data['period']],$data['subsidiaries_productive'],2,array(
    42, // Inventory in tons
    43  // Employees
  ),true,false);
  $pdf->printPageFooter();
}
/**
 * Multi-Subsidiary Totals Profit And Loss YTD Report
 */
if(count($data['subsidiaries'])>1){
  $pdf->AddPage();
  $pdf->printPageHeader('Totals','Profit & Loss YTD Report');
  $pdf->printMultiSubsidiaryTotalsMonthlyTable(10,25,277,$data['month_name'].' '.$data['year'],array_keys(api_period_range($data['year'].'01',$data['period'])),array('Commercials'=>$data['subsidiaries_commercial'],'Productives'=>$data['subsidiaries_productive']),2,array(19, // stock sales
    20, // mill sales
    21, // total sales
    22, // Other revenues / expense
    23, // Total Incomes
    24, // Cost of raw materials
    45, // Depreciation fund
    25, // First Margin
    26, // Other variable costs
    27, // Personnel cost
    28, // Extraodinary incomes / losses
    29, // EBITDA
    30, // Depreciation / Rent / Leasing
    31, // Interests
    32, // Exchange losses / gain
    33, // EBT
    34, // Tax
    35, // Net result
    null,
    36, // Ton sold from Stock
    37, // Ton sold from Mill
    38, // Total ton sold
    39, // Ton directly from Mill
    40, // EBITDA / Total Incomes
    41  // First Margin / Total Incomes
    //42, // Inventory in tons
    //43, // Employees
    //44  // Unit Expenses
  ));
  $pdf->printMultiSubsidiaryTotalsMonthlyTable(10,185,277,$data['month_name'].' '.$data['year'],[$data['period']],array('Commercials'=>$data['subsidiaries_commercial'],'Productives'=>$data['subsidiaries_productive']),2,array(
    42, // Inventory in tons
    43  // Employees
  ),true,false);
  $pdf->printPageFooter();
}
/**
 * Multi-Subsidiary Commercial Balance Sheet Montly Report
 */
if(count($data['subsidiaries'])>1){
  $pdf->AddPage();
  $pdf->printPageHeader('Commercial Subsidiaries','Balance Sheet Monthly Report');
  $pdf->printMultiSubsidiaryMonthlyTable(10,25,277,$data['month_name'].' '.$data['year'],[$data['period']],$data['subsidiaries_commercial'],1,array(1,  // Tangible / Intangible fixed assets
    2,  // Financial fixed assets
    3,  // Fixed assets
    4,  // Inventories
    5,  // Trade receivables
    6, // of which overdue receivables
    7,  // Trade payables
    8,  // Debts towards CAS
    9,  // Operating working capital
    10, // Other receivables
    11, // Other payables
    12, // Net working capital
    13, // Severance Indemnity
    14, // Other provisions
    15, // Net invested capital
    16, // Equity
    17, // Net Financial Position
    18  // Source of financing
  ),false);
  $pdf->printPageFooter();
}
/**
 * Multi-Subsidiary Productive Balance Sheet Montly Report
 */
if(count($data['subsidiaries'])>1){
  $pdf->AddPage();
  $pdf->printPageHeader('Productive Subsidiaries','Balance Sheet Monthly Report');
  $pdf->printMultiSubsidiaryMonthlyTable(10,25,277,$data['month_name'].' '.$data['year'],[$data['period']],$data['subsidiaries_productive'],1,array(1,  // Tangible / Intangible fixed assets
    2,  // Financial fixed assets
    3,  // Fixed assets
    4,  // Inventories
    5,  // Trade receivables
    6, // of which overdue receivables
    7,  // Trade payables
    8,  // Debts towards CAS
    9,  // Operating working capital
    10, // Other receivables
    11, // Other payables
    12, // Net working capital
    13, // Severance Indemnity
    14, // Other provisions
    15, // Net invested capital
    16, // Equity
    17, // Net Financial Position
    18  // Source of financing
  ),false);
  $pdf->printPageFooter();
}
/**
 * Multi-Subsidiary Totals Balance Sheet Montly Report
 */
if(count($data['subsidiaries'])>1){
  $pdf->AddPage();
  $pdf->printPageHeader('Totals','Balance Sheet Monthly Report');
  $pdf->printMultiSubsidiaryTotalsMonthlyTable(10,25,277,$data['month_name'].' '.$data['year'],[$data['period']],array('Commercials'=>$data['subsidiaries_commercial'],'Productives'=>$data['subsidiaries_productive']),1,array(1,  // Tangible / Intangible fixed assets
    2,  // Financial fixed assets
    3,  // Fixed assets
    4,  // Inventories
    5,  // Trade receivables
    6, // of which overdue receivables
    7,  // Trade payables
    8,  // Debts towards CAS
    9,  // Operating working capital
    10, // Other receivables
    11, // Other payables
    12, // Net working capital
    13, // Severance Indemnity
    14, // Other provisions
    15, // Net invested capital
    16, // Equity
    17, // Net Financial Position
    18  // Source of financing
  ),false);
  $pdf->printPageFooter();
}
/**
 * PDF Output
 */
if(!DEBUG){$pdf->Output('dossier_'.date('YmdHis').'.pdf', 'I');}
