<?php
/**
 * Subsidiaries Reports - Management (Templates)
 *
 * @package Coordinator\Modules\Subsidiaries
 * @company Cogne Acciai Speciali s.p.a
 *
 * @var strApplication $app
 */

// build templates table
$templates_table=new strTable(api_text("management-templates-tr-unvalued"));
$templates_table->addHeader("&nbsp;");
$templates_table->addHeader(api_text("cSubsidiariesReportsTemplate-property-name"),"nowrap");
$templates_table->addHeader(api_text("cSubsidiariesReportsTemplate-property-description"),null,"100%");
$templates_table->addHeaderAction(api_url(["scr"=>"templates_edit"]),"fa-plus",api_text("table-td-add"),null,"text-right");

// cycle all templates
foreach(cSubsidiariesReportsTemplate::availables(true) as $template_fobj){
  // build operation button
  $ob=new strOperationsButton();
  $ob->addElement(api_url(["scr"=>"templates_edit","idTemplate"=>$template_fobj->id,"return"=>["scr"=>"management","tab"=>"templates"]]),"fa-pencil",api_text("table-td-edit"));
  if($template_fobj->deleted){$ob->addElement(api_url(["scr"=>"controller","act"=>"undelete","obj"=>"cSubsidiariesReportsTemplate","idTemplate"=>$template_fobj->id,"return"=>["scr"=>"management","tab"=>"templates"]]),"fa-trash-o",api_text("table-td-undelete"),true,api_text("cSubsidiariesReportsTemplate-confirm-undelete"));}
  else{$ob->addElement(api_url(["scr"=>"controller","act"=>"delete","obj"=>"cSubsidiariesReportsTemplate","idTemplate"=>$template_fobj->id,"return"=>["scr"=>"management","tab"=>"templates"]]),"fa-trash",api_text("table-td-delete"),true,api_text("cSubsidiariesReportsTemplate-confirm-delete"));}
  // make table row class
  $tr_class_array=array();
  if($template_fobj->id==$_REQUEST["idTemplate"]){$tr_class_array[]="currentrow";}
  if($template_fobj->deleted){$tr_class_array[]="deleted";}
  // make subsidiaries row
  $templates_table->addRow(implode(" ",$tr_class_array));
  $templates_table->addRowFieldAction(api_url(["scr"=>"templates_view","tab"=>"entries","idTemplate"=>$template_fobj->id]),"fa-search",api_text("table-td-view"));
  $templates_table->addRowField($template_fobj->name,"nowrap");
  $templates_table->addRowField($template_fobj->description,"truncate-ellipsis");
  $templates_table->addRowField($ob->render(),"nowrap text-right");
}
