<?php
/**
 * Subsidiaries Reports - Controller
 *
 * @package Coordinator\Modules\SubsidiariesReports
 * @company Cogne Acciai Speciali s.p.a
 */

// debug
api_dump($_REQUEST,"_REQUEST");
// check if object controller function exists
if(function_exists($_REQUEST["obj"]."_controller")){
  // call object controller function
  call_user_func($_REQUEST["obj"]."_controller",$_REQUEST["act"]);
}else{
  api_alerts_add(api_text("alert_controllerObjectNotFound",[MODULE,$_REQUEST["obj"]."_controller"]),"danger");
  api_redirect("?mod=".MODULE);
}

/**
 * Unit controller
 *
 * @param string $action Object action
 */
function cSubsidiariesReportsUnit_controller($action){
  // check authorizations
  api_checkAuthorization("subsidiaries_reports-manage","dashboard");
  // get object
  $unit_obj=new cSubsidiariesReportsUnit($_REQUEST["idUnit"]);
  api_dump($unit_obj,"Unit object");
  // check object
  if($action!="store" && !$unit_obj->exists()){api_alerts_add(api_text("cSubsidiariesReportsUnit-alert-exists"),"danger");api_redirect(api_url(["scr"=>"management","tab"=>"units"]));}
  // execution
  try{
    switch($action){
      case "store":
        $unit_obj->store($_REQUEST);
        api_alerts_add(api_text("cSubsidiariesReportsUnit-alert-stored"),"success");
        break;
      case "delete":
        $unit_obj->delete();
        api_alerts_add(api_text("cSubsidiariesReportsUnit-alert-deleted"),"warning");
        break;
      case "undelete":
        $unit_obj->undelete();
        api_alerts_add(api_text("cSubsidiariesReportsUnit-alert-undeleted"),"warning");
        break;
      case "remove":
        $unit_obj->remove();
        api_alerts_add(api_text("cSubsidiariesReportsUnit-alert-removed"),"warning");
        break;
      default:
        throw new Exception("Unit action \"".$action."\" was not defined..");
    }
    // redirect
    api_redirect(api_return_url(["scr"=>"management","tab"=>"units","idUnit"=>$unit_obj->id]));
  }catch(Exception $e){
    // dump, alert and redirect
    api_redirect_exception($e,api_url(["scr"=>"management","tab"=>"units","idUnit"=>$unit_obj->id]),"cSubsidiariesReportsUnit-alert-error");
  }
}

/**
 * Currency controller
 *
 * @param string $action Object action
 */
function cSubsidiariesReportsCurrency_controller($action){
  // check authorizations
  api_checkAuthorization("subsidiaries_reports-manage","dashboard");
  // get object
  $currency_obj=new cSubsidiariesReportsCurrency($_REQUEST["idCurrency"]);
  api_dump($currency_obj,"Currency object");
  // check object
  if($action!="store" && !$currency_obj->exists()){api_alerts_add(api_text("cSubsidiariesReportsCurrency-alert-exists"),"danger");api_redirect(api_url(["scr"=>"management","tab"=>"currencies"]));}
  // execution
  try{
    switch($action){
      case "store":
        $currency_obj->store($_REQUEST);
        api_alerts_add(api_text("cSubsidiariesReportsCurrency-alert-stored"),"success");
        break;
      case "delete":
        $currency_obj->delete();
        api_alerts_add(api_text("cSubsidiariesReportsCurrency-alert-deleted"),"warning");
        break;
      case "undelete":
        $currency_obj->undelete();
        api_alerts_add(api_text("cSubsidiariesReportsCurrency-alert-undeleted"),"warning");
        break;
      case "remove":
        $currency_obj->remove();
        api_alerts_add(api_text("cSubsidiariesReportsCurrency-alert-removed"),"warning");
        break;
      default:
        throw new Exception("Currency action \"".$action."\" was not defined..");
    }
    // redirect
    api_redirect(api_return_url(["scr"=>"management","tab"=>"currencies","idCurrency"=>$currency_obj->id]));
  }catch(Exception $e){
    // dump, alert and redirect
    api_redirect_exception($e,api_url(["scr"=>"management","tab"=>"currencies","idCurrency"=>$currency_obj->id]),"cSubsidiariesReportsCurrency-alert-error");
  }
}

/**
 * Currency Exchange Rate controller
 *
 * @param string $action Object action
 */
function cSubsidiariesReportsCurrencyExchangeRate_controller($action){
  // check authorizations
  api_checkAuthorization("subsidiaries_reports-edit_all","dashboard");
  // get object
  $exchangerate_obj=new cSubsidiariesReportsCurrencyExchangerate($_REQUEST["idCurrencyExchangerate"]);
  api_dump($exchangerate_obj,"currency exchange rate object");
  // check object
  if($action!="store" && !$exchangerate_obj->exists()){api_alerts_add(api_text("cSubsidiariesReportsCurrencyExchangeRate-alert-exists"),"danger");api_redirect(api_url(["scr"=>"currencies_list"]));}
  // execution
  try{
    switch($action){
      case "store":
        $exchangerate_obj->store($_REQUEST);
        api_alerts_add(api_text("cSubsidiariesReportsCurrencyExchangeRate-alert-stored"),"success");
        break;
      case "remove":
        $exchangerate_obj->remove();
        api_alerts_add(api_text("cSubsidiariesReportsCurrencyExchangeRate-alert-removed"),"warning");
        break;
      default:
        throw new Exception("Currency Exchange Rate action \"".$action."\" was not defined..");
    }
    // redirect
    api_redirect(api_return_url(["scr"=>"currencies_view","tab"=>"entries","idCurrency"=>$exchangerate_obj->fkCurrency,"idExchangerate"=>$exchangerate_obj->id]));
  }catch(Exception $e){
    // dump, alert and redirect
    api_redirect_exception($e,api_url(["scr"=>"currencies_view","tab"=>"entries","idCurrency"=>$exchangerate_obj->fkCurrency,"idExchangerate"=>$exchangerate_obj->id]),"cSubsidiariesReportsCurrencyExchangeRate-alert-error");
  }
}

/**
 * Template controller
 *
 * @param string $action Object action
 */
function cSubsidiariesReportsTemplate_controller($action){
  // check authorizations
  api_checkAuthorization("subsidiaries_reports-manage","dashboard");
  // get object
  $template_obj=new cSubsidiariesReportsTemplate($_REQUEST["idTemplate"]);
  api_dump($template_obj,"Template object");
  // check object
  if($action!="store" && !$template_obj->exists()){api_alerts_add(api_text("cSubsidiariesReportsTemplate-alert-exists"),"danger");api_redirect(api_url(["scr"=>"management","tab"=>"templates"]));}
  // execution
  try{
    switch($action){
      case "store":
        $template_obj->store($_REQUEST);
        api_alerts_add(api_text("cSubsidiariesReportsTemplate-alert-stored"),"success");
        break;
      case "delete":
        $template_obj->delete();
        api_alerts_add(api_text("cSubsidiariesReportsTemplate-alert-deleted"),"warning");
        break;
      case "undelete":
        $template_obj->undelete();
        api_alerts_add(api_text("cSubsidiariesReportsTemplate-alert-undeleted"),"warning");
        break;
      case "remove":
        $template_obj->remove();
        api_alerts_add(api_text("cSubsidiariesReportsTemplate-alert-removed"),"warning");
        break;
      default:
        throw new Exception("Template action \"".$action."\" was not defined..");
    }
    // redirect
    api_redirect(api_return_url(["scr"=>"management","tab"=>"templates","idTemplate"=>$template_obj->id]));
  }catch(Exception $e){
    // dump, alert and redirect
    api_redirect_exception($e,api_url(["scr"=>"management","tab"=>"templates","idTemplate"=>$template_obj->id]),"cSubsidiariesReportsTemplate-alert-error");
  }
}

/**
 * Template Entry controller
 *
 * @param string $action Object action
 */
function cSubsidiariesReportsTemplateEntry_controller($action){
  // check authorizations
  api_checkAuthorization("subsidiaries_reports-manage","dashboard");
  // get object
  $entry_obj=new cSubsidiariesReportsTemplateEntry($_REQUEST["idEntry"]);
  api_dump($entry_obj,"template entry object");
  // check object
  if($action!="store" && !$entry_obj->exists()){api_alerts_add(api_text("cSubsidiariesReportsTemplateEntry-alert-exists"),"danger");api_redirect(api_url(["scr"=>"templates_list"]));}
  // execution
  try{
    switch($action){
      case "store":
        $entry_obj->store($_REQUEST);
        api_alerts_add(api_text("cSubsidiariesReportsTemplateEntry-alert-stored"),"success");
        break;
      case "move":
        $entry_obj->move($_REQUEST["direction"]);
        api_alerts_add(api_text("cSubsidiariesReportsTemplateEntry-alert-moved"),"success");
        break;
      case "delete":
        $entry_obj->delete();
        api_alerts_add(api_text("cSubsidiariesReportsTemplateEntry-alert-deleted"),"warning");
        break;
      case "undelete":
        $entry_obj->undelete();
        api_alerts_add(api_text("cSubsidiariesReportsTemplateEntry-alert-undeleted"),"warning");
        break;
      case "remove":
        $entry_obj->remove();
        api_alerts_add(api_text("cSubsidiariesReportsTemplateEntry-alert-removed"),"warning");
        break;
      default:
        throw new Exception("Template entry action \"".$action."\" was not defined..");
    }
    // redirect
    api_redirect(api_return_url(["scr"=>"templates_view","tab"=>"entries","idTemplate"=>$entry_obj->fkTemplate,"idEntry"=>$entry_obj->id]));
  }catch(Exception $e){
    // dump, alert and redirect
    api_redirect_exception($e,api_url(["scr"=>"templates_view","tab"=>"entries","idTemplate"=>$entry_obj->fkTemplate,"idEntry"=>$entry_obj->id]),"cSubsidiariesReportsTemplateEntry-alert-error");
  }
}

/**
 * Report controller
 *
 * @param string $action Object action
 */
function cSubsidiariesReportsReport_controller($action){
  // get object
  $report_obj=new cSubsidiariesReportsReport($_REQUEST["idReport"]);
  api_dump($report_obj,"Report object");
  // check object
  if($action!="store" && !$report_obj->exists()){api_alerts_add(api_text("cSubsidiariesReportsReport-alert-exists"),"danger");api_redirect(api_url(["scr"=>"reports_list"]));}
  // execution
  try{
    switch($action){
      case "store":
        // check authorizations
        api_checkAuthorization("subsidiaries_reports-edit_all","dashboard");
        // check for exists
        if($report_obj->exists()){api_alerts_add(api_text("cSubsidiariesReportsReport-alert-error"),"danger");api_redirect(api_url(["scr"=>"reports_view","idReport"=>$report_obj->id]));}
        // insert
        $report_obj->store($_REQUEST);
        // check for success
        if($report_obj->exists()){
          api_alerts_add(api_text("cSubsidiariesReportsReport-alert-stored"),"success");
          // cycle all template entries
          foreach(api_sortObjectsArray(cSubsidiariesReportsTemplateEntry::availables(false,["fkTemplate"=>$report_obj->fkTemplate]),"order",false) as $template_entry_fobj){
            // build and store report entry
            $report_entry_obj=new cSubsidiariesReportsReportEntry();
            $report_entry_obj->store(["fkReport"=>$report_obj->id,"fkTemplateEntry"=>$template_entry_fobj->id]);
          }
        }
        break;
      case "remove":
        // check authorizations
        api_checkAuthorization("subsidiaries_reports-manage","dashboard");
        $report_obj->remove();
        api_alerts_add(api_text("cSubsidiariesReportsReport-alert-removed"),"warning");
        break;
      case "import":
        // check authorizations
        api_checkAuthorization("subsidiaries_reports-edit_all","dashboard");
        $report_obj->import($_REQUEST,$_FILES["file"]);
        api_alerts_add(api_text("cSubsidiariesReportsReport-alert-imported"),"success");
        break;
      case "fill":
        // check authorizations
        if(!$report_obj->fillable()){api_alerts_add(api_text("cSubsidiariesReportsReport-alert-denied"),"warning");api_redirect(api_url(["scr"=>"reports_view","idReport"=>$report_obj->id]));}
        $report_obj->fill($_REQUEST);
        api_alerts_add(api_text("cSubsidiariesReportsReport-alert-filled"),"success");
        break;
      default:
        throw new Exception("Report action \"".$action."\" was not defined..");
    }
    // redirect
    api_redirect(api_return_url(["scr"=>"reports_view","idReport"=>$report_obj->id]));
  }catch(Exception $e){
    // dump, alert and redirect
    api_redirect_exception($e,api_url(["scr"=>"reports_list","idReport"=>$report_obj->id]),"cSubsidiariesReportsReport-alert-error");
  }
}

/**
 * Report Entry Value controller
 *
 * @param string $action Object action
 */
function cSubsidiariesReportsReportEntryValue_controller($action){
  // check authorizations
  api_checkAuthorization("subsidiaries_reports-edit_all","dashboard");
  // get object
  $report_entry_value_obj=new cSubsidiariesReportsReportEntryValue($_REQUEST["idReportEntryValue"]);
  api_dump($report_entry_value_obj,"report entry value object");
  // check object
  if($action!="store" && !$report_entry_value_obj->exists()){api_alerts_add(api_text("cSubsidiariesReportsReportEntryValue-alert-exists"),"danger");api_redirect(api_url(["scr"=>"reports_view","tab"=>"entries","idReport"=>$report_entry_value_obj->getReportEntry()->getReport()->id,"idReportEntry"=>$report_entry_value_obj->getReportEntry()->id]));}
  // execution
  try{
    switch($action){
      case "store":
        $report_entry_value_obj->store($_REQUEST);
        api_alerts_add(api_text("cSubsidiariesReportsReportEntryValue-alert-stored"),"success");
        break;
      default:
        throw new Exception("Report entry value action \"".$action."\" was not defined..");
    }
    // redirect
    api_redirect(api_return_url(["scr"=>"reports_view","tab"=>"entries","idReport"=>$report_entry_value_obj->getReportEntry()->getReport()->id,"idReportEntry"=>$report_entry_value_obj->getReportEntry()->id,"idReportEntryValue"=>$report_entry_value_obj->id]));
  }catch(Exception $e){
    // dump, alert and redirect
    api_redirect_exception($e,api_url(["scr"=>"reports_view","tab"=>"entries","idReport"=>$report_entry_value_obj->getReportEntry()->getReport()->id,"idReportEntry"=>$report_entry_value_obj->getReportEntry()->id,"idReportEntryValue"=>$report_entry_value_obj->id]),"cSubsidiariesReportsReportEntryValue-alert-error");
  }
}
