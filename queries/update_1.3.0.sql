--
-- Subsidiaries Reports - Update (1.3.0)
--
-- @package Coordinator\Modules\SubsidiariesReports
-- @company Cogne Acciai Speciali s.p.a
--

-- --------------------------------------------------------

SET FOREIGN_KEY_CHECKS = 0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";

-- --------------------------------------------------------

--
-- Alter structure for table `subsidiaries_reports__templates__entries`
--

alter table subsidiaries_reports__templates__entries
	add fkUnit_2 int UNSIGNED default NULL null after fkUnit,
	add fkUnit_3 int UNSIGNED default NULL null after fkUnit_2,
	add fkUnit_4 int UNSIGNED default NULL null after fkUnit_3;

-- --------------------------------------------------------

--
-- Alter structure for table `subsidiaries_reports__templates__entries`
--

alter table subsidiaries_reports__reports__entries__values
	add budget_2 DOUBLE default NULL null after budget,
	add budget_3 DOUBLE default NULL null after budget_2,
	add budget_4 DOUBLE default NULL null after budget_3,
	add value_2 DOUBLE default NULL null after value,
	add value_3 DOUBLE default NULL null after value_2,
	add value_4 DOUBLE default NULL null after value_3;

-- --------------------------------------------------------

SET FOREIGN_KEY_CHECKS = 1;

-- --------------------------------------------------------
