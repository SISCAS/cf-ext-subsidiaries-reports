--
-- Subsidiaries Reports - Update (1.0.13)
--
-- @package Coordinator\Modules\SubsidiariesReports
-- @company Cogne Acciai Speciali s.p.a
--

-- --------------------------------------------------------

SET FOREIGN_KEY_CHECKS = 0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";

-- --------------------------------------------------------

--
-- Authorizations
--

INSERT IGNORE INTO `framework__modules__authorizations` (`id`,`fkModule`,`order`) VALUES
	('subsidiaries_reports-view_all','subsidiaries_reports',3),
	('subsidiaries_reports-edit_all','subsidiaries_reports',4);

-- --------------------------------------------------------

SET FOREIGN_KEY_CHECKS = 1;

-- --------------------------------------------------------
