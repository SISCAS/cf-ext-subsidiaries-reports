--
-- Subsidiaries Reports - Setup (1.0.0)
--
-- @package Coordinator\Modules\SubsidiariesReports
-- @company Cogne Acciai Speciali s.p.a
--

-- --------------------------------------------------------

SET FOREIGN_KEY_CHECKS = 0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";

-- --------------------------------------------------------

--
-- Table structure for table `subsidiaries_reports__units`
--

CREATE TABLE IF NOT EXISTS `subsidiaries_reports__units` (
	`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
	`deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
	`code` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
	`name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `subsidiaries_reports__units__logs`
--

CREATE TABLE IF NOT EXISTS `subsidiaries_reports__units__logs` (
	`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
	`fkObject` int(11) unsigned NOT NULL,
	`fkUser` int(11) unsigned DEFAULT NULL,
	`timestamp` int(11) unsigned NOT NULL,
	`alert` tinyint(1) unsigned NOT NULL DEFAULT '0',
	`event` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
	`properties_json` text COLLATE utf8_unicode_ci,
	PRIMARY KEY (`id`),
	KEY `fkObject` (`fkObject`),
	CONSTRAINT `subsidiaries_reports__units__logs_ibfk_1` FOREIGN KEY (`fkObject`) REFERENCES `subsidiaries_reports__units` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `subsidiaries_reports__templates`
--

CREATE TABLE IF NOT EXISTS `subsidiaries_reports__templates` (
	`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
	`deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
	`name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
	`description` text COLLATE utf8_unicode_ci,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


--
-- Table structure for table `subsidiaries_reports__templates__logs`
--

CREATE TABLE IF NOT EXISTS `subsidiaries_reports__templates__logs` (
	`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
	`fkObject` int(11) unsigned NOT NULL,
	`fkUser` int(11) unsigned DEFAULT NULL,
	`timestamp` int(11) unsigned NOT NULL,
	`alert` tinyint(1) unsigned NOT NULL DEFAULT '0',
	`event` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
	`properties_json` text COLLATE utf8_unicode_ci,
	PRIMARY KEY (`id`),
	KEY `fkObject` (`fkObject`),
	CONSTRAINT `subsidiaries_reports__templates__logs_ibfk_1` FOREIGN KEY (`fkObject`) REFERENCES `subsidiaries_reports__templates` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `subsidiaries_reports__templates__entries`
--

CREATE TABLE IF NOT EXISTS `subsidiaries_reports__templates__entries` (
	`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
	`deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
	`order` int(11) unsigned NOT NULL,
	`typology` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
	`fkTemplate` int(11) unsigned NOT NULL,
	`fkUnit` int(11) unsigned NOT NULL,
	`name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
	`description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
	`formula` text COLLATE utf8_unicode_ci,
	PRIMARY KEY (`id`),
	KEY `fkTemplate` (`fkTemplate`),
	KEY `fkUnit` (`fkUnit`),
	CONSTRAINT `subsidiaries_reports__templates__entries_ibfk_1` FOREIGN KEY (`fkTemplate`) REFERENCES `subsidiaries_reports__templates` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT `subsidiaries_reports__templates__entries_ibfk_2` FOREIGN KEY (`fkUnit`) REFERENCES `subsidiaries_reports__units` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `subsidiaries_reports__reports`
--

CREATE TABLE IF NOT EXISTS `subsidiaries_reports__reports` (
	`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
	`deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
	`fkSubsidiary` int(11) unsigned NOT NULL,
	`fkTemplate` int(11) unsigned NOT NULL,
	`year` int(11) unsigned NOT NULL,
	`lock` int(11) unsigned NULL,
	PRIMARY KEY (`id`),
	UNIQUE KEY `fkSubsidiary_fkTemplate_year` (`fkSubsidiary`,`fkTemplate`,`year`),
	KEY `fkSubsidiary` (`fkSubsidiary`),
	KEY `fkTemplate` (`fkTemplate`),
	CONSTRAINT `subsidiaries_reports__reports_ibfk_1` FOREIGN KEY (`fkSubsidiary`) REFERENCES `subsidiaries__subsidiaries` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT `subsidiaries_reports__reports_ibfk_2` FOREIGN KEY (`fkTemplate`) REFERENCES `subsidiaries_reports__templates` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `subsidiaries_reports__reports__logs`
--

CREATE TABLE IF NOT EXISTS `subsidiaries_reports__reports__logs` (
	`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
	`fkObject` int(11) unsigned NOT NULL,
	`fkUser` int(11) unsigned DEFAULT NULL,
	`timestamp` int(11) unsigned NOT NULL,
	`alert` tinyint(1) unsigned NOT NULL DEFAULT '0',
	`event` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
	`properties_json` text COLLATE utf8_unicode_ci,
	PRIMARY KEY (`id`),
	KEY `fkObject` (`fkObject`),
	CONSTRAINT `subsidiaries_reports__reports__logs_ibfk_1` FOREIGN KEY (`fkObject`) REFERENCES `subsidiaries_reports__reports` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `subsidiaries_reports__reports__entries`
--

CREATE TABLE IF NOT EXISTS `subsidiaries_reports__reports__entries` (
	`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
	`deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
	`fkReport` int(11) unsigned NOT NULL,
	`fkTemplateEntry` int(11) unsigned NOT NULL,
	PRIMARY KEY (`id`),
	UNIQUE KEY `fkReport_fkTemplateEntry` (`fkReport`,`fkTemplateEntry`),
	KEY `fkReport` (`fkReport`),
	KEY `fkTemplateEntry` (`fkTemplateEntry`),
	CONSTRAINT `subsidiaries_reports__reports__entries_ibfk_1` FOREIGN KEY (`fkReport`) REFERENCES `subsidiaries_reports__reports` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT `subsidiaries_reports__reports__entries_ibfk_2` FOREIGN KEY (`fkTemplateEntry`) REFERENCES `subsidiaries_reports__templates__entries` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `subsidiaries_reports__reports__entries__values`
--

CREATE TABLE IF NOT EXISTS `subsidiaries_reports__reports__entries__values` (
	`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
	`deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
	`fkReportEntry` int(11) unsigned NOT NULL,
	`period` int(11) unsigned NOT NULL,
	`budget` double DEFAULT NULL,
	`value` double DEFAULT NULL,
	PRIMARY KEY (`id`),
	UNIQUE KEY `fkReportEntry_period` (`fkReportEntry`,`period`),
	KEY `fkReportEntry` (`fkReportEntry`),
	CONSTRAINT `subsidiaries_reports__reports__entries__values_ibfk_1` FOREIGN KEY (`fkReportEntry`) REFERENCES `subsidiaries_reports__reports__entries` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Authorizations
--

INSERT IGNORE INTO `framework__modules__authorizations` (`id`,`fkModule`,`order`) VALUES
('subsidiaries_reports-manage','subsidiaries_reports',1),
('subsidiaries_reports-usage','subsidiaries_reports',2);

-- --------------------------------------------------------

SET FOREIGN_KEY_CHECKS = 1;

-- --------------------------------------------------------
