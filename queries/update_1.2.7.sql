--
-- Subsidiaries Reports - Update (1.2.7)
--
-- @package Coordinator\Modules\SubsidiariesReports
-- @company Cogne Acciai Speciali s.p.a
--

-- --------------------------------------------------------

SET FOREIGN_KEY_CHECKS = 0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";

-- --------------------------------------------------------

--
-- Alter structure for table `subsidiaries_reports__currencies__exchangerates`
--

ALTER TABLE `subsidiaries_reports__currencies__exchangerates`
	CHANGE COLUMN `value` `avg` DOUBLE UNSIGNED NULL DEFAULT NULL AFTER `year`,
	ADD COLUMN `jan` DOUBLE UNSIGNED NULL DEFAULT NULL AFTER `avg`,
	ADD COLUMN `feb` DOUBLE UNSIGNED NULL DEFAULT NULL AFTER `jan`,
	ADD COLUMN `mar` DOUBLE UNSIGNED NULL DEFAULT NULL AFTER `feb`,
	ADD COLUMN `apr` DOUBLE UNSIGNED NULL DEFAULT NULL AFTER `mar`,
	ADD COLUMN `may` DOUBLE UNSIGNED NULL DEFAULT NULL AFTER `apr`,
	ADD COLUMN `jun` DOUBLE UNSIGNED NULL DEFAULT NULL AFTER `may`,
	ADD COLUMN `jul` DOUBLE UNSIGNED NULL DEFAULT NULL AFTER `jun`,
	ADD COLUMN `aug` DOUBLE UNSIGNED NULL DEFAULT NULL AFTER `jul`,
	ADD COLUMN `sep` DOUBLE UNSIGNED NULL DEFAULT NULL AFTER `aug`,
	ADD COLUMN `oct` DOUBLE UNSIGNED NULL DEFAULT NULL AFTER `sep`,
	ADD COLUMN `nov` DOUBLE UNSIGNED NULL DEFAULT NULL AFTER `oct`,
	ADD COLUMN `dec` DOUBLE UNSIGNED NULL DEFAULT NULL AFTER `nov`
;

-- --------------------------------------------------------

SET FOREIGN_KEY_CHECKS = 1;

-- --------------------------------------------------------
