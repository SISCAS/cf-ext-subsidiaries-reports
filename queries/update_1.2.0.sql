--
-- Subsidiaries Reports - Update (1.2.0)
--
-- @package Coordinator\Modules\SubsidiariesReports
-- @company Cogne Acciai Speciali s.p.a
--

-- --------------------------------------------------------

SET FOREIGN_KEY_CHECKS = 0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";

-- --------------------------------------------------------

--
-- Table structure for table `subsidiaries_reports__currencies`
--

CREATE TABLE `subsidiaries_reports__currencies` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`deleted` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
	`code` VARCHAR(8) NOT NULL COLLATE 'utf8_unicode_ci',
	`name` VARCHAR(32) NOT NULL COLLATE 'utf8_unicode_ci',
	PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `subsidiaries_reports__currencies__logs`
--

CREATE TABLE `subsidiaries_reports__currencies__logs` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`fkObject` INT(11) UNSIGNED NOT NULL,
	`fkUser` INT(11) UNSIGNED NULL DEFAULT NULL,
	`timestamp` INT(11) UNSIGNED NOT NULL,
	`alert` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
	`event` VARCHAR(32) NOT NULL COLLATE 'utf8_unicode_ci',
	`properties_json` TEXT NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
	PRIMARY KEY (`id`) USING BTREE,
	INDEX `fkObject` (`fkObject`) USING BTREE,
	CONSTRAINT `subsidiaries_reports__currencies__logs_ibfk_1` FOREIGN KEY (`fkObject`) REFERENCES `subsidiaries_reports__currencies` (`id`) ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `subsidiaries_reports__currencies__exchangerates`
--

CREATE TABLE `subsidiaries_reports__currencies__exchangerates` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`deleted` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
	`fkCurrency` INT(11) UNSIGNED NOT NULL,
	`year` INT(11) UNSIGNED NOT NULL,
	`value` DOUBLE NULL DEFAULT NULL,
	PRIMARY KEY (`id`) USING BTREE,
	UNIQUE INDEX `fkCurrency_year` (`fkCurrency`, `year`) USING BTREE,
	INDEX `fkCurrency` (`fkCurrency`) USING BTREE,
	CONSTRAINT `subsidiaries_reports__currencies__exchangerates_ibfk_1` FOREIGN KEY (`fkCurrency`) REFERENCES `subsidiaries_reports__currencies` (`id`) ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

SET FOREIGN_KEY_CHECKS = 1;

-- --------------------------------------------------------
