<?php
/**
 * Subsidiaries Reports - Currencies View (Exchange Rates)
 *
 * @package Coordinator\Modules\SubsidiariesReports
 * @company Cogne Acciai Speciali s.p.a
 *
 * @var strApplication $app
 * @var cSubsidiariesReportsCurrency $currency_obj
 */

// build exchange rates table
$exchangerate_table=new strTable(api_text("currencies_view-exchangerates-tr-unvalued"));
$exchangerate_table->addHeader(api_text("cSubsidiariesReportsCurrencyExchangeRate-property-year"),"nowrap");
$exchangerate_table->addHeader(api_text("cSubsidiariesReportsCurrencyExchangeRate-property-avg"),"text-right nowrap");
$exchangerate_table->addHeader(api_text("cSubsidiariesReportsCurrencyExchangeRate-property-jan"),"text-right nowrap");
$exchangerate_table->addHeader(api_text("cSubsidiariesReportsCurrencyExchangeRate-property-feb"),"text-right nowrap");
$exchangerate_table->addHeader(api_text("cSubsidiariesReportsCurrencyExchangeRate-property-mar"),"text-right nowrap");
$exchangerate_table->addHeader(api_text("cSubsidiariesReportsCurrencyExchangeRate-property-apr"),"text-right nowrap");
$exchangerate_table->addHeader(api_text("cSubsidiariesReportsCurrencyExchangeRate-property-may"),"text-right nowrap");
$exchangerate_table->addHeader(api_text("cSubsidiariesReportsCurrencyExchangeRate-property-jun"),"text-right nowrap");
$exchangerate_table->addHeader(api_text("cSubsidiariesReportsCurrencyExchangeRate-property-jul"),"text-right nowrap");
$exchangerate_table->addHeader(api_text("cSubsidiariesReportsCurrencyExchangeRate-property-aug"),"text-right nowrap");
$exchangerate_table->addHeader(api_text("cSubsidiariesReportsCurrencyExchangeRate-property-sep"),"text-right nowrap");
$exchangerate_table->addHeader(api_text("cSubsidiariesReportsCurrencyExchangeRate-property-oct"),"text-right nowrap");
$exchangerate_table->addHeader(api_text("cSubsidiariesReportsCurrencyExchangeRate-property-nov"),"text-right nowrap");
$exchangerate_table->addHeader(api_text("cSubsidiariesReportsCurrencyExchangeRate-property-dec"),"text-right nowrap");
if(api_checkAuthorization("subsidiaries_reports-edit_all")){$exchangerate_table->addHeader("&nbsp;");}

// cycle all exchange rates
foreach($currency_obj->getExchangeRates(api_checkAuthorization("subsidiaries_reports-edit_all")) as $exchangerate_fobj){
  if(api_checkAuthorization("subsidiaries_reports-edit_all")){
    // build operation button
    $ob=new strOperationsButton();
    $ob->addElement(api_url(["scr"=>"currencies_view","tab"=>"exchangerates","act"=>"exchangerate_edit","idCurrency"=>$currency_obj->id,"idCurrencyExchangerate"=>$exchangerate_fobj->id]),"fa-pencil",api_text("table-td-edit"));
    $ob->addElement(api_url(["scr"=>"controller","act"=>"remove","obj"=>"cSubsidiariesReportsCurrencyExchangeRate","idCurrency"=>$currency_obj->id,"idCurrencyExchangerate"=>$exchangerate_fobj->id,"return"=>["scr"=>"currencies_view","tab"=>"exchangerates","idCurrency"=>$currency_obj->id]]),"fa-trash",api_text("table-td-remove"),true,api_text("cSubsidiariesReportsCurrencyExchangeRate-confirm-remove"));
  }
  // make table row class
  $tr_class_array=array();
  if($exchangerate_fobj->id==$_REQUEST["idCurrencyExchangerate"]){$tr_class_array[]="currentrow";}
  if($exchangerate_fobj->deleted){$tr_class_array[]="deleted";}
  // make row
  $exchangerate_table->addRow(implode(" ",$tr_class_array));
  $exchangerate_table->addRowField($exchangerate_fobj->year." EUR","nowrap");
  $exchangerate_table->addRowField(api_number_format($exchangerate_fobj->avg,4),"text-right nowrap");
  $exchangerate_table->addRowField(api_number_format($exchangerate_fobj->jan,4),"text-right nowrap");
  $exchangerate_table->addRowField(api_number_format($exchangerate_fobj->feb,4),"text-right nowrap");
  $exchangerate_table->addRowField(api_number_format($exchangerate_fobj->mar,4),"text-right nowrap");
  $exchangerate_table->addRowField(api_number_format($exchangerate_fobj->apr,4),"text-right nowrap");
  $exchangerate_table->addRowField(api_number_format($exchangerate_fobj->may,4),"text-right nowrap");
  $exchangerate_table->addRowField(api_number_format($exchangerate_fobj->jun,4),"text-right nowrap");
  $exchangerate_table->addRowField(api_number_format($exchangerate_fobj->jul,4),"text-right nowrap");
  $exchangerate_table->addRowField(api_number_format($exchangerate_fobj->aug,4),"text-right nowrap");
  $exchangerate_table->addRowField(api_number_format($exchangerate_fobj->sep,4),"text-right nowrap");
  $exchangerate_table->addRowField(api_number_format($exchangerate_fobj->oct,4),"text-right nowrap");
  $exchangerate_table->addRowField(api_number_format($exchangerate_fobj->nov,4),"text-right nowrap");
  $exchangerate_table->addRowField(api_number_format($exchangerate_fobj->dec,4),"text-right nowrap");
  if(api_checkAuthorization("subsidiaries_reports-edit_all")){$exchangerate_table->addRowField($ob->render(),"nowrap text-right");}
}

// check for actions
if(in_array(ACTION,["exchangerate_add","exchangerate_edit"])){
  // get selected exchangerate
  $selected_exchangerate_obj=new cSubsidiariesReportsCurrencyExchangeRate($_REQUEST["idCurrencyExchangerate"]);
  // get form
  $form=$selected_exchangerate_obj->form_edit(["return"=>["scr"=>"currencies_view","tab"=>"exchangerates","idCurrency"=>$currency_obj->id]]);
  // replace fkCurrency
  $form->removeField("fkCurrency");
  $form->addField("hidden","fkCurrency",null,$currency_obj->id);
  if($selected_exchangerate_obj->id){
    $form->removeField("year");
    $form->addField("hidden","year",api_text("cSubsidiariesReportsCurrencyExchangeRate-property-year"),$selected_exchangerate_obj->year);
  }
  // additional controls
  $form->addControl("button",api_text("form-fc-cancel"),"#",null,null,null,"data-dismiss='modal'");
  if($selected_exchangerate_obj->exists()){$form->addControl("button",api_text("form-fc-remove"),api_url(["scr"=>"controller","act"=>"remove","obj"=>"cSubsidiariesReportsCurrencyExchangeRate","idCurrency"=>$currency_obj->id,"idExchangerate"=>$selected_exchangerate_obj->id,"return"=>["scr"=>"currencies_view","tab"=>"exchangerates","idCurrency"=>$currency_obj->id]]),"btn-danger",api_text("cSubsidiariesReportsCurrencyExchangeRate-confirm-remove"));}
  // build modal
  $modal=new strModal(api_text("currencies_view-exchangerates-modal-title-".($selected_exchangerate_obj->exists()?"edit":"add"),[$currency_obj->code,$selected_exchangerate_obj->year]),null,"currencies_view-exchangerates");
  $modal->setBody($form->render(1));
  // add modal to application
  $app->addModal($modal);
  // modal scripts
  $app->addScript("$(function(){\$('#modal_currencies_view-exchangerates').modal({show:true,backdrop:'static',keyboard:false});});");
}
