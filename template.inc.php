<?php
/**
 * Subsidiaries Reports - Template
 *
 * @package Coordinator\Modules\SubsidiariesReports
 * @company Cogne Acciai Speciali s.p.a
 */

// build application
$app=new strApplication();
// build nav
$nav=new strNav("nav-tabs");
// dashboard
$nav->addItem(api_icon("fa-th-large",null,"hidden-link"),api_url(["scr"=>"dashboard"]));
// management
if(api_checkAuthorization("subsidiaries_reports-manage")){$nav->addItem(api_text("nav-management"),api_url(["scr"=>"management","tab"=>"templates"]));}
// dossier
$nav->addItem(api_text("nav-dossier"),api_url(["scr"=>"dossier"]),(api_checkAuthorization(["subsidiaries_reports-usage","subsidiaries_reports-view_all"])));

/**
 * Currencies
 *
 * @var cSubsidiariesReportsCurrency $currency_obj
 */
$nav->addItem(api_text("nav-currencies-list"),api_url(["scr"=>"currencies_list"]));
if(api_script_prefix()=="currencies"){
  if(in_array(SCRIPT,array("currencies_view","currencies_edit")) && $currency_obj->exists()){
    $nav->addItem(api_text("nav-operations"),null,null,"active");
    $nav->addSubItem(api_text("nav-currencies-operations-exchangerate"),api_url(["scr"=>"currencies_view","tab"=>"exchangerates","act"=>"exchangerate_add","idCurrency"=>$currency_obj->id]),(api_checkAuthorization("subsidiaries_reports-edit_all")));
  }
}

/**
 * Templates
 *
 * @var cSubsidiariesReportsTemplate $template_obj
 */
if(api_script_prefix()=="templates"){
  if(SCRIPT=="templates_edit" && !$template_obj->exists()){$nav->addItem(api_text("nav-templates-add"),api_url(["scr"=>"templates_edit"]),(api_checkAuthorization("subsidiaries_reports-manage")));}
  elseif(is_object($template_obj) && $template_obj->exists() && in_array(SCRIPT,array("templates_view","templates_edit"))){
    $nav->addItem(api_text("nav-operations"),null,null,"active");
    $nav->addSubItem(api_text("nav-templates-operations-edit"),api_url(["scr"=>"templates_edit","idTemplate"=>$template_obj->id]),(api_checkAuthorization("subsidiaries_reports-manage")));
    $nav->addSubSeparator();
    $nav->addSubItem(api_text("nav-templates-operations-entry_add"),api_url(["scr"=>"templates_view","tab"=>"entries","act"=>"entry_add","idTemplate"=>$template_obj->id]),(api_checkAuthorization("subsidiaries_reports-manage")));
  }
}

/**
 * Reports
 *
 * @var cSubsidiariesReportsReport $report_obj
 */
$nav->addItem(api_text("nav-reports-list"),api_url(["scr"=>"reports_list"]));
if(api_script_prefix()=="reports"){
  if(in_array(SCRIPT,array("reports_view","reports_edit","reports_fill")) && $report_obj->exists()){
    $nav->addItem(api_text("nav-operations"),null,null,"active");
    $nav->addSubItem(api_text("nav-reports-operations-fill"),api_url(["scr"=>"reports_view","tab"=>"entries","act"=>"fill","idReport"=>$report_obj->id]),(api_checkAuthorization(["subsidiaries_reports-usage","subsidiaries_reports-edit_all"])));
    $nav->addSubSeparator();
    $nav->addSubItem(api_text("nav-reports-operations-import"),api_url(["scr"=>"reports_view","tab"=>"entries","act"=>"import","idReport"=>$report_obj->id]),(api_checkAuthorization("subsidiaries_reports-edit_all")));
    $nav->addSubItem(api_text("nav-reports-operations-remove"),api_url(["scr"=>"controller","act"=>"remove","obj"=>"cSubsidiariesReportsReport","idReport"=>$report_obj->id]),(api_checkAuthorization("subsidiaries_reports-manage")),api_text("cSubsidiariesReportsReport-confirm-remove"));
  }else{
    $nav->addItem(api_text("nav-reports-add"),api_url(["scr"=>"reports_edit"]),(api_checkAuthorization("subsidiaries_reports-edit_all")));
  }
}

// add nav to html
$app->addContent($nav->render(false));
