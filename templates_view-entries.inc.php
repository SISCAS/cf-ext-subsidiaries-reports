<?php
/**
 * Subsidiaries Reports - Templates View (Entries)
 *
 * @package Coordinator\Modules\SubsidiariesReports
 * @company Cogne Acciai Speciali s.p.a
 *
 * @var strApplication $app
 * @var cSubsidiariesReportsTemplate $template_obj
 */

// build entries table
$entries_table=new strTable(api_text("templates_view-entries-tr-unvalued"));
$entries_table->addHeader("&nbsp;");
$entries_table->addHeader(api_text("cSubsidiariesReportsTemplateEntry-property-name"),"nowrap");
$entries_table->addHeader(api_text("cSubsidiariesReportsTemplateEntry-property-description")." / ".api_text("cSubsidiariesReportsTemplateEntry-property-formula"),null,"100%");
$entries_table->addHeader(api_text("cSubsidiariesReportsTemplateEntry-property-fkUnit"),"nowrap text-right");

// cycle all entries
foreach($template_obj->getEntries(api_checkAuthorization("subsidiaries_reports-manage")) as $entry_fobj){
  // build operation button            /** @todo valutare se contare piu efficaciemente    ( al move down ) */
  $ob=new strOperationsButton();
  $ob->addElement(api_url(["scr"=>"controller","act"=>"move","direction"=>"up","obj"=>"cSubsidiariesReportsTemplateEntry","idEntry"=>$entry_fobj->id,"return"=>["scr"=>"templates_view","tab"=>"entries","idTemplate"=>$template_obj->id]]),"fa-arrow-up",api_text("table-td-move-up"),($entry_fobj->order>1));
  $ob->addElement(api_url(["scr"=>"controller","act"=>"move","direction"=>"down","obj"=>"cSubsidiariesReportsTemplateEntry","idEntry"=>$entry_fobj->id,"return"=>["scr"=>"templates_view","tab"=>"entries","idTemplate"=>$template_obj->id]]),"fa-arrow-down",api_text("table-td-move-down"),($entry_fobj->order!=count($template_obj->getEntries())));  /** @todo valutare se contare piu efficaciemente */
  $ob->addElement(api_url(["scr"=>"templates_view","tab"=>"entries","act"=>"entry_edit","idTemplate"=>$template_obj->id,"idEntry"=>$entry_fobj->id]),"fa-pencil",api_text("table-td-edit"),(api_checkAuthorization("subsidiaries_reports-manage")));
  if($entry_fobj->deleted){$ob->addElement(api_url(["scr"=>"controller","act"=>"undelete","obj"=>"cSubsidiariesReportsTemplateEntry","idTemplate"=>$template_obj->id,"idEntry"=>$entry_fobj->id,"return"=>["scr"=>"templates_view","tab"=>"entries","idTemplate"=>$template_obj->id]]),"fa-trash-o",api_text("table-td-undelete"),true,api_text("cSubsidiariesReportsTemplateEntry-confirm-undelete"));}
  else{$ob->addElement(api_url(["scr"=>"controller","act"=>"delete","obj"=>"cSubsidiariesReportsTemplateEntry","idTemplate"=>$template_obj->id,"idEntry"=>$entry_fobj->id,"return"=>["scr"=>"templates_view","tab"=>"entries","idTemplate"=>$template_obj->id]]),"fa-trash",api_text("table-td-delete"),true,api_text("cSubsidiariesReportsTemplateEntry-confirm-delete"));}
  // make table row class
  $tr_class_array=array();
  if($entry_fobj->id==$_REQUEST["idEntry"]){$tr_class_array[]="currentrow";}
  if($entry_fobj->deleted){$tr_class_array[]="deleted";}
  // make units
  $units=$entry_fobj->getUnit()->getLabel();
  if($entry_fobj->getUnit_2()){$units.=', '.$entry_fobj->getUnit_2()->getLabel();}
  if($entry_fobj->getUnit_3()){$units.=', '.$entry_fobj->getUnit_3()->getLabel();}
  if($entry_fobj->getUnit_4()){$units.=', '.$entry_fobj->getUnit_4()->getLabel();}
  // make description
  $descriptions=[];
  if($entry_fobj->description){$descriptions[]=$entry_fobj->description;}
  if($entry_fobj->formula){$descriptions[]="<code>".$entry_fobj->parseFormula()."</code>";}
  // make row
  $entries_table->addRow(implode(" ",$tr_class_array));
  $entries_table->addRowField($entry_fobj->getTypology()->getLabel(false,true),"nowrap");
  $entries_table->addRowField($entry_fobj->name,"nowrap");
  $entries_table->addRowField(implode("<br>",$descriptions),"truncate-ellipsis");
  $entries_table->addRowField($units,"nowrap text-right");
  $entries_table->addRowField($ob->render(),"nowrap text-right");
}

// check for actions
if(in_array(ACTION,["entry_add","entry_edit"])){
  // get selected entry
  $selected_entry_obj=new cSubsidiariesReportsTemplateEntry($_REQUEST["idEntry"]);
  // get form
  $form=$selected_entry_obj->form_edit(["return"=>["scr"=>"templates_view","tab"=>"entries","idTemplate"=>$template_obj->id]]);
  // replace fkTemplate
  $form->removeField("fkTemplate");
  $form->addField("hidden","fkTemplate",null,$template_obj->id);
  // add formula entry
  $form->addField("select","formula_entry",api_text("templates_view-entries-fl-formula_entry"),null,api_text("templates_view-entries-fp-formula_entry"));
  foreach(cSubsidiariesReportsTemplateEntry::availables($selected_entry_obj->exists(),["fkTemplate"=>$template_obj->id]) as $entry_fobj){
    if($entry_fobj->id==$selected_entry_obj->id){continue;}
    $form->addFieldOption($entry_fobj->id,$entry_fobj->name);
  }
  $form->addFieldAddonButton("#","+","btn btn-default");
  // formula entry scripts
  $app->addScript("function template_view_entries_toggle(){if($('#form_subsidiaries_reports__template__entry-edit_form_input_typology option:selected').val()=='calculated'){\$('#form_subsidiaries_reports__template__entry-edit_form_input_formula_form_group').show();$('#form_subsidiaries_reports__template__entry-edit_form_input_formula_entry_form_group').show();}else{\$('#form_subsidiaries_reports__template__entry-edit_form_input_formula_form_group').hide();$('#form_subsidiaries_reports__template__entry-edit_form_input_formula_entry_form_group').hide();}};");
  $app->addScript("$(document).ready(function(){template_view_entries_toggle();});");
  $app->addScript("$(document).ready(function(){\$('#form_subsidiaries_reports__template__entry-edit_form_input_typology').change(function(){template_view_entries_toggle();});});");
  $app->addScript("$(document).ready(function(){\$('#form_subsidiaries_reports__template__entry-edit_form_input_formula_entry_button').click(function(){val=$('#form_subsidiaries_reports__template__entry-edit_form_input_formula_entry option:selected').val();if(val>0){\$('#form_subsidiaries_reports__template__entry-edit_form_input_formula').val($('#form_subsidiaries_reports__template__entry-edit_form_input_formula').val()+' {'+val+'} ');}});});");
  // additional controls
  $form->addControl("button",api_text("form-fc-cancel"),"#",null,null,null,"data-dismiss='modal'");
  if($selected_entry_obj->exists()){$form->addControl("button",api_text("form-fc-remove"),api_url(["scr"=>"controller","act"=>"remove","obj"=>"cSubsidiariesReportsTemplateEntry","idTemplate"=>$template_obj->id,"idEntry"=>$selected_entry_obj->id,"return"=>["scr"=>"templates_view","tab"=>"entries","idTemplate"=>$template_obj->id]]),"btn-danger",api_text("cSubsidiariesReportsTemplateEntry-confirm-remove"));}
  // build modal
  $modal=new strModal(api_text("templates_view-entries-modal-title-".($selected_entry_obj->exists()?"edit":"add"),$template_obj->name),null,"templates_view-entries");
  $modal->setBody($form->render(1));
  // add modal to application
  $app->addModal($modal);
  // modal scripts
  $app->addScript("$(function(){\$('#modal_templates_view-entries').modal({show:true,backdrop:'static',keyboard:false});});");
  $app->addScript("$(document).ready(function(){\$('select[name=\"fkUser\"]').select2({allowClear:true,placeholder:\"".api_text("cSubsidiariesReportsTemplateEntry-placeholder-fkUser")."\",dropdownParent:\$('#modal_templates_view-entries')});});");
}
