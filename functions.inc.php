<?php
/**
 * Subsidiaries Reports - Functions
 *
 * @package Coordinator\Modules\SubsidiariesReports
 * @company Cogne Acciai Speciali s.p.a
 */

require_once(DIR."modules/subsidiaries_reports/classes/cSubsidiariesReportsUnit.class.php");
require_once(DIR."modules/subsidiaries_reports/classes/cSubsidiariesReportsCurrency.class.php");
require_once(DIR."modules/subsidiaries_reports/classes/cSubsidiariesReportsCurrencyExchangeRate.class.php");
require_once(DIR."modules/subsidiaries_reports/classes/cSubsidiariesReportsTemplate.class.php");
require_once(DIR."modules/subsidiaries_reports/classes/cSubsidiariesReportsTemplateEntry.class.php");
require_once(DIR."modules/subsidiaries_reports/classes/cSubsidiariesReportsTemplateEntryTypology.class.php");
require_once(DIR."modules/subsidiaries_reports/classes/cSubsidiariesReportsReport.class.php");
require_once(DIR."modules/subsidiaries_reports/classes/cSubsidiariesReportsReportEntry.class.php");
require_once(DIR."modules/subsidiaries_reports/classes/cSubsidiariesReportsReportEntryValue.class.php");
