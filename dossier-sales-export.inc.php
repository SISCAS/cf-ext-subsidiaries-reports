<?php
/**
 * Subsidiaries Reports - SALES Dossier Export
 *
 * @package Coordinator\Modules\SubsidiariesReports
 * @company Cogne Acciai Speciali s.p.a
 *
 * @var array $data
 * @var cSettings $settings
 */
// check authorizations
api_checkAuthorization(["subsidiaries_reports-usage","subsidiaries_reports-view_all"],"dashboard");
// check objects
if(!is_array($data)){api_alerts_add(api_text('crm_alert-dataNotFound'),'danger');api_redirect('?mod='.MODULE.'&scr=dossier');}
// include pdf library
require_once(DIR.'helpers/tcpdf/php/tcpdf.php');
// include math library
require_once(DIR."helpers/evalmath/php/EvalMath.php");
// extend tcpdf class
class TCPDFX extends TCPDF{
  /**
   * Custom Properties
   */
  public $border_debug='';
  public $data=array();
  public $fixedX=null;
  /*
   * Custom Methods
   */
  public function LnFixedX($h=null,$cell=false){
    $this->Ln($h,$cell);
    if($this->fixedX){$this->setX($this->fixedX);}
  }
  public function resetDefaultStyle(){
    $this->SetFont('helvetica','',9);
    $this->setTextColor(0,0,0);
  }
  public function TableCell($width,$height,$content='',$border='',$align='L',$class=null){ //,$font_style='',$font_size=10
    if(!strlen($content)||!$content){$content='-';}
    // default style
    $this->resetDefaultStyle();
    $fill=false;
    // switch class
    switch($class){
      case "title":
        $this->SetFont('helvetica','B',11);
        break;
      case "footer":
        $this->SetFont('helvetica','',7);
        break;
      case "header":
        $this->SetFont('helvetica','I',9);
        break;
      case "total":
        $this->SetFont('helvetica','B',9);
        $this->setTextColor(255,255,255);
        $this->setFillColor(119,119,119);
        $this->setFillColor(119,119,119);
        $fill=true;
        break;
      case "actual":
        $this->SetFont('helvetica','B',9);
        $this->setTextColor(255,255,255);
        $this->setFillColor(51,122,183);
        $this->setFillColor(40,96,144);
        $fill=true;
        break;
      case "budget":
        $this->SetFont('helvetica','B',9);
        $this->setTextColor(255,255,255);
        $this->setFillColor(217,83,79);
        $this->setFillColor(171,4,52);
        $fill=true;
        break;
    }
    $this->MultiCell($width,$height,$content,$border,$align,$fill,0,null,null,true,0,false,false,0,'M',true);
    $this->resetDefaultStyle();
  }
  /**
   * Print Cover
   */
  public function printCover(){
    $this->Image(DIR.'/modules/subsidiaries_reports/images/cover.png',0,0,297,210,'PNG',null,'T');
    $this->SetFont('helvetica','B',22);
    $this->SetTextColor(230);
    $this->MultiCell(120,40,"Sales Reports\n\n".$this->data['month_name'].' '.$this->data['year'],$this->border_debug,'L',false,0,15,10,true,0,false,false,0,'M',true);
    $this->resetDefaultStyle();
  }
  /**
   * Print Page Header
   */
  public function printPageHeader($title,$subtitle){
    // check for subsidiary logo
    $logo_path=DIR.'modules/subsidiaries_reports/images/logo.png';     // @todo creare logo filiali nel modulo filiali con upload
    $this->border_debug='';
    $logo_size=getimagesize($logo_path);
    $logo_width=$logo_size[0];
    $logo_height=$logo_size[1];
    $logo_width_resized=round($logo_width*10/$logo_height);
    $this->Image($logo_path,10,10,'',10,'PNG','http://www.cogne.com','T');
    $this->SetFont('helvetica','B',18);
    $this->MultiCell(110,10,$title,$this->border_debug,'S',false,0,$logo_width_resized+12,10,true,0,false,false,0,'M',true);
    $this->SetFont('helvetica','',18);
    $this->MultiCell(0,10,$subtitle,$this->border_debug,'R',false,0,10,10,true,0,false,false,0,'M',true);
    $this->resetDefaultStyle();
  }
  /**
   * Print Page Footer
   */
  public function printPageFooter(){
    $this->border_debug='';
    // set xy and fix
    $this->setXY(10,-10);
    // print title
    $this->TableCell(65,3,"Sales Report",$this->border_debug,'L','footer');
    $this->TableCell(65,3,date('Y-m-d'),$this->border_debug,'C','footer');
    $this->TableCell(65,3,'Page '.$this->PageNo().' of '.rtrim($this->getAliasNbPages()),$this->border_debug,'R','footer');
    $this->resetDefaultStyle();
  }
  /**
   * Print Subsidiary Monthly Table
   */
  public function printSubsidiaryMonthlyTable($startX,$startY,$width,$title,$entryTitle,$idSubsidiary,$idTemplate,$idEntries,$styles=true){
    //api_dump($startX.':'.$startY.' - '.$title.' ('.$idSubsidiary.','.$idTemplate.')');
    // set xy and fix
    $this->setXY($startX,$startY);
    $this->fixedX=$startX;
    // calculate columns width
    $columns_width=$width-120;
    // print title
    $this->TableCell($columns_width,8,$title,$this->border_debug,'L','title');
    $this->TableCell(60,8,'ACTUAL','R','C','header');
    $this->TableCell(60,8,'BUDGET',$this->border_debug,'C','header');
    $this->LnFixedX();
    // print headers
    $this->TableCell($columns_width,5,$entryTitle,$this->border_debug,'L','header');
    $this->TableCell(15,5,'TONS',$this->border_debug,'R','header');
    $this->TableCell(15,5,'VKEUR',$this->border_debug,'R','header');
    $this->TableCell(15,5,'MKEUR',$this->border_debug,'R','header');
    $this->TableCell(15,5,'MARGIN','R','R','header');
    $this->TableCell(15,5,'TONS',$this->border_debug,'R','header');
    $this->TableCell(15,5,'VKEUR',$this->border_debug,'R','header');
    $this->TableCell(15,5,'MKEUR',$this->border_debug,'R','header');
    $this->TableCell(15,5,'MARGIN',$this->border_debug,'R','header');
    $this->LnFixedX();
    // print entries
    foreach($idEntries as $idEntry){
      $entry=$this->data['templates_entries'][$idTemplate][$idEntry];
      $values=$this->data['reports_entries'][$idSubsidiary][$idTemplate][$idEntry][$this->data['period']];
      //var_dump($entry);
      //var_dump($values);
      // check for 100% errors
      //if($entry->unit=='%' && round($values->actual)==100){$values->actual=null;}
      //if($entry->unit=='%' && round($values->budget)==100){$values->budget=null;}
      // set total
      if($styles && ($entry->typology=='calculated' || strpos(strtolower($entry->name),'total')!==false)){$total=true;}else{$total=false;}
      // print entry
      $this->TableCell($columns_width,6,$entry->name,'1','L',($total?'total':null));
      $this->TableCell(15,6,api_number_format($values->actual_1,0),'1','R',($total?'actual':null));
      $this->TableCell(15,6,api_number_format($values->actual_2,0),'1','R',($total?'actual':null));
      $this->TableCell(15,6,api_number_format($values->actual_3,0),'1','R',($total?'actual':null));
      $this->TableCell(15,6,api_number_format(($values->actual_3 / $values->actual_2 * 100),2),'1','R',($total?'actual':null));

      $this->TableCell(15,6,api_number_format($values->budget_1,0),'1','R',($total?'budget':null));
      $this->TableCell(15,6,api_number_format($values->budget_2,0),'1','R',($total?'budget':null));
      $this->TableCell(15,6,api_number_format($values->budget_3,0),'1','R',($total?'budget':null));
      $this->TableCell(15,6,api_number_format(($values->budget_3 / $values->budget_2 * 100),2),'1','R',($total?'budget':null));

      $this->LnFixedX();
    }
    $this->resetDefaultStyle();
    $this->fixedX=null;
  }
  /**
   * Print Subsidiary YTD Table
   */
  public function printSubsidiaryYtdTable($startX,$startY,$width,$title,$entryTitle,$idSubsidiary,$idTemplate,$periods,$idEntries,$styles=true){
    //api_dump($startX.':'.$startY.' - '.$title.' ('.$idSubsidiary.','.$idTemplate.')');
    // calculate periods and totals
    $calculated_data=$this->calculateTotals($idTemplate,$idEntries,['ALL'=>[$idSubsidiary]],$periods);
    // set xy and fix
    $this->setXY($startX,$startY);
    $this->fixedX=$startX;
    // calculate columns width
    $columns_width=$width-120;
    // print title
    $this->TableCell($columns_width,8,$title,$this->border_debug,'L','title');
    $this->TableCell(60,8,'ACTUAL','R','C','header');
    $this->TableCell(60,8,'BUDGET',$this->border_debug,'C','header');
    $this->LnFixedX();
    // print headers
    $this->TableCell($columns_width,5,$entryTitle,$this->border_debug,'L','header');
    $this->TableCell(15,5,'TONS',$this->border_debug,'R','header');
    $this->TableCell(15,5,'VKEUR',$this->border_debug,'R','header');
    $this->TableCell(15,5,'MKEUR',$this->border_debug,'R','header');
    $this->TableCell(15,5,'MARGIN','R','R','header');
    $this->TableCell(15,5,'TONS',$this->border_debug,'R','header');
    $this->TableCell(15,5,'VKEUR',$this->border_debug,'R','header');
    $this->TableCell(15,5,'MKEUR',$this->border_debug,'R','header');
    $this->TableCell(15,5,'MARGIN',$this->border_debug,'R','header');
    $this->LnFixedX();
    // print entries
    foreach($idEntries as $idEntry){
      $entry=$this->data['templates_entries'][$idTemplate][$idEntry];
      //$values=$this->data['reports_entries'][$idSubsidiary][$idTemplate][$idEntry][$period];
      $values=$calculated_data[$idEntry]['ALL'][$idSubsidiary];
      //api_dump($this->GetX().':'.$this->GetY().' '.$entry->name.' '.$entry->unit.' '.$values->actual.' | '.$values->budget.' '.$entry->typology);
      // set total
      if($styles && ($entry->typology=='calculated' || strpos(strtolower($entry->name),'total')!==false)){$total=true;}else{$total=false;}
      // print entry
      $this->TableCell($columns_width,6,$entry->name,'1','L',($styles?($entry->typology=='calculated'?'total':null):null));
      $this->TableCell(15,6,api_number_format($values->actual_1,0),'1','R',($total?'actual':null));
      $this->TableCell(15,6,api_number_format($values->actual_2,0),'1','R',($total?'actual':null));
      $this->TableCell(15,6,api_number_format($values->actual_3,0),'1','R',($total?'actual':null));
      $this->TableCell(15,6,api_number_format(($values->actual_3 / $values->actual_2 * 100),2),'1','R',($total?'actual':null));
      $this->TableCell(15,6,api_number_format($values->budget_1,0),'1','R',($total?'budget':null));
      $this->TableCell(15,6,api_number_format($values->budget_2,0),'1','R',($total?'budget':null));
      $this->TableCell(15,6,api_number_format($values->budget_3,0),'1','R',($total?'budget':null));
      $this->TableCell(15,6,api_number_format(($values->budget_3 / $values->budget_2 * 100),2),'1','R',($total?'budget':null));
      $this->LnFixedX();
    }
    $this->resetDefaultStyle();
    $this->fixedX=null;
  }
  /*
   * Calculate Total for Periods and Subsidiaries groups
   */
  public function calculateTotals($idTemplate,$idEntries,$groupsSubsidiaries,$periods){
    // definitions
    $return=array();
    $entries_array=array();
    $formulas_array=array();
    // get all entries objects
    foreach($idEntries as $idEntry){$entries_array[$idEntry]=new cSubsidiariesReportsTemplateEntry($idEntry);}
    // temporary array with key {idFormula} for formula replace
    $formulas_replace_array=array();
    // cycle all calculate entries and store formulas
    foreach($entries_array as $idEntry=>$entry_obj){
      if($entry_obj->typology!='calculated'){continue;}
      $formulas_replace_array["{".$entry_obj->id."}"]=" ( ".$entry_obj->formula." ) ";
    }
    // cycle all calculate entries and recursively replace formulas
    foreach($entries_array as $idEntry=>$entry_obj){
      if($entry_obj->typology!='calculated'){continue;}
      $formula=$entry_obj->formula;
      // replace formulas recursively (max 100 times)
      for($i=0;$i<100;$i++){$formula=str_replace(array_keys($formulas_replace_array),$formulas_replace_array,$formula);}
      $formulas_array[$idEntry]=$formula;
    }
    //api_dump($formulas_array,'formulas_array');
    // temporary array with key [$idSubsidiary][{idEntry}] for formula replace
    $formulas_data_actual_1=array();
    $formulas_data_actual_2=array();
    $formulas_data_actual_3=array();
    $formulas_data_actual_4=array();
    $formulas_data_budget_1=array();
    $formulas_data_budget_2=array();
    $formulas_data_budget_3=array();
    $formulas_data_budget_4=array();
    // cycle all un-calculated entries
    foreach($idEntries as $idEntry){
      if($entries_array[$idEntry]->typology=='calculated'){continue;}
      // temporary field for groups totals sum
      $entry_group_total_actual_1=0;
      $entry_group_total_actual_2=0;
      $entry_group_total_actual_3=0;
      $entry_group_total_actual_4=0;
      $entry_group_total_budget_1=0;
      // cycle all groups
      foreach($groupsSubsidiaries as $group=>$idSubsidiaries){
        // temporary field for subsidiaries totals sum
        $entry_subsidiaries_total_actual=0;
        $entry_subsidiaries_total_budget=0;
        // cycle all subsidiaries
        foreach($idSubsidiaries as $idSubsidiary){
          // temporary field for periods sum
          $entry_periods_total_actual_1=0;
          $entry_periods_total_actual_2=0;
          $entry_periods_total_actual_3=0;
          $entry_periods_total_actual_4=0;
          $entry_periods_total_budget_1=0;
          $entry_periods_total_budget_2=0;
          $entry_periods_total_budget_3=0;
          $entry_periods_total_budget_4=0;
          foreach($periods as $period){
            // increment periods sum
            $entry_periods_total_actual_1+=$this->data['reports_entries'][$idSubsidiary][$idTemplate][$idEntry][$period]->actual_1;
            $entry_periods_total_actual_2+=$this->data['reports_entries'][$idSubsidiary][$idTemplate][$idEntry][$period]->actual_2;
            $entry_periods_total_actual_3+=$this->data['reports_entries'][$idSubsidiary][$idTemplate][$idEntry][$period]->actual_3;
            $entry_periods_total_actual_4+=$this->data['reports_entries'][$idSubsidiary][$idTemplate][$idEntry][$period]->actual_4;
            $entry_periods_total_budget_1+=$this->data['reports_entries'][$idSubsidiary][$idTemplate][$idEntry][$period]->budget_1;
            $entry_periods_total_budget_2+=$this->data['reports_entries'][$idSubsidiary][$idTemplate][$idEntry][$period]->budget_2;
            $entry_periods_total_budget_3+=$this->data['reports_entries'][$idSubsidiary][$idTemplate][$idEntry][$period]->budget_3;
            $entry_periods_total_budget_4+=$this->data['reports_entries'][$idSubsidiary][$idTemplate][$idEntry][$period]->budget_4;
          }
          // increment subsidiaries totals
          $entry_subsidiaries_total_actual_1+=$entry_periods_total_actual_1;
          $entry_subsidiaries_total_actual_2+=$entry_periods_total_actual_2;
          $entry_subsidiaries_total_actual_3+=$entry_periods_total_actual_3;
          $entry_subsidiaries_total_actual_4+=$entry_periods_total_actual_4;
          $entry_subsidiaries_total_budget_1+=$entry_periods_total_budget_1;
          $entry_subsidiaries_total_budget_2+=$entry_periods_total_budget_2;
          $entry_subsidiaries_total_budget_3+=$entry_periods_total_budget_3;
          $entry_subsidiaries_total_budget_4+=$entry_periods_total_budget_4;
          // set periods total as formula data for replace
          $formulas_data_actual_1[$group][$idSubsidiary]['{'.$idEntry.'}']=$entry_periods_total_actual_1;
          $formulas_data_actual_2[$group][$idSubsidiary]['{'.$idEntry.'}']=$entry_periods_total_actual_2;
          $formulas_data_actual_3[$group][$idSubsidiary]['{'.$idEntry.'}']=$entry_periods_total_actual_3;
          $formulas_data_actual_4[$group][$idSubsidiary]['{'.$idEntry.'}']=$entry_periods_total_actual_4;
          $formulas_data_budget_1[$group][$idSubsidiary]['{'.$idEntry.'}']=$entry_periods_total_budget_1;
          $formulas_data_budget_2[$group][$idSubsidiary]['{'.$idEntry.'}']=$entry_periods_total_budget_2;
          $formulas_data_budget_3[$group][$idSubsidiary]['{'.$idEntry.'}']=$entry_periods_total_budget_3;
          $formulas_data_budget_4[$group][$idSubsidiary]['{'.$idEntry.'}']=$entry_periods_total_budget_4;
          // build subsidiary values object
          $subsidiary_values=new stdClass();
          $subsidiary_values->actual_1=$entry_periods_total_actual_1;
          $subsidiary_values->actual_2=$entry_periods_total_actual_2;
          $subsidiary_values->actual_3=$entry_periods_total_actual_3;
          $subsidiary_values->actual_4=$entry_periods_total_actual_4;
          $subsidiary_values->budget_1=$entry_periods_total_budget_1;
          $subsidiary_values->budget_2=$entry_periods_total_budget_2;
          $subsidiary_values->budget_3=$entry_periods_total_budget_3;
          $subsidiary_values->budget_4=$entry_periods_total_budget_4;
          // add subsidiary values to return
          $return[$idEntry][$group][$idSubsidiary]=$subsidiary_values;
        }
        // increment group totals
        $entry_group_total_actual_1+=$entry_subsidiaries_total_actual_1;
        $entry_group_total_actual_2+=$entry_subsidiaries_total_actual_2;
        $entry_group_total_actual_3+=$entry_subsidiaries_total_actual_3;
        $entry_group_total_actual_4+=$entry_subsidiaries_total_actual_4;
        $entry_group_total_budget_1+=$entry_subsidiaries_total_budget_1;
        $entry_group_total_budget_2+=$entry_subsidiaries_total_budget_2;
        $entry_group_total_budget_3+=$entry_subsidiaries_total_budget_3;
        $entry_group_total_budget_4+=$entry_subsidiaries_total_budget_4;
        // set subsidiaries total as formula data for replace
        $formulas_data_actual_1[$group]['TOTALS']['{'.$idEntry.'}']=$entry_subsidiaries_total_actual_1;
        $formulas_data_actual_2[$group]['TOTALS']['{'.$idEntry.'}']=$entry_subsidiaries_total_actual_2;
        $formulas_data_actual_3[$group]['TOTALS']['{'.$idEntry.'}']=$entry_subsidiaries_total_actual_3;
        $formulas_data_actual_4[$group]['TOTALS']['{'.$idEntry.'}']=$entry_subsidiaries_total_actual_4;
        $formulas_data_budget_1[$group]['TOTALS']['{'.$idEntry.'}']=$entry_subsidiaries_total_budget_1;
        $formulas_data_budget_2[$group]['TOTALS']['{'.$idEntry.'}']=$entry_subsidiaries_total_budget_2;
        $formulas_data_budget_3[$group]['TOTALS']['{'.$idEntry.'}']=$entry_subsidiaries_total_budget_3;
        $formulas_data_budget_4[$group]['TOTALS']['{'.$idEntry.'}']=$entry_subsidiaries_total_budget_4;
        // build total values object
        $subsidiaries_total_values=new stdClass();
        $subsidiaries_total_values->actual_1=$entry_subsidiaries_total_actual_1;
        $subsidiaries_total_values->actual_2=$entry_subsidiaries_total_actual_2;
        $subsidiaries_total_values->actual_3=$entry_subsidiaries_total_actual_3;
        $subsidiaries_total_values->actual_4=$entry_subsidiaries_total_actual_4;
        $subsidiaries_total_values->budget_1=$entry_subsidiaries_total_budget_1;
        $subsidiaries_total_values->budget_2=$entry_subsidiaries_total_budget_2;
        $subsidiaries_total_values->budget_3=$entry_subsidiaries_total_budget_3;
        $subsidiaries_total_values->budget_4=$entry_subsidiaries_total_budget_4;
        // add totals values to return
        $return[$idEntry][$group]['TOTALS']=$subsidiaries_total_values;
      }
      // set group total as formula data for replace
      $formulas_data_actual_1['TOTALS']['TOTALS']['{'.$idEntry.'}']=$entry_group_total_actual_1;
      $formulas_data_actual_2['TOTALS']['TOTALS']['{'.$idEntry.'}']=$entry_group_total_actual_2;
      $formulas_data_actual_3['TOTALS']['TOTALS']['{'.$idEntry.'}']=$entry_group_total_actual_3;
      $formulas_data_actual_4['TOTALS']['TOTALS']['{'.$idEntry.'}']=$entry_group_total_actual_4;
      $formulas_data_budget_1['TOTALS']['TOTALS']['{'.$idEntry.'}']=$entry_group_total_budget_1;
      $formulas_data_budget_2['TOTALS']['TOTALS']['{'.$idEntry.'}']=$entry_group_total_budget_2;
      $formulas_data_budget_3['TOTALS']['TOTALS']['{'.$idEntry.'}']=$entry_group_total_budget_3;
      $formulas_data_budget_4['TOTALS']['TOTALS']['{'.$idEntry.'}']=$entry_group_total_budget_4;
      // build group total values object
      $group_total_values=new stdClass();
      $group_total_values->actual_1=$entry_group_total_actual_1;
      $group_total_values->actual_2=$entry_group_total_actual_2;
      $group_total_values->actual_3=$entry_group_total_actual_3;
      $group_total_values->actual_4=$entry_group_total_actual_4;
      $group_total_values->budget_1=$entry_group_total_budget_1;
      // add totals values to return
      $return[$idEntry]['TOTALS']['TOTALS']=$group_total_values;
    }
    /*
    api_dump($formulas_data_actual_1,'formulas_data_actual_1');
    api_dump($formulas_data_actual_2,'formulas_data_actual_2');
    api_dump($formulas_data_actual_3,'formulas_data_actual_3');
    api_dump($formulas_data_actual_4,'formulas_data_actual_4');
    api_dump($formulas_data_budget_1,'formulas_data_budget_1');
    api_dump($formulas_data_budget_2,'formulas_data_budget_2');
    api_dump($formulas_data_budget_3,'formulas_data_budget_3');
    api_dump($formulas_data_budget_4,'formulas_data_budget_4');
    */
    // cycle all calculated entries
    foreach($idEntries as $idEntry){
      if($entries_array[$idEntry]->typology!='calculated'){continue;}
      // add total to groups array
      $groupsSubsidiaries['TOTALS']=array();
      // cycle all groups
      foreach($groupsSubsidiaries as $group=>$idSubsidiaries){
        // add total to subsidiaries array
        $idSubsidiaries[]='TOTALS';
        // cycle all subsidiaries
        foreach($idSubsidiaries as $idSubsidiary){
          //api_dump($group.'-'.$idSubsidiary);
          // build values object
          $totals_values=new stdClass();
          $totals_values->actual_1=0;
          $totals_values->actual_2=0;
          $totals_values->actual_3=0;
          $totals_values->actual_4=0;
          $totals_values->budget_1=0;
          $totals_values->budget_2=0;
          $totals_values->budget_3=0;
          $totals_values->budget_4=0;
          // replace formulas items with datas
          $formula_evaluated_actual_1=str_replace(array_keys($formulas_data_actual_1[$group][$idSubsidiary]),$formulas_data_actual_1[$group][$idSubsidiary],$formulas_array[$idEntry]);
          $formula_evaluated_actual_2=str_replace(array_keys($formulas_data_actual_2[$group][$idSubsidiary]),$formulas_data_actual_2[$group][$idSubsidiary],$formulas_array[$idEntry]);
          $formula_evaluated_actual_3=str_replace(array_keys($formulas_data_actual_3[$group][$idSubsidiary]),$formulas_data_actual_3[$group][$idSubsidiary],$formulas_array[$idEntry]);
          $formula_evaluated_actual_4=str_replace(array_keys($formulas_data_actual_4[$group][$idSubsidiary]),$formulas_data_actual_4[$group][$idSubsidiary],$formulas_array[$idEntry]);
          $formula_evaluated_budget_1=str_replace(array_keys($formulas_data_budget_1[$group][$idSubsidiary]),$formulas_data_budget_1[$group][$idSubsidiary],$formulas_array[$idEntry]);
          $formula_evaluated_budget_2=str_replace(array_keys($formulas_data_budget_2[$group][$idSubsidiary]),$formulas_data_budget_2[$group][$idSubsidiary],$formulas_array[$idEntry]);
          $formula_evaluated_budget_3=str_replace(array_keys($formulas_data_budget_3[$group][$idSubsidiary]),$formulas_data_budget_3[$group][$idSubsidiary],$formulas_array[$idEntry]);
          $formula_evaluated_budget_4=str_replace(array_keys($formulas_data_budget_4[$group][$idSubsidiary]),$formulas_data_budget_4[$group][$idSubsidiary],$formulas_array[$idEntry]);
          // try to evaluate formulas
          try{$totals_values->actual_1=(new EvalMath())->evaluate($formula_evaluated_actual_1);}catch(Exception $e){$totals_values->actual_1='NC';}
          try{$totals_values->actual_2=(new EvalMath())->evaluate($formula_evaluated_actual_2);}catch(Exception $e){$totals_values->actual_2='NC';}
          try{$totals_values->actual_3=(new EvalMath())->evaluate($formula_evaluated_actual_3);}catch(Exception $e){$totals_values->actual_3='NC';}
          try{$totals_values->actual_4=(new EvalMath())->evaluate($formula_evaluated_actual_4);}catch(Exception $e){$totals_values->actual_4='NC';}
          try{$totals_values->budget_1=(new EvalMath())->evaluate($formula_evaluated_budget_1);}catch(Exception $e){$totals_values->budget_1='NC';}
          try{$totals_values->budget_2=(new EvalMath())->evaluate($formula_evaluated_budget_2);}catch(Exception $e){$totals_values->budget_2='NC';}
          try{$totals_values->budget_3=(new EvalMath())->evaluate($formula_evaluated_budget_3);}catch(Exception $e){$totals_values->budget_3='NC';}
          try{$totals_values->budget_4=(new EvalMath())->evaluate($formula_evaluated_budget_4);}catch(Exception $e){$totals_values->budget_4='NC';}
          // add subsidiary calculated values to return
          $return[$idEntry][$group][$idSubsidiary]=$totals_values;
        }
      }
    }
    api_dump($return,'return');
    return $return;
  }
}
// settings
global $pdf;
global $border;
global $settings;
// create new pdf document
$pdf=new TCPDFX('P','mm','A4',true,'UTF-8',false,true);
// set properties
$pdf->SetCreator('TCPDF');
$pdf->SetAuthor($settings->owner);
$pdf->SetTitle('Sales Reports');
$pdf->SetSubject('Sales Reports '.ucfirst(api_calendar_months()[$data['month']]).' '.$data['year']);
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);
$pdf->SetFont('helvetica','',10);
$pdf->SetDefaultMonospacedFont('freemono');
$pdf->SetMargins(10,10);
$pdf->SetAutoPageBreak(false,10);
$pdf->setImageScale(1.25);
$pdf->setFontSubsetting(true);
// custom properties
$pdf->data=$data;
$pdf->border_debug='';
/**
 * Cover
 */
if(count($data['subsidiaries'])>1){
  $pdf->AddPage();
  $pdf->printCover();
}
// cycle all subsidiaries
foreach(array_keys($data['subsidiaries']) as $idSubsidiary){
  // skip non commercial
  //if($data['subsidiaries'][$idSubsidiary]->typology!='commercial'){continue;}
  /**
   * Subsidiary Monthly Report
   */
  $pdf->AddPage();
  $pdf->printPageHeader($data['subsidiaries'][$idSubsidiary]->name,'Monthly Report');
  $pdf->printSubsidiaryMonthlyTable(10,30,190,'Sales From Stock',$data['month_name'].' '.$data['year'],$idSubsidiary,7,array(
    77, // PES
    78, // PEL
    79, // SPE
    80, // LMA
    81, // TOTAL HOT ROLLED BARS
    82, // TRA
    83, // RET
    84, // SRE
    85, // TOTAL COLD FINISHED BARS
    86, // LAM
    87, // LAP
    88, // TOTAL BLACK BARS
    89, // TOTAL BARS IN NICHEL ALLOY
    90, // TOTAL BARS
    91, // TOTAL WIRE ROD STAINLESS
    92, // TOTAL WIRE ROD IN NICHEL ALLOYS
    93, // TOTAL WIRE ROD
    94, // COL LINGOTTI
    95, // COL BRAMME
    96, // FUC
    97, // LAM
    98, // SPT
    99, // TOTAL SEMIFINISHED PRODUCTS STAINLESS
    100, // TOTAL SEMIFINISHED PRODUCTS IN NICHEL ALLOYS
    101, // TOTAL SEMIFINISHED PRODUCTS
    102, // TOTAL OTHER PRODUCTS (COGNE)
    103, // TOTAL COGNE PRODUCTS
    104, // WIRE ROD
    105, // BARS
    106, // HOLLOW BARS
    107, // TOOL STEELS
    108, // WALSIN - BARS
    109, // WALSIN - WIRE ROD
    110, // DLP - BARS
    111, // DLP - SPT
    112, // OTHERS
    113, // TOTAL OTHER PRODUCTS (NOT COGNE)
    114, // TOTAL SALES FROM STOCK
  ));
  $pdf->printPageFooter();
  $pdf->AddPage();
  $pdf->printPageHeader($data['subsidiaries'][$idSubsidiary]->name,'Monthly Report');
  $pdf->printSubsidiaryMonthlyTable(10,30,190,'Sales From Mills (Subsidiary Invoice)',$data['month_name'].' '.$data['year'],$idSubsidiary,7,array(
    115, // TOTAL HOT ROLLED BARS
    116, // TOTAL COLD FINISHED BARS
    117, // TOTAL BLACK BARS
    118, // TOTAL BARS
    119, // TOTAL WIRE ROD
    120, // TOTAL SEMIFINISHED PRODUCTS
    121, // TOTAL STAINLESS
    122, // TOTAL BARS IN NICHEL ALLOYS
    123, // TOTAL WIRE ROD IN NICHEL ALLOYS
    124, // TOTAL SEMIFINISHED PRODUCTS IN NICHEL ALLOYS
    125, // TOTAL IN NICHEL ALLOY
    126, // TOTAL VALVE STEELS
    127, // TOTAL OTHER PRODUCTS (OF THE GROUP)
    128, // TOTAL OTHER PRODUCTS (OUT OF THE GROUP)
    129, // TOTAL SALES FROM MILL (SUBSIDIARY INVOICE)
  ));
  $pdf->printSubsidiaryMonthlyTable(10,140,190,'Sales From Mills (Mill Invoice)',$data['month_name'].' '.$data['year'],$idSubsidiary,7,array(
    130, // TOTAL HOT ROLLED BARS
    131, // TOTAL COLD FINISHED BARS
    132, // TOTAL BLACK BARS
    133, // TOTAL BARS
    134, // TOTAL WIRE ROD
    135, // TOTAL SEMIFINISHED PRODUCTS
    136, // TOTAL STAINLESS
    137, // TOTAL BARS IN NICHEL ALLOYS
    138, // TOTAL WIRE ROD IN NICHEL ALLOYS
    139, // TOTAL SEMIFINISHED PRODUCTS IN NICHEL ALLOYS
    140, // TOTAL IN NICHEL ALLOY
    141, // TOTAL VALVE STEELS
    142, // TOTAL OTHER PRODUCTS (OF THE GROUP)
    143, // TOTAL OTHER PRODUCTS (OUT OF THE GROUP)
    144, // TOTAL SALES FROM MILL (MILL INVOICE)
  ));
  $pdf->printSubsidiaryMonthlyTable(10,250,190,'Total Sales',$data['month_name'].' '.$data['year'],$idSubsidiary,7,array(
    114, // TOTAL SALES FROM STOCK
    145, // TOTAL SALES FROM MILL
    146, // TOTAL SALES
  ));
  $pdf->printPageFooter();

  // skip for january
  if($data['month']==1){continue;}
  /**
   * Subsidiary YTD Report
   */
  $pdf->AddPage();
  // header
  $pdf->printPageHeader($data['subsidiaries'][$idSubsidiary]->name,'YTD Report');
  // profit and loss
  $pdf->printSubsidiaryYtdTable(10,30,190,'Sales From Stock',$data['january'].' to '.$data['month_name'].' '.$data['year'],$idSubsidiary,7,array_keys(api_period_range($data['year'].'01',$data['period'])),array(
    77, // PES
    78, // PEL
    79, // SPE
    80, // LMA
    81, // TOTAL HOT ROLLED BARS
    82, // TRA
    83, // RET
    84, // SRE
    85, // TOTAL COLD FINISHED BARS
    86, // LAM
    87, // LAP
    88, // TOTAL BLACK BARS
    89, // TOTAL BARS IN NICHEL ALLOY
    90, // TOTAL BARS
    91, // TOTAL WIRE ROD STAINLESS
    92, // TOTAL WIRE ROD IN NICHEL ALLOYS
    93, // TOTAL WIRE ROD
    94, // COL LINGOTTI
    95, // COL BRAMME
    96, // FUC
    97, // LAM
    98, // SPT
    99, // TOTAL SEMIFINISHED PRODUCTS STAINLESS
    100, // TOTAL SEMIFINISHED PRODUCTS IN NICHEL ALLOYS
    101, // TOTAL SEMIFINISHED PRODUCTS
    102, // TOTAL OTHER PRODUCTS (COGNE)
    103, // TOTAL COGNE PRODUCTS
    104, // WIRE ROD
    105, // BARS
    106, // HOLLOW BARS
    107, // TOOL STEELS
    108, // WALSIN - BARS
    109, // WALSIN - WIRE ROD
    110, // DLP - BARS
    111, // DLP - SPT
    112, // OTHERS
    113, // TOTAL OTHER PRODUCTS (NOT COGNE)
    114, // TOTAL SALES FROM STOCK
  ));
  $pdf->printPageFooter();
  $pdf->AddPage();
  $pdf->printPageHeader($data['subsidiaries'][$idSubsidiary]->name,'YTD Report');
  $pdf->printSubsidiaryYtdTable(10,30,190,'Sales From Mills (Subsidiary Invoice)',$data['january'].' to '.$data['month_name'].' '.$data['year'],$idSubsidiary,7,array_keys(api_period_range($data['year'].'01',$data['period'])),array(
    115, // TOTAL HOT ROLLED BARS
    116, // TOTAL COLD FINISHED BARS
    117, // TOTAL BLACK BARS
    118, // TOTAL BARS
    119, // TOTAL WIRE ROD
    120, // TOTAL SEMIFINISHED PRODUCTS
    121, // TOTAL STAINLESS
    122, // TOTAL BARS IN NICHEL ALLOYS
    123, // TOTAL WIRE ROD IN NICHEL ALLOYS
    124, // TOTAL SEMIFINISHED PRODUCTS IN NICHEL ALLOYS
    125, // TOTAL IN NICHEL ALLOY
    126, // TOTAL VALVE STEELS
    127, // TOTAL OTHER PRODUCTS (OF THE GROUP)
    128, // TOTAL OTHER PRODUCTS (OUT OF THE GROUP)
    129, // TOTAL SALES FROM MILL (SUBSIDIARY INVOICE)
  ));
  $pdf->printSubsidiaryYtdTable(10,140,190,'Sales From Mills (Mill Invoice)',$data['january'].' to '.$data['month_name'].' '.$data['year'],$idSubsidiary,7,array_keys(api_period_range($data['year'].'01',$data['period'])),array(
    130, // TOTAL HOT ROLLED BARS
    131, // TOTAL COLD FINISHED BARS
    132, // TOTAL BLACK BARS
    133, // TOTAL BARS
    134, // TOTAL WIRE ROD
    135, // TOTAL SEMIFINISHED PRODUCTS
    136, // TOTAL STAINLESS
    137, // TOTAL BARS IN NICHEL ALLOYS
    138, // TOTAL WIRE ROD IN NICHEL ALLOYS
    139, // TOTAL SEMIFINISHED PRODUCTS IN NICHEL ALLOYS
    140, // TOTAL IN NICHEL ALLOY
    141, // TOTAL VALVE STEELS
    142, // TOTAL OTHER PRODUCTS (OF THE GROUP)
    143, // TOTAL OTHER PRODUCTS (OUT OF THE GROUP)
    144, // TOTAL SALES FROM MILL (MILL INVOICE)
  ));
  $pdf->printSubsidiaryYtdTable(10,250,190,'Total Sales',$data['january'].' to '.$data['month_name'].' '.$data['year'],$idSubsidiary,7,array_keys(api_period_range($data['year'].'01',$data['period'])),array(
    114, // TOTAL SALES FROM STOCK
    145, // TOTAL SALES FROM MILL
    146, // TOTAL SALES
  ));
  $pdf->printPageFooter();
}
/**
 * PDF Output
 */
if(!DEBUG){$pdf->Output('dossier_'.date('YmdHis').'.pdf', 'I');}
