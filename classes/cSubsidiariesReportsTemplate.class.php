<?php
/**
 * Subsidiaries Reports - Template
 *
 * @package Coordinator\Modules\SubsidiariesReports
 * @company Cogne Acciai Speciali s.p.a
 */

/**
 * Subsidiaries Reports, Template class
 */
class cSubsidiariesReportsTemplate extends cObject{

  /** Parameters */
  static protected $table="subsidiaries_reports__templates";
  static protected $logs=true;

  /** Properties */
  protected $id;
  protected $deleted;
  protected $name;
  protected $description;

  /**
   * Check
   *
   * @return boolean
   * @throws Exception
   */
  protected function check(){
    // check properties
    if(!strlen(trim($this->name))){throw new Exception("Template, name is mandatory..");}
    // return
    return true;
  }

  /**
   * Decode log properties
   *
   * {@inheritdoc}
   */
  public static function log_decode($event,$properties){
    // make return array
    $return_array=array();
    // subobject events
    if($properties["_obj"]=="cSubsidiariesReportsTemplateEntry"){$return_array[]=api_text($properties["_obj"]).": ".(new cSubsidiariesReportsTemplateEntry($properties["_id"]))->name;}
    // return
    return implode(" | ",$return_array);
  }

  /**
   * Get Entries
   *
   * @param boolean $deleted Get also deleted entries
   *
   * @return object[]|false Array of entries objects or false
   */
  public function getEntries($deleted=false){return api_sortObjectsArray(cSubsidiariesReportsTemplateEntry::availables($deleted,["fkTemplate"=>$this->id]),"order",false);}

  /**
   * Get Reports
   *
   * @param boolean $deleted Get also deleted reports
   *
   * @return object[]|false Array of reports objects or false
   */
  public function getReports($deleted=false){return cSubsidiariesReportsReport::availables($deleted,["fkTemplate"=>$this->id]);}

  /**
   * Edit form
   *
   * @param string[] $additional_parameters Array of url additional parameters
   * @return object Form structure
   */
  public function form_edit(array $additional_parameters=array()){
    // build form
    $form=new strForm(api_url(array_merge(["mod"=>"subsidiaries_reports","scr"=>"controller","act"=>"store","obj"=>"cSubsidiariesReportsTemplate","idTemplate"=>$this->id],$additional_parameters)),"POST",null,null,"subsidiaries_reports__template-edit_form");
    // fields
    $form->addField("text","name",api_text("cSubsidiariesReportsTemplate-property-name"),$this->name,api_text("cSubsidiariesReportsTemplate-placeholder-name"),null,null,null,"required");
    $form->addField("textarea","description",api_text("cSubsidiariesReportsTemplate-property-description"),$this->description,api_text("cSubsidiariesReportsTemplate-placeholder-description"),null,null,null,"rows='2'");
    // controls
    $form->addControl("submit",api_text("form-fc-save"));
    // return
    return $form;
  }

  // Disable remove function
  public function remove(){throw new Exception("Template remove function disabled by developer..");}

  // debug
  //protected function event_triggered($event){api_dump($event,static::class." event triggered");}

}
