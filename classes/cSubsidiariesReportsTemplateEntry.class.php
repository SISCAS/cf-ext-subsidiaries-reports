<?php
/**
 * Subsidiaries Reports - Template Entry
 *
 * @package Coordinator\Modules\SubsidiariesReports
 * @company Cogne Acciai Speciali s.p.a
 */

/**
 * Subsidiaries Reports, Template Entry class
 */
class cSubsidiariesReportsTemplateEntry extends cObject{

  /** Parameters */
  static protected $table="subsidiaries_reports__templates__entries";
  static protected $sortable=true;
  static protected $sortingGroups=["fkTemplate"];

  /** Properties */
  protected $id;
  protected $deleted;
  protected $order;
  protected $typology;
  protected $fkTemplate;
  protected $fkUnit;
  protected $fkUnit_2;
  protected $fkUnit_3;
  protected $fkUnit_4;
  protected $name;
  protected $description;
  protected $formula;

  /**
   * Check
   *
   * @return boolean
   * @throws Exception
   */
  protected function check(){
    // check properties
    if(!strlen(trim($this->typology))){throw new Exception("Template entry, typology is mandatory..");}
    if(!strlen(trim($this->fkTemplate))){throw new Exception("Template entry, template key is mandatory..");}
    if(!strlen(trim($this->fkUnit))){throw new Exception("Template entry, unit of measurement key is mandatory..");}
    if(!strlen(trim($this->name))){throw new Exception("Template entry, name is mandatory..");}
    // return
    return true;
  }

  /**
   * Get Typology
   *
   * @return cSubsidiariesReportsTemplateEntryTypology
   */
  public function getTypology(){return new cSubsidiariesReportsTemplateEntryTypology($this->typology);}

  /**
   * Get Template
   *
   * @return cSubsidiariesReportsTemplate
   */
  public function getTemplate(){return new cSubsidiariesReportsTemplate($this->fkTemplate);}

  /**
   * Get Unit
   *
   * @return cSubsidiariesReportsUnit
   */
  public function getUnit(){return new cSubsidiariesReportsUnit($this->fkUnit);}
  public function getUnit_2(){if(!$this->fkUnit_2){return null;}return new cSubsidiariesReportsUnit($this->fkUnit_2);}
  public function getUnit_3(){if(!$this->fkUnit_3){return null;}return new cSubsidiariesReportsUnit($this->fkUnit_3);}
  public function getUnit_4(){if(!$this->fkUnit_4){return null;}return new cSubsidiariesReportsUnit($this->fkUnit_4);}

  /**
   * Parse formula
   * Convert {key} to entry name
   *
   * @return string|false
   */
  public function parseFormula(){
    // check for typology
    if($this->typology!="calculated"){return false;}
    // definitions
    $replace_array=array();
    foreach(cSubsidiariesReportsTemplateEntry::availables(true) as $entry_fobj){$replace_array["{".$entry_fobj->id."}"]=$entry_fobj->name;}
    // replace and return
    return str_replace(array_keys($replace_array),$replace_array,$this->formula);
  }

  /**
   * Edit form
   *
   * @param string[] $additional_parameters Array of url additional parameters
   * @return object Form structure
   */
  public function form_edit(array $additional_parameters=array()){
    // build form
    $form=new strForm(api_url(array_merge(["mod"=>"subsidiaries_reports","scr"=>"controller","act"=>"store","obj"=>"cSubsidiariesReportsTemplateEntry","idEntry"=>$this->id],$additional_parameters)),"POST",null,null,"subsidiaries_reports__template__entry-edit_form");
    // fields
    $form->addField("select","fkTemplate",api_text("cSubsidiariesReportsTemplateEntry-property-fkTemplate"),$this->fkTemplate,api_text("cSubsidiariesReportsTemplateEntry-placeholder-fkTemplate"),null,null,null,"required");
    foreach(cSubsidiariesReportsTemplate::availables($this->exists()) as $template_fobj){$form->addFieldOption($template_fobj->id,$template_fobj->name);}
    $form->addField("select","typology",api_text("cSubsidiariesReportsTemplateEntry-property-typology"),$this->typology,api_text("cSubsidiariesReportsTemplateEntry-placeholder-typology"),null,null,null,"required");
    foreach(cSubsidiariesReportsTemplateEntryTypology::availables() as $typology_fobj){$form->addFieldOption($typology_fobj->code,$typology_fobj->text);}
    $form->addField("select","fkUnit",api_text("cSubsidiariesReportsTemplateEntry-property-fkUnit"),$this->fkUnit,api_text("cSubsidiariesReportsTemplateEntry-placeholder-fkUnit"),null,null,null,"required");
    foreach(cSubsidiariesReportsUnit::availables($this->exists()) as $unit_fobj){$form->addFieldOption($unit_fobj->id,$unit_fobj->getLabel());}
    $form->addField("select","fkUnit_2",api_text("cSubsidiariesReportsTemplateEntry-property-fkUnit_2"),$this->fkUnit_2,api_text("cSubsidiariesReportsTemplateEntry-placeholder-fkUnit_optional"));
    foreach(cSubsidiariesReportsUnit::availables($this->exists()) as $unit_fobj){$form->addFieldOption($unit_fobj->id,$unit_fobj->getLabel());}
    $form->addField("select","fkUnit_3",api_text("cSubsidiariesReportsTemplateEntry-property-fkUnit_3"),$this->fkUnit_3,api_text("cSubsidiariesReportsTemplateEntry-placeholder-fkUnit_optional"));
    foreach(cSubsidiariesReportsUnit::availables($this->exists()) as $unit_fobj){$form->addFieldOption($unit_fobj->id,$unit_fobj->getLabel());}
    $form->addField("select","fkUnit_4",api_text("cSubsidiariesReportsTemplateEntry-property-fkUnit_4"),$this->fkUnit_4,api_text("cSubsidiariesReportsTemplateEntry-placeholder-fkUnit_optional"));
    foreach(cSubsidiariesReportsUnit::availables($this->exists()) as $unit_fobj){$form->addFieldOption($unit_fobj->id,$unit_fobj->getLabel());}
    $form->addField("text","name",api_text("cSubsidiariesReportsTemplateEntry-property-name"),$this->name,api_text("cSubsidiariesReportsTemplateEntry-placeholder-name"),null,null,null,"required");
    $form->addField("text","description",api_text("cSubsidiariesReportsTemplateEntry-property-description"),$this->description,api_text("cSubsidiariesReportsTemplateEntry-placeholder-description"));
    $form->addField("textarea","formula",api_text("cSubsidiariesReportsTemplateEntry-property-formula"),$this->formula,api_text("cSubsidiariesReportsTemplateEntry-placeholder-formula"),null,null,null,"rows='3'");
    // controls
    $form->addControl("submit",api_text("form-fc-save"));
    // return
    return $form;
  }

  /**
   * Store
   *
   * {@inheritdoc}
   */
  public function store(array $properties,$log=true){
    // clear formula for fillable entries
    if($properties["typology"]=="fillable"){$properties["formula"]=null;}else{$properties["formula"]=trim($properties["formula"]);}
    // call parent and return
    return parent::store($properties,$log);
  }

  /**
   * Event Triggered
   *
   * {@inheritdoc}
   */
  protected function event_triggered($event){
    // debug
    //api_dump($event,static::class." event triggered");
    // skip trace events
    if($event->typology=="trace"){return;}
    // log event to template
    $this->getTemplate()->event_log($event->typology,$event->action,array_merge(["_obj"=>"cSubsidiariesReportsTemplateEntry","_id"=>$this->id,"_fkTemplate"=>$this->fkTemplate],$event->properties));
  }

  // Disable remove function
  public function remove(){throw new Exception("Template entry remove function disabled by developer..");}

}
