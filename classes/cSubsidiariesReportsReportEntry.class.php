<?php
/**
 * Subsidiaries Reports - Report Entry
 *
 * @package Coordinator\Modules\SubsidiariesReports
 * @company Cogne Acciai Speciali s.p.a
 */

/**
 * Subsidiaries Reports, Report Entry class
 */
class cSubsidiariesReportsReportEntry extends cObject{

  /** Parameters */
  static protected $table="subsidiaries_reports__reports__entries";

  /** Properties */
  protected $id;
  protected $deleted;
  protected $fkReport;
  protected $fkTemplateEntry;

  /**
   * Check
   *
   * @return boolean
   * @throws Exception
   */
  protected function check(){
    // check properties
    if(!strlen(trim($this->fkReport))){throw new Exception("Report entry, report key is mandatory..");}
    if(!strlen(trim($this->fkTemplateEntry))){throw new Exception("Report entry, template entry key is mandatory..");}
    // return
    return true;
  }

  /**
   * Get Name
   *
   * @return string Entry name (from template entry)
   */
  public function getName($popup_description=false,$inline_description=false,$new_line=false){
    // get template entry
    $template_entry=$this->getTemplateEntry();
    // check for popup and description
    if($popup_description && ($template_entry->description || $template_entry->formula)){
      // return name with popup
      return api_link("#",$template_entry->name,$template_entry->description." ".$template_entry->parseFormula(),"hidden-link",true);
    }else{
      // return textual name
      return $template_entry->name.($inline_description ? ($new_line ? '<br>' : ' ').$template_entry->description : null);
    }
  }

  /**
   * Get Unit
   *
   * @return cSubsidiariesReportsUnit Entry unit (from template entry)
   */
  public function getUnit(){return $this->getTemplateEntry()->getUnit();}
  public function getUnit_2(){return $this->getTemplateEntry()->getUnit_2();}
  public function getUnit_3(){return $this->getTemplateEntry()->getUnit_3();}
  public function getUnit_4(){return $this->getTemplateEntry()->getUnit_4();}

  /**
   * Get Report
   *
   * @return cSubsidiariesReportsReport
   */
  public function getReport(){return new cSubsidiariesReportsReport($this->fkReport);}

  /**
   * Get Template Entry
   *
   * @return cSubsidiariesReportsTemplateEntry
   */
  public function getTemplateEntry(){return new cSubsidiariesReportsTemplateEntry($this->fkTemplateEntry);}

  /**
   * Get Template Typology
   *
   * @return cSubsidiariesReportsTemplateEntryTypology
   */
  public function getTemplateTypology(){return $this->getTemplateEntry()->getTypology();}

  /**
   * Get Values
   *
   * @param boolean $deleted Get also deleted entries
   *
   * @return object[]|false Array of entries objects or false
   */
  public function getValues($deleted=false){return cSubsidiariesReportsReportEntryValue::availables($deleted,["fkReportEntry"=>$this->id]);}

  /**
   * Check if is fillable
   *
   * @return boolean
   */
  public function isFillable(){if($this->getTemplateEntry()->typology=="fillable"){return true;}else{return false;}}

  /**
   * Check if is calculated
   *
   * @return boolean
   */
  public function isCalculated(){if($this->getTemplateEntry()->typology=="calculated"){return true;}else{return false;}}

  /**
   * Edit form
   *
   * @param string[] $additional_parameters Array of url additional parameters
   * @return object Form structure
   */
  public function form_edit(array $additional_parameters=array()){
    // build form
    $form=new strForm(api_url(array_merge(["mod"=>"subsidiaries_reports","scr"=>"controller","act"=>"store","obj"=>"cSubsidiariesReportsReportEntry","idEntry"=>$this->id],$additional_parameters)),"POST",null,null,"subsidiaries_reports__report__entry-edit_form");
    // fields
    $form->addField("select","fkReport",api_text("cSubsidiariesReportsReportEntry-property-fkReport"),$this->fkReport,api_text("cSubsidiariesReportsReportEntry-placeholder-fkReport"),null,null,null,"required");
    foreach(cSubsidiariesReportsReport::availables($this->exists()) as $report_fobj){$form->addFieldOption($report_fobj->id,$report_fobj->getLabel());}
    $form->addField("select","fkTemplateEntry",api_text("cSubsidiariesReportsReportEntry-property-fkTemplateEntry"),$this->fkTemplateEntry,api_text("cSubsidiariesReportsReportEntry-placeholder-fkTemplateEntry"),null,null,null,"required");
    foreach(cSubsidiariesReportsTemplateEntry::availables($this->exists()) as $template_entry_fobj){$form->addFieldOption($template_entry_fobj->id,$template_entry_fobj->name);}
    // controls
    $form->addControl("submit",api_text("form-fc-save"));
    // return
    return $form;
  }

  // Disable remove function
  public function remove(){throw new Exception("Report entry remove function disabled by developer..");}

  // debug
  //protected function event_triggered($event){api_dump($event,static::class." event triggered");}

}
