<?php
/**
 * Subsidiaries Reports - Unit
 *
 * @package Coordinator\Modules\SubsidiariesReports
 * @company Cogne Acciai Speciali s.p.a
 */

/**
 * Subsidiaries Reports, Unit class
 */
class cSubsidiariesReportsUnit extends cObject{

  /** Parameters */
  static protected $table="subsidiaries_reports__units";
  static protected $logs=true;

  /** Properties */
  protected $id;
  protected $deleted;
  protected $code;
  protected $name;

  /**
   * Check
   *
   * @return boolean
   * @throws Exception
   */
  protected function check(){
    // check properties
    if(!strlen(trim($this->code))){throw new Exception("Unit, code is mandatory..");}
    if(!strlen(trim($this->name))){throw new Exception("Unit, name is mandatory..");}
    // return
    return true;
  }

  /**
   * Get Label
   *
   * @return string Unit label
   */
  public function getLabel($popup_name=false){
    if($popup_name){
      return api_link("#",$this->code,$this->name,"hidden-link",true);
    }else{
      return $this->code." (".$this->name.")";
    }
  }

  /**
   * Edit form
   *
   * @param string[] $additional_parameters Array of url additional parameters
   * @return object Form structure
   */
  public function form_edit(array $additional_parameters=array()){
    // build form
    $form=new strForm(api_url(array_merge(["mod"=>"subsidiaries_reports","scr"=>"controller","act"=>"store","obj"=>"cSubsidiariesReportsUnit","idUnit"=>$this->id],$additional_parameters)),"POST",null,null,"subsidiaries_reports__unit-edit_form");
    // fields
    $form->addField("text","code",api_text("cSubsidiariesReportsUnit-property-code"),$this->code,api_text("cSubsidiariesReportsUnit-placeholder-code"),null,null,null,"required");
    $form->addField("text","name",api_text("cSubsidiariesReportsUnit-property-name"),$this->name,api_text("cSubsidiariesReportsUnit-placeholder-name"),null,null,null,"required");
    // controls
    $form->addControl("submit",api_text("form-fc-save"));
    // return
    return $form;
  }

  /**
   * Remove
   *
   * {@inheritdoc}
   */
  public function remove(){
    // check if is used
    if(!cSubsidiariesReportsTemplateEntry::count(true,["fkUnit"=>$this->id])){return parent::remove();}
    else{throw new Exception("Cannot remove unit of measurement already used..");}
  }

  // debug
  //protected function event_triggered($event){api_dump($event,static::class." event triggered");}

}
