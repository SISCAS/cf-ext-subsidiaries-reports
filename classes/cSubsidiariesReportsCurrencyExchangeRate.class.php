<?php
/**
 * Subsidiaries Reports - Currency Exchange Rate
 *
 * @package Coordinator\Modules\SubsidiariesReports
 * @company Cogne Acciai Speciali s.p.a
 */

/**
 * Subsidiaries Reports, Currency Exchange Rate class
 */
class cSubsidiariesReportsCurrencyExchangeRate extends cObject{

  /** Parameters */
  static protected $table="subsidiaries_reports__currencies__exchangerates";

  /** Properties */
  protected $id;
  protected $deleted;
  protected $fkCurrency;
  protected $year;
  protected $avg;
  protected $jan;
  protected $feb;
  protected $mar;
  protected $apr;
  protected $may;
  protected $jun;
  protected $jul;
  protected $aug;
  protected $sep;
  protected $oct;
  protected $nov;
  protected $dec;

  /**
   * Check
   *
   * @return boolean
   * @throws Exception
   */
  protected function check(){
    // check properties
    if(!strlen(trim($this->fkCurrency))){throw new Exception("Currency exchange rate, currency key is mandatory..");}
    if($this->year<2000){throw new Exception("Currency exchange rate, year is mandatory..");}
    if(!strlen(trim($this->value))){throw new Exception("Currency exchange rate, value is mandatory..");}
    // return
    return true;
  }

  /**
   * Load from Currency and Year
   *
   * @param integer $currency Currency ID
   * @param integer $year Year number
   * @return boolean
   */
  public function loadFromCurrencyYear($currency,$year){return parent::loadFromFields(["fkCurrency"=>$currency,"year"=>$year]);}

  /**
   * Get Currency
   *
   * @return cSubsidiariesReportsCurrency
   */
  public function getCurrency(){return new cSubsidiariesReportsCurrency($this->fkCurrency);}

  /**
   * Edit form
   *
   * @param string[] $additional_parameters Array of url additional parameters
   * @return object Form structure
   */
  public function form_edit(array $additional_parameters=array()){
    // build form
    $form=new strForm(api_url(array_merge(["mod"=>"subsidiaries_reports","scr"=>"controller","act"=>"store","obj"=>"cSubsidiariesReportsCurrencyExchangeRate","idCurrencyExchangerate"=>$this->id],$additional_parameters)),"POST",null,null,"subsidiaries_reports__currency__exchangerate-edit_form");
    // fields
    $form->addField("select","fkCurrency",api_text("cSubsidiariesReportsCurrencyExchangeRate-property-fkCurrency"),$this->fkCurrency,api_text("cSubsidiariesReportsCurrencyExchangeRate-placeholder-fkCurrency"),null,null,null,"required");
    foreach(cSubsidiariesReportsCurrency::availables($this->exists()) as $currency_fobj){$form->addFieldOption($currency_fobj->id,$currency_fobj->getLabel());}
    $form->addField("select","year",api_text("cSubsidiariesReportsCurrencyExchangeRate-property-year"),($this->year?$this->year:date("Y")),api_text("cSubsidiariesReportsCurrencyExchangeRate-placeholder-year"),null,null,null,"required");
    for($year=date("Y");$year>=2020;$year--){$form->addFieldOption($year,$year);}
    $form->addField("number","avg",api_text("cSubsidiariesReportsCurrencyExchangeRate-property-avg"),$this->avg,api_text("cSubsidiariesReportsCurrencyExchangeRate-placeholder-avg"),null,null,null,"step='0.0001'");
    $form->addField("number","jan",api_text("cSubsidiariesReportsCurrencyExchangeRate-property-jan"),$this->jan,api_text("cSubsidiariesReportsCurrencyExchangeRate-placeholder-jan"),null,null,null,"step='0.0001'");
    $form->addField("number","feb",api_text("cSubsidiariesReportsCurrencyExchangeRate-property-feb"),$this->feb,api_text("cSubsidiariesReportsCurrencyExchangeRate-placeholder-feb"),null,null,null,"step='0.0001'");
    $form->addField("number","mar",api_text("cSubsidiariesReportsCurrencyExchangeRate-property-mar"),$this->mar,api_text("cSubsidiariesReportsCurrencyExchangeRate-placeholder-mar"),null,null,null,"step='0.0001'");
    $form->addField("number","apr",api_text("cSubsidiariesReportsCurrencyExchangeRate-property-apr"),$this->apr,api_text("cSubsidiariesReportsCurrencyExchangeRate-placeholder-apr"),null,null,null,"step='0.0001'");
    $form->addField("number","may",api_text("cSubsidiariesReportsCurrencyExchangeRate-property-may"),$this->may,api_text("cSubsidiariesReportsCurrencyExchangeRate-placeholder-may"),null,null,null,"step='0.0001'");
    $form->addField("number","jun",api_text("cSubsidiariesReportsCurrencyExchangeRate-property-jun"),$this->jun,api_text("cSubsidiariesReportsCurrencyExchangeRate-placeholder-jun"),null,null,null,"step='0.0001'");
    $form->addField("number","jul",api_text("cSubsidiariesReportsCurrencyExchangeRate-property-jul"),$this->jul,api_text("cSubsidiariesReportsCurrencyExchangeRate-placeholder-jul"),null,null,null,"step='0.0001'");
    $form->addField("number","aug",api_text("cSubsidiariesReportsCurrencyExchangeRate-property-aug"),$this->aug,api_text("cSubsidiariesReportsCurrencyExchangeRate-placeholder-aug"),null,null,null,"step='0.0001'");
    $form->addField("number","sep",api_text("cSubsidiariesReportsCurrencyExchangeRate-property-sep"),$this->sep,api_text("cSubsidiariesReportsCurrencyExchangeRate-placeholder-sep"),null,null,null,"step='0.0001'");
    $form->addField("number","oct",api_text("cSubsidiariesReportsCurrencyExchangeRate-property-oct"),$this->oct,api_text("cSubsidiariesReportsCurrencyExchangeRate-placeholder-oct"),null,null,null,"step='0.0001'");
    $form->addField("number","nov",api_text("cSubsidiariesReportsCurrencyExchangeRate-property-nov"),$this->nov,api_text("cSubsidiariesReportsCurrencyExchangeRate-placeholder-nov"),null,null,null,"step='0.0001'");
    $form->addField("number","dec",api_text("cSubsidiariesReportsCurrencyExchangeRate-property-dec"),$this->dec,api_text("cSubsidiariesReportsCurrencyExchangeRate-placeholder-dec"),null,null,null,"step='0.0001'");
    $form->addFieldAddon($this->getCurrency()->code,"append");
    // controls
    $form->addControl("submit",api_text("form-fc-save"));
    // return
    return $form;
  }

  /**
   * Event Triggered
   *
   * {@inheritdoc}
   */
  protected function event_triggered($event){
    //api_dump($event,static::class." event triggered");
    // skip trace events
    if($event->typology=="trace" || !$event->logged){return;}
    // log event to subsidiary
    $this->getCurrency()->event_log($event->typology,$event->action,array_merge(["_obj"=>"cSubsidiariesReportsCurrencyExchangeRate","_id"=>$this->id,"_fkCurrency"=>$this->fkCurrency,"_year"=>$this->year],$event->properties));
  }

  // Disable remove function
  public function remove(){throw new Exception("Report currency value remove function disabled by developer..");}

}
