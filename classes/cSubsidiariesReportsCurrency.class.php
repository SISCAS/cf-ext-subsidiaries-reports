<?php
/**
 * Subsidiaries Reports - Currency
 *
 * @package Coordinator\Modules\SubsidiariesReports
 * @company Cogne Acciai Speciali s.p.a
 */

/**
 * Subsidiaries Reports, Currency class
 */
class cSubsidiariesReportsCurrency extends cObject{

  /** Parameters */
  static protected $table="subsidiaries_reports__currencies";
  static protected $logs=true;

  /** Properties */
  protected $id;
  protected $deleted;
  protected $code;
  protected $name;

  /**
   * Check
   *
   * @return boolean
   * @throws Exception
   */
  protected function check(){
    // check properties
    if(!strlen(trim($this->code))){throw new Exception("Currency, code is mandatory..");}
    if(!strlen(trim($this->name))){throw new Exception("Currency, name is mandatory..");}
    // return
    return true;
  }

  /**
   * Decode log properties
   *
   * {@inheritdoc}
   */
  public static function log_decode($event,$properties){
    // make return array
    $return_array=array();
    // subobject events
    if($properties["_obj"]=="cSubsidiariesReportsCurrencyExchangeRate"){
      $return_array[]=$properties["_year"];
      if($properties["value"]){$return_array[]=api_text("cSubsidiariesReportsReportEntryValue-property-value").": ".$properties["value"]["previous"]." &rarr; ".$properties["value"]["current"];}
    }
    // return
    return implode(" | ",$return_array);
  }

  /**
   * Get Label
   *
   * @return string Currency label
   */
  public function getLabel(){return $this->code." (".$this->name.")";}

  /**
   * Get Exchange Rates
   *
   * @param boolean $deleted Get also deleted entries
   * @return object[]|false Array of entries objects or false
   */
  public function getExchangeRates($deleted=false){return api_sortObjectsArray(cSubsidiariesReportsCurrencyExchangeRate::availables($deleted,["fkCurrency"=>$this->id]),"year",true);}

  /**
   * Get Exchange Rates by Year
   *
   * @param integer $year Year of echange rates
   * @param boolean $deleted Get also deleted entries
   * @return object[]|false Array of entries objects or false
   */
  public function getYearExchangeRates($year,$deleted=false){foreach(cSubsidiariesReportsCurrencyExchangeRate::availables($deleted,["fkCurrency"=>$this->id,"year"=>$year]) as $exchangerate){return $exchangerate;}}

  /**
   * Edit form
   *
   * @param string[] $additional_parameters Array of url additional parameters
   * @return object Form structure
   */
  public function form_edit(array $additional_parameters=array()){
    // build form
    $form=new strForm(api_url(array_merge(["mod"=>"subsidiaries_reports","scr"=>"controller","act"=>"store","obj"=>"cSubsidiariesReportsCurrency","idCurrency"=>$this->id],$additional_parameters)),"POST",null,null,"subsidiaries_reports__currency-edit_form");
    // fields
    $form->addField("text","code",api_text("cSubsidiariesReportsCurrency-property-code"),$this->code,api_text("cSubsidiariesReportsCurrency-placeholder-code"),null,null,null,"required");
    $form->addField("text","name",api_text("cSubsidiariesReportsCurrency-property-name"),$this->name,api_text("cSubsidiariesReportsCurrency-placeholder-name"),null,null,null,"required");
    // controls
    $form->addControl("submit",api_text("form-fc-save"));
    // return
    return $form;
  }

  /**
   * Remove
   *
   * {@inheritdoc}
   */
  public function remove(){
    // check if is used
    if(!cSubsidiariesReportsTemplateEntry::count(true,["fkCurrency"=>$this->id])){return parent::remove();}
    else{throw new Exception("Cannot remove currency of measurement already used..");}
  }

  // debug
  //protected function event_triggered($event){api_dump($event,static::class." event triggered");}

}
