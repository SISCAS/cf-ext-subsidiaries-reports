<?php
/**
 * Subsidiaries Reports - Report Entry Value
 *
 * @package Coordinator\Modules\SubsidiariesReports
 * @company Cogne Acciai Speciali s.p.a
 */

/**
 * Subsidiaries Reports, Report Entry Value class
 */
class cSubsidiariesReportsReportEntryValue extends cObject{

  /** Parameters */
  static protected $table="subsidiaries_reports__reports__entries__values";

  /** Properties */
  protected $id;
  protected $deleted;
  protected $fkReportEntry;
  protected $period;
  protected $budget;
  protected $budget_2;
  protected $budget_3;
  protected $budget_4;
  protected $value;
  protected $value_2;
  protected $value_3;
  protected $value_4;

  /**
   * Check
   *
   * @return boolean
   * @throws Exception
   */
  protected function check(){
    // check properties
    if(!strlen(trim($this->fkReportEntry))){throw new Exception("Report entry value, report entry key is mandatory..");}
    if(!api_period_check($this->period)){throw new Exception("Report entry value, period is mandatory..");}
    // return
    return true;
  }

  /**
   * Load from Entry and Period
   *
   * @param integer $entry Entry ID
   * @param integer $period Period number
   * @return boolean
   */
  public function loadFromEntryPeriod($entry,$period){return parent::loadFromFields(["fkReportEntry"=>$entry,"period"=>$period]);}

  /**
   * Get Report Entry
   *
   * @return cSubsidiariesReportsReportEntry
   */
  public function getReportEntry(){return new cSubsidiariesReportsReportEntry($this->fkReportEntry);}

  /**
   * Edit form
   *
   * @param string[] $additional_parameters Array of url additional parameters
   * @return object Form structure
   */
  public function form_edit(array $additional_parameters=array(),$selected_entry_obj=null){
    /** @var cSubsidiariesReportsReportEntry $report_entry_obj */
    $report_entry_obj=($selected_entry_obj ?? $this->fkReportEntry);
    // build form
    $form=new strForm(api_url(array_merge(["mod"=>"subsidiaries_reports","scr"=>"controller","act"=>"store","obj"=>"cSubsidiariesReportsReportEntryValue","idReportEntryValue"=>$this->id],$additional_parameters)),"POST",null,null,"subsidiaries_reports__report__entry__value-edit_form");
    // fields
    $form->addField("select","fkReportEntry",api_text("cSubsidiariesReportsReportEntryValue-property-fkReportEntry"),$this->fkReportEntry,api_text("cSubsidiariesReportsReportEntryValue-placeholder-fkReportEntry"),null,null,null,"required");
    foreach(cSubsidiariesReportsReportEntry::availables($this->exists()) as $report_entry_fobj){$form->addFieldOption($report_entry_fobj->id,$report_entry_fobj->getName());}
    $form->addField("select","period",api_text("cSubsidiariesReportsReport-property-period"),($this->period?$this->period:date("Ym",strtotime("-1 month"))),api_text("cSubsidiariesReportsReport-placeholder-period"),null,null,null,"required");
    foreach(api_period_range($this->getReportEntry()->getReport()->year."01",$this->getReportEntry()->getReport()->year."12") as $value=>$label){$form->addFieldOption($value,$label);}
    $form->addField("number","budget",api_text("cSubsidiariesReportsReportEntryValue-property-budget"),$this->budget,api_text("cSubsidiariesReportsReportEntryValue-placeholder-budget"),null,null,null,"step='0.01'");
    $form->addFieldAddon($report_entry_obj->getUnit()->code,"append");
    if($report_entry_obj->getUnit_2()){
      $form->addField("number","budget_2",api_text("cSubsidiariesReportsReportEntryValue-property-budget"),$this->budget_2,api_text("cSubsidiariesReportsReportEntryValue-placeholder-budget"),null,null,null,"step='0.01'");
      $form->addFieldAddon($report_entry_obj->getUnit_2()->code,"append");
    }
    if($report_entry_obj->getUnit_3()){
      $form->addField("number","budget_3",api_text("cSubsidiariesReportsReportEntryValue-property-budget"),$this->budget_3,api_text("cSubsidiariesReportsReportEntryValue-placeholder-budget"),null,null,null,"step='0.01'");
      $form->addFieldAddon($report_entry_obj->getUnit_3()->code,"append");
    }
    if($report_entry_obj->getUnit_4()){
      $form->addField("number","budget_4",api_text("cSubsidiariesReportsReportEntryValue-property-budget"),$this->budget_4,api_text("cSubsidiariesReportsReportEntryValue-placeholder-budget"),null,null,null,"step='0.01'");
      $form->addFieldAddon($report_entry_obj->getUnit_4()->code,"append");
    }
    $form->addField('separator');
    $form->addField("number","value",api_text("cSubsidiariesReportsReportEntryValue-property-value"),$this->value,api_text("cSubsidiariesReportsReportEntryValue-placeholder-value"),null,null,null,"step='0.01'");
    $form->addFieldAddon($report_entry_obj->getUnit()->code,"append");

    if($report_entry_obj->getUnit_2()){
      $form->addField("number","value_2",api_text("cSubsidiariesReportsReportEntryValue-property-value"),$this->value_2,api_text("cSubsidiariesReportsReportEntryValue-placeholder-value"),null,null,null,"step='0.01'");
      $form->addFieldAddon($report_entry_obj->getUnit_2()->code,"append");
    }
    if($report_entry_obj->getUnit_3()){
      $form->addField("number","value_3",api_text("cSubsidiariesReportsReportEntryValue-property-value"),$this->value_3,api_text("cSubsidiariesReportsReportEntryValue-placeholder-value"),null,null,null,"step='0.01'");
      $form->addFieldAddon($report_entry_obj->getUnit_3()->code,"append");
    }
    if($report_entry_obj->getUnit_4()){
      $form->addField("number","value_4",api_text("cSubsidiariesReportsReportEntryValue-property-value"),$this->value_4,api_text("cSubsidiariesReportsReportEntryValue-placeholder-value"),null,null,null,"step='0.01'");
      $form->addFieldAddon($report_entry_obj->getUnit_4()->code,"append");
    }
    // controls
    $form->addControl("submit",api_text("form-fc-save"));
    // return
    return $form;
  }

  /**
   * Event Triggered
   *
   * {@inheritdoc}
   */
  protected function event_triggered($event){
    //api_dump($event,static::class." event triggered");
    // skip trace events
    if($event->typology=="trace" || !$event->logged){return;}
    // log event to subsidiary
    $this->getReportEntry()->getReport()->event_log($event->typology,$event->action,array_merge(["_obj"=>"cSubsidiariesReportsReportEntryValue","_id"=>$this->id,"_fkReportEntry"=>$this->fkReportEntry,"_period"=>$this->period],$event->properties));
  }

  // Disable remove function
  public function remove(){throw new Exception("Report entry value remove function disabled by developer..");}

}
