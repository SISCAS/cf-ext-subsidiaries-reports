<?php
/**
 * Subsidiaries Reports - Report
 *
 * @package Coordinator\Modules\SubsidiariesReports
 * @company Cogne Acciai Speciali s.p.a
 */

/**
 * Subsidiaries Reports, Report class
 */
class cSubsidiariesReportsReport extends cObject{

  /** Parameters */
  static protected $table="subsidiaries_reports__reports";
  static protected $logs=true;

  /** Properties */
  protected $id;
  protected $deleted;
  protected $fkSubsidiary;
  protected $fkTemplate;
  protected $year;
  protected $lock;

  /**
   * Check
   *
   * {@inheritdoc}
   */
  protected function check(){
    // check properties
    if(!strlen(trim($this->fkSubsidiary))){throw new Exception("Report, subsidiary key is mandatory..");}
    if(!strlen(trim($this->fkTemplate))){throw new Exception("Report, template key is mandatory..");}
    if(strlen(trim($this->year))!=4){throw new Exception("Report, year is mandatory..");}
    // return
    return true;
  }

  /**
   * Decode log properties
   *
   * {@inheritdoc}
   */
  public static function log_decode($event,$properties){
    // make return array
    $return_array=array();
    // properties
    if($properties["period"]){$return_array[]=api_period($properties["period"]);}
    if($properties["stored"]!=$properties["total"]){$return_array[]=$properties["stored"]."/".$properties["total"];}
    if($properties["field"]){$return_array[]=api_text("cSubsidiariesReportsReportEntryValue-property-".$properties["field"]);}
    // subobject events
    if($properties["_obj"]=="cSubsidiariesReportsReportEntryValue"){
      $return_array[]=(new cSubsidiariesReportsReportEntry($properties["_fkReportEntry"]))->getName()." ".api_period($properties["_period"]);
      if($properties["budget"]){$return_array[]=api_text("cSubsidiariesReportsReportEntryValue-property-budget").": ".$properties["budget"]["previous"]." &rarr; ".$properties["budget"]["current"];}
      if($properties["value"]){$return_array[]=api_text("cSubsidiariesReportsReportEntryValue-property-value").": ".$properties["value"]["previous"]." &rarr; ".$properties["value"]["current"];}
    }
    // return
    return implode(" | ",$return_array);
  }

  /**
   * Viewable
   * Check if subsidiary report is visible by an user
   *
   * @param null|integer|cUser $user User object or ID
   * @return boolean
   */
  public function viewable($user=null){
    // get user or load from session
    if(!$user){$user_obj=$GLOBALS["session"]->user;}
    else{$user_obj=new cUser($user);}
    // check for super users
    if($user_obj->superuser){return true;}
    // check for module manage authorization
    if(api_checkAuthorization("subsidiaries_reports-view_all")){return true;}
    // cycle all subsidiary members to check if user is assigned
    foreach($this->getSubsidiary()->getMembers() as $member_fobj){if($member_fobj->fkUser==$user_obj->id){return true;}}
    // user not found in subsidiary members
    return false;
  }

  /**
   * Fillable
   * Check if subsidiary report is fillable by an user
   *
   * @param null|integer|cUser $user User object or ID
   * @return boolean
   */
  public function fillable($user=null){
    // get user or load from session
    if(!$user){$user_obj=$GLOBALS["session"]->user;}
    else{$user_obj=new cUser($user);}
    // check for super users
    if($user_obj->superuser){return true;}
    // check for managers
    if(api_checkAuthorization("subsidiaries_reports-edit_all")){return true;}
    // cycle all subsidiary members to check if user is assigned
    foreach($this->getSubsidiary()->getMembers() as $member_fobj){if($member_fobj->fkUser==$user_obj->id){return true;}}
    // user not found in subsidiary members
    return false;
  }

  /**
   * Get Label
   *
   * @return string Report label
   */
  public function getLabel($subsidiary=true){
    $label=$this->getTemplate()->name." ".$this->year;
    if($subsidiary){$label=$this->getSubsidiary()->name." - ".$label;}
    return $label;
  }

  /**
   * Get Template
   *
   * @return cSubsidiariesReportsTemplate
   */
  public function getTemplate(){return new cSubsidiariesReportsTemplate($this->fkTemplate);}

  /**
   * Get Subsidiary
   *
   * @return cSubsidiariesSubsidiary
   */
  public function getSubsidiary(){return new cSubsidiariesSubsidiary($this->fkSubsidiary);}

  /**
   * Get Entries
   *
   * @param boolean $deleted Get also deleted entries
   * @return cSubsidiariesReportsReportEntry[]|false Array of entries objects or false
   */
  public function getEntries($deleted=false){
    $return=array();
    $template_entries=$this->getTemplate()->getEntries(true);
    //api_dump($template_entries,'template entries');
    $report_entries=cSubsidiariesReportsReportEntry::availables($deleted,["fkReport"=>$this->id]);
    //api_dump($report_entries,'report entries');
    foreach(array_keys($template_entries) as $idEntry){
      //api_dump($idEntry);
      foreach($report_entries as $entry){
        if($entry->fkTemplateEntry==$idEntry){
          $return[$entry->id]=$entry;
          continue(2);
        }
      }
    }
    return $return;
  }

  /**
   * Get Datas
   * Return array of budgets and values evaluating all formulas
   *
   * @return array
   */
  public function getDatas(){
    // include eval math library
    require_once(DIR."helpers/evalmath/php/EvalMath.php");
    // definitions
    $return_array=array();
    $replace_formula_array=array();
    $replace_budget_array=array();
    $replace_value_array=array();
    $calculated_budget_array=array();
    $calculated_value_array=array();
    // get all entries
    $report_entries_array=$this->getEntries();
    // cycle all calculated entries
    foreach($report_entries_array as $entry_fobj){
      if(!$entry_fobj->isCalculated()){continue;}
      // make formula replace array
      $replace_formula_array["{".$entry_fobj->fkTemplateEntry."}"]=" ( ".$entry_fobj->getTemplateEntry()->formula." ) ";
    }
    // cycle all fillable entries
    /** @var cSubsidiariesReportsReportEntry $entry_fobj */
    foreach($report_entries_array as $entry_fobj){
      if(!$entry_fobj->isFillable()){continue;}
      // cycle all values
      foreach($entry_fobj->getValues() as $value_fobj){
        // make budget replace array
        if($value_fobj->budget!=null){$replace_budget_array[$value_fobj->period]["{".$entry_fobj->fkTemplateEntry."}"]=$value_fobj->budget;}
        if($value_fobj->budget_2!=null){$replace_budget_2_array[$value_fobj->period]["{".$entry_fobj->fkTemplateEntry."}"]=$value_fobj->budget_2;}
        if($value_fobj->budget_3!=null){$replace_budget_3_array[$value_fobj->period]["{".$entry_fobj->fkTemplateEntry."}"]=$value_fobj->budget_3;}
        if($value_fobj->budget_4!=null){$replace_budget_4_array[$value_fobj->period]["{".$entry_fobj->fkTemplateEntry."}"]=$value_fobj->budget_4;}
        // make value replace array
        if($value_fobj->value!=null){$replace_value_array[$value_fobj->period]["{".$entry_fobj->fkTemplateEntry."}"]=$value_fobj->value;}
        if($value_fobj->value_2!=null){$replace_value_2_array[$value_fobj->period]["{".$entry_fobj->fkTemplateEntry."}"]=$value_fobj->value_2;}
        if($value_fobj->value_3!=null){$replace_value_3_array[$value_fobj->period]["{".$entry_fobj->fkTemplateEntry."}"]=$value_fobj->value_3;}
        if($value_fobj->value_4!=null){$replace_value_4_array[$value_fobj->period]["{".$entry_fobj->fkTemplateEntry."}"]=$value_fobj->value_4;}
        // add budgets and values to return array
        $return_array[$entry_fobj->fkTemplateEntry][$value_fobj->period]=(object)[
          "id"=>$value_fobj->id,
          "budget"=>$value_fobj->budget,
          "budget_2"=>$value_fobj->budget_2,
          "budget_3"=>$value_fobj->budget_3,
          "budget_4"=>$value_fobj->budget_4,
          "value"=>$value_fobj->value,
          "value_2"=>$value_fobj->value_2,
          "value_3"=>$value_fobj->value_3,
          "value_4"=>$value_fobj->value_4
        ];
      }
    }
    // debug
    //api_dump($replace_formula_array,"replace formula array");
    //api_dump($replace_budget_array,"replace budget array");
    //api_dump($replace_value_array,"replace value array");
    // cycle all calculate entries
    foreach($report_entries_array as $entry_fobj){
      // @attenzione #1 questa parte è presente anche nel file dossier-export.php
      if(!$entry_fobj->isCalculated()){continue;}
      // unset
      unset($calculated_budget_array);unset($calculated_budget_2_array);unset($calculated_budget_3_array);unset($calculated_budget_4_array);
      unset($calculated_value_array);unset($calculated_value_2_array);unset($calculated_value_3_array);unset($calculated_value_4_array);
      // get template entry formula
      $formula=$entry_fobj->getTemplateEntry()->formula;
      // replace formulas recursively (max 100 times)
      for($i=0;$i<100;$i++){$formula=str_replace(array_keys($replace_formula_array),$replace_formula_array,$formula);}
      // debug
      //api_dump($formula,"formula ".$entry_fobj->getTemplateEntry()->id);
      // cycle all periods
      for($period=$this->year."01";$period<=$this->year."12";$period++){
        if(!is_array($replace_budget_array[$period])){continue;}
        // definitions
        $values_obj=new stdClass();
        // replace budgets
        if(is_array($replace_budget_array[$period])){$calculated_budget_array[$period]=str_replace(array_keys($replace_budget_array[$period]),$replace_budget_array[$period],$formula);}
        if(is_array($replace_budget_2_array[$period])){$calculated_budget_2_array[$period]=str_replace(array_keys($replace_budget_2_array[$period]),$replace_budget_2_array[$period],$formula);}
        if(is_array($replace_budget_3_array[$period])){$calculated_budget_3_array[$period]=str_replace(array_keys($replace_budget_3_array[$period]),$replace_budget_3_array[$period],$formula);}
        if(is_array($replace_budget_4_array[$period])){$calculated_budget_4_array[$period]=str_replace(array_keys($replace_budget_4_array[$period]),$replace_budget_4_array[$period],$formula);}
        // replace values
        if(is_array($replace_value_array[$period])){$calculated_value_array[$period]=str_replace(array_keys($replace_value_array[$period]),$replace_value_array[$period],$formula);}
        if(is_array($replace_value_2_array[$period])){$calculated_value_2_array[$period]=str_replace(array_keys($replace_value_2_array[$period]),$replace_value_2_array[$period],$formula);}
        if(is_array($replace_value_3_array[$period])){$calculated_value_3_array[$period]=str_replace(array_keys($replace_value_3_array[$period]),$replace_value_3_array[$period],$formula);}
        if(is_array($replace_value_4_array[$period])){$calculated_value_4_array[$period]=str_replace(array_keys($replace_value_4_array[$period]),$replace_value_4_array[$period],$formula);}
        // evaluate
        $math=new EvalMath();
        // set report entry id
        $values_obj->id=$return_array[$entry_fobj->fkTemplateEntry][$period]->id;
        // try to calculate
        if(isset($calculated_budget_array[$period])){try{$values_obj->budget=(new EvalMath())->evaluate($calculated_budget_array[$period]);}catch(Exception $e){$values_obj->budget='NC';}}
        if(isset($calculated_budget_2_array[$period])){try{$values_obj->budget_2=(new EvalMath())->evaluate($calculated_budget_2_array[$period]);}catch(Exception $e){$values_obj->budget_2='NC';}}
        if(isset($calculated_budget_3_array[$period])){try{$values_obj->budget_3=(new EvalMath())->evaluate($calculated_budget_3_array[$period]);}catch(Exception $e){$values_obj->budget_3='NC';}}
        if(isset($calculated_budget_4_array[$period])){try{$values_obj->budget_4=(new EvalMath())->evaluate($calculated_budget_4_array[$period]);}catch(Exception $e){$values_obj->budget_4='NC';}}
        if(isset($calculated_value_array[$period])){try{$values_obj->value=(new EvalMath())->evaluate($calculated_value_array[$period]);}catch(Exception $e){$values_obj->value='NC';}}
        if(isset($calculated_value_2_array[$period])){try{$values_obj->value_2=(new EvalMath())->evaluate($calculated_value_2_array[$period]);}catch(Exception $e){$values_obj->value_2='NC';}}
        if(isset($calculated_value_3_array[$period])){try{$values_obj->value_3=(new EvalMath())->evaluate($calculated_value_3_array[$period]);}catch(Exception $e){$values_obj->value_3='NC';}}
        if(isset($calculated_value_4_array[$period])){try{$values_obj->value_4=(new EvalMath())->evaluate($calculated_value_4_array[$period]);}catch(Exception $e){$values_obj->value_4='NC';}}
        // add values to return array
        $return_array[$entry_fobj->fkTemplateEntry][$period]=$values_obj;
      }

    }
    // debug
    //api_dump($calculated_budget_array,"calculated budget array");
    //api_dump($calculated_value_array,"calculated value array");
    // return
    return $return_array;
  }

  /**
   * Edit form
   *
   * @param string[] $additional_parameters Array of url additional parameters
   * @return object Form structure
   */
  public function form_edit(array $additional_parameters=array()){
    // build form
    $form=new strForm(api_url(array_merge(["mod"=>"subsidiaries_reports","scr"=>"controller","act"=>"store","obj"=>"cSubsidiariesReportsReport","idReport"=>$this->id],$additional_parameters)),"POST",null,null,"subsidiaries_reports__report-edit_form");
    // fields
    $form->addField("select","fkSubsidiary",api_text("cSubsidiariesReportsReport-property-fkSubsidiary"),$this->fkSubsidiary,api_text("cSubsidiariesReportsReport-placeholder-fkSubsidiary"),null,null,null,"required");
    foreach(cSubsidiariesSubsidiary::availables($this->exists()) as $template_fobj){$form->addFieldOption($template_fobj->id,$template_fobj->name);}
    $form->addField("select","fkTemplate",api_text("cSubsidiariesReportsReport-property-fkTemplate"),$this->fkTemplate,api_text("cSubsidiariesReportsReport-placeholder-fkTemplate"),null,null,null,"required");
    foreach(cSubsidiariesReportsTemplate::availables($this->exists()) as $template_fobj){$form->addFieldOption($template_fobj->id,$template_fobj->name);}
    $form->addField("select","year",api_text("cSubsidiariesReportsReport-property-year"),($this->year?$this->year:date("Y")),api_text("cSubsidiariesReportsReport-placeholder-year"),null,null,null,"required");
    for($year=(date("Y")-1);$year<=(date("Y")+1);$year++){$form->addFieldOption($year,$year);}
    // controls
    $form->addControl("submit",api_text("form-fc-save"));
    // return
    return $form;
  }

  /**
   * Import budget or values
   *
   * @param mixed[] $properties Array of properties
   * @param file $file Uploaded file stream
   * @param boolean $log Log event
   * @return boolean
   * @throws Exception
   */
  public function import(array $properties,$file,$log=true){
    // check parameters
    if(!in_array($properties["field"],array("budget","value"))){throw new Exception("Report import, field not defined..");}
    if(!api_uploads_check($file)){throw new Exception("Report import, upload error..");}
    // definitions
    $import_array=array();
    $field_name=$properties["field"];
    // include xlsx library
    require_once(DIR."helpers/simplexlsx/php/SimpleXLSX.php");
    // build xlsx
    $xlsx=new SimpleXLSX($file["tmp_name"],false,DEBUG);
    // debug
    //api_dump($xlsx->sheetNames(),"xslx sheets");
    //api_dump($xlsx->rows(),"xlsx rows");
    // cycle all rows
    foreach($xlsx->rows() as $row=>$columns){
      // skip headers
      if($row==0){continue;}
      // checks columns count and if entry key exists
      if(count($columns)<16){throw new Exception("Report import, row columns count error..");}
      if(!array_key_exists($columns[0],$this->getEntries(true))){throw new Exception("Report import, entry key \"".$columns[0]."\" was not found..");}
      // cycle and get periods for current entry
      for($i=3;$i<=14;$i++){$import_array[$columns[0]][($i-2)][]=round(floatval($columns[$i]),2);}
    }
    // debug
    //api_dump($import_array,"import array");
    // cycle all entries
    foreach($import_array as $fkReportEntry=>$return_array){
      foreach($return_array as $month=>$values){
        // make period
        $period=$this->year.str_pad($month,2,"0",STR_PAD_LEFT);
        // build report entry value properties array
        $report_entry_value_properties=array(
          "fkReportEntry"=>$fkReportEntry,
          "period"=>$period,
          $field_name=>$values[0]
        );
        if(isset($values[1])){$report_entry_value_properties[$field_name."_2"]=$values[1];}
        if(isset($values[2])){$report_entry_value_properties[$field_name."_3"]=$values[2];}
        if(isset($values[3])){$report_entry_value_properties[$field_name."_4"]=$values[3];}
        // debug
        //api_dump($report_entry_value_properties,"entry value properties");die();
        // load and store report entry value
        $report_entry_value_obj=new cSubsidiariesReportsReportEntryValue();
        $report_entry_value_obj->loadFromEntryPeriod($fkReportEntry,$period);
        $report_entry_value_obj->store($report_entry_value_properties,false);
      }
    }
    // throw event
    $this->event("information","imported",["field"=>$field_name],$log);
    // return
    return true;
  }

  /**
   * Fill Values
   *
   * @param mixed[] $properties Array of properties
   * @param boolean $log Log event
   * @return boolean
   * @throws Exception
   */
  public function fill(array $properties,$log=true){
    // check parameters
    if(!is_array($properties["entries"])){throw new Exception("Report fill, entries must be an array..");}
    if(!api_period_check($properties["period"])){throw new Exception("Report fill, invalid period..");}
    // definitions
    $stored_count=0;
    $period=$properties["period"];
    // cycle all entries
    foreach($properties["entries"] as $fkReportEntry=>$value){
      // check if entry id exists
      if(!array_key_exists($fkReportEntry,$this->getEntries(true))){continue;}
      // build report entry value properties array
      $report_entry_value_properties=array(
        "fkReportEntry"=>$fkReportEntry,
        "period"=>$period,
        "value"=>$value
      );
      if(isset($properties["entries_2"][$fkReportEntry])){$report_entry_value_properties["value_2"]=$properties["entries_2"][$fkReportEntry];}
      if(isset($properties["entries_3"][$fkReportEntry])){$report_entry_value_properties["value_3"]=$properties["entries_3"][$fkReportEntry];}
      if(isset($properties["entries_4"][$fkReportEntry])){$report_entry_value_properties["value_4"]=$properties["entries_4"][$fkReportEntry];}
      // debug
      //api_dump($report_entry_value_properties,"entry value properties");die();
      // load and store report entry value
      $report_entry_value_obj=new cSubsidiariesReportsReportEntryValue();
      $report_entry_value_obj->loadFromEntryPeriod($fkReportEntry,$period);
      // check for store result
      if($report_entry_value_obj->store($report_entry_value_properties,false)){$stored_count++;}
    }
    // throw event
    if($stored_count==count($properties["entries"])){$this->event("information","filled",["period"=>$period],$log);}
    else{$this->event("warning","filled",["period"=>$period,"stored"=>$stored_count,"total"=>count($properties["entries"])],$log);}
    // return
    return true;
  }

  // Remove function enabled only in debug mode
  public function remove(){if(!DEBUG){throw new Exception("Report remove function disabled by developer.. (enabled in debug mode)");}else{parent::remove();}}

  // debug
  //protected function event_triggered($event){api_dump($event,static::class." event triggered");}

}
