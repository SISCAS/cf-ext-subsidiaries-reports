<?php
/**
 * Subsidiaries Reports - Management (Units)
 *
 * @package Coordinator\Modules\Subsidiaries
 * @company Cogne Acciai Speciali s.p.a
 *
 * @var strApplication $app
 */

// build units table
$units_table=new strTable(api_text("management-units-tr-unvalued"));
$units_table->addHeader(api_text("cSubsidiariesReportsUnit-property-code"),"nowrap");
$units_table->addHeader(api_text("cSubsidiariesReportsUnit-property-name"),null,"100%");
$units_table->addHeaderAction(api_url(["scr"=>"management","tab"=>"units","act"=>"unit_add"]),"fa-plus",api_text("table-td-add"),null,"text-right");

// cycle all units
foreach(cSubsidiariesReportsUnit::availables(true) as $unit_fobj){
  // build operation button
  $ob=new strOperationsButton();
  $ob->addElement(api_url(["scr"=>"management","tab"=>"units","act"=>"unit_view","idUnit"=>$unit_fobj->id]),"fa-info-circle",api_text("table-td-view"));
  $ob->addElement(api_url(["scr"=>"management","tab"=>"units","act"=>"unit_edit","idUnit"=>$unit_fobj->id]),"fa-pencil",api_text("table-td-edit"),(api_checkAuthorization("subsidiaries_reports-manage")));
  if($unit_fobj->deleted){$ob->addElement(api_url(["scr"=>"controller","act"=>"undelete","obj"=>"cSubsidiariesReportsUnit","idUnit"=>$unit_fobj->id,"return"=>["scr"=>"management","tab"=>"units"]]),"fa-trash-o",api_text("table-td-undelete"),true,api_text("cSubsidiariesReportsUnit-confirm-undelete"));}
  else{$ob->addElement(api_url(["scr"=>"controller","act"=>"delete","obj"=>"cSubsidiariesReportsUnit","idUnit"=>$unit_fobj->id,"return"=>["scr"=>"management","tab"=>"units"]]),"fa-trash",api_text("table-td-delete"),true,api_text("cSubsidiariesReportsUnit-confirm-delete"));}
  // make table row class
  $tr_class_array=array();
  if($unit_fobj->id==$_REQUEST["idUnit"]){$tr_class_array[]="currentrow";}
  if($unit_fobj->deleted){$tr_class_array[]="deleted";}
  // make subsidiaries row
  $units_table->addRow(implode(" ",$tr_class_array));
  $units_table->addRowField($unit_fobj->code,"nowrap");
  $units_table->addRowField($unit_fobj->name,"truncate-ellipsis");
  $units_table->addRowField($ob->render(),"nowrap text-right");
}

// check for view action
if(ACTION=="unit_view"){
  // get selected unit
  $selected_unit_obj=new cSubsidiariesReportsUnit($_REQUEST["idUnit"]);
  // build description list
  $dl=new strDescriptionList("br","dl-horizontal");
  $dl->addElement(api_text("cSubsidiariesReportsUnit-property-code"),$selected_unit_obj->code);
  $dl->addElement(api_text("cSubsidiariesReportsUnit-property-name"),api_tag("strong",$selected_unit_obj->name));
  // build modal
  $modal=new strModal(api_text("management-units-modal-title"),null,"management-units-view");
  $modal->setBody($dl->render()."<hr>".api_logs_table($selected_unit_obj->getLogs((!$_REQUEST["all_logs"]?10:null)))->render());
  // add modal to application
  $app->addModal($modal);
  // modal scripts
  $app->addScript("$(function(){\$('#modal_management-units-view').modal();});");
}

// check for add or edit actions
if(in_array(ACTION,["unit_add","unit_edit"]) && api_checkAuthorization("subsidiaries_reports-manage")){
  // get selected unit
  $selected_unit_obj=new cSubsidiariesReportsUnit($_REQUEST["idUnit"]);
  // get form
  $form=$selected_unit_obj->form_edit(["return"=>["scr"=>"management","tab"=>"units"]]);
  // additional controls
  $form->addControl("button",api_text("form-fc-cancel"),"#",null,null,null,"data-dismiss='modal'");
  if($selected_unit_obj->exists()){$form->addControl("button",api_text("form-fc-remove"),api_url(["scr"=>"controller","act"=>"remove","obj"=>"cSubsidiariesReportsUnit","idUnit"=>$selected_unit_obj->id,"return"=>["scr"=>"management","tab"=>"units"]]),"btn-danger",api_text("cSubsidiariesReportsUnit-confirm-remove"));}
  // build modal
  $modal=new strModal(api_text("management-units-modal-title"),null,"management-units-edit");
  $modal->setBody($form->render(1));
  // add modal to application
  $app->addModal($modal);
  // modal scripts
  $app->addScript("$(function(){\$('#modal_management-units-edit').modal({show:true,backdrop:'static',keyboard:false});});");
}
