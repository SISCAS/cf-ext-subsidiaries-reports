<?php
/**
 * Subsidiaries Reports - Dossier
 *
 * @package Coordinator\Modules\SubsidiariesReports
 * @company Cogne Acciai Speciali s.p.a
 */

// check authorizations
api_checkAuthorization(["subsidiaries_reports-usage","subsidiaries_reports-view_all"],"dashboard");
// check and generate report
if(isset($_REQUEST['report']) && isset($_REQUEST['year']) && isset($_REQUEST['month']) && isset($_REQUEST['subsidiary'])){
  include('dossier-'.$_REQUEST['report'].'.inc.php');
  dossier_generator($_REQUEST['year'],$_REQUEST['month'],$_REQUEST['subsidiary']);
}else{dossier_selector();}

/**
 * Selector
 */
function dossier_selector(){
  // include module template
  require_once(MODULE_PATH.'template.inc.php');
  /** @var strApplication $app */
  $app->setTitle(api_text('reports_edit-new'));
  // build form
  $form=new strForm('index.php','GET',null,'target="_blank"','subsidiaries_reports__dossier-select_form');
  $form->addField('hidden','mod',null,'subsidiaries_reports');
  $form->addField('hidden','scr',null,'dossier');
  $form->addField('select','report',api_text('dossier-report'),null,api_text('dossier-report-placeholder'),null,null,null,'required');
  foreach(['coge'=>'COGE','sales'=>'SALES'] as $code=>$label){$form->addFieldOption($code,$label);}
  $form->addField('select','year',api_text('dossier-year'),(date('n')==1?date('Y')-1:date('Y')),api_text('dossier-year-placeholder'),null,null,null,'required');
  for($year=(date('Y')+(date('n')>9?1:0));$year>=2020;$year--){$form->addFieldOption($year,$year);}
  $form->addField('select','month',api_text('dossier-month'),api_calendar_previous_month(date('n')),api_text('dossier-month-placeholder'),null,null,null,'required');
  foreach(api_calendar_months() as $month_number=>$month_name){$form->addFieldOption($month_number,api_text($month_name));}
  $form->addField('select','subsidiary',api_text('dossier-subsidiary'),null,api_text('dossier-subsidiary-placeholder'),null,null,null,'required');
  $form->addFieldOption('*',api_text('dossier-subsidiary-all'));
  foreach(cSubsidiariesSubsidiary::availables() as $subsidiary_fobj){
    $show=false;
    if(api_checkAuthorization('subsidiaries_reports-view_all')){$show=true;}
    else{foreach($subsidiary_fobj->getMembers() as $member_fobj){if($member_fobj->fkUser==$GLOBALS['session']->user->id){$show=true;break;}}}
    //if($subsidiary_fobj->getTypology()->code=='administrative'){$show=false;}
    if($show){$form->addFieldOption($subsidiary_fobj->id,$subsidiary_fobj->getlabel());}
  }
  $form->addControl('submit',api_text('dossier-submit'));
  // build grid
  $grid=new strGrid();
  $grid->addRow();
  $grid->addCol($form->render(),'col-xs-12');
  // add content to application
  $app->addContent($grid->render());
  // renderize application
  $app->render();
}
