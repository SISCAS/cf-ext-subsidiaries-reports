<?php
/**
 * Subsidiaries Reports - SALES Dossier
 *
 * @package Coordinator\Modules\SubsidiariesReports
 * @company Cogne Acciai Speciali s.p.a
 */

function dossier_generator($year,$month,$subsidiary){
  // build data
  global $data;
  $data=array();
  // set period
  $data['year']=$year;
  $data['month']=$month;
  $data['period']=$year.str_pad($month,2,'0',STR_PAD_LEFT);
  $data['january']=ucfirst(api_calendar_months()[1]);
  $data['month_name']=ucfirst(api_calendar_months()[$month]);
  //api_dump($year.$month,'Period');
  //api_dump($subsidiary,'Subsidiary');
  // get subsidiaries
  $subsidiaries_array=array();
  $commercial_subsidiaries_array=array();
  $productive_subsidiaries_array=array();
  foreach(cSubsidiariesSubsidiary::availables() as $subsidiary_fobj){
    if($subsidiary!='*' && $subsidiary!=$subsidiary_fobj->id){continue;}
    if($subsidiary=='*' && !api_checkAuthorization('subsidiaries_reports-view_all')){continue;}
    if(!$subsidiary_fobj->hasMember($GLOBALS['session']->user->id) && !api_checkAuthorization('subsidiaries_reports-view_all')){continue;}
    if($subsidiary_fobj->getTypology()->code=='administrative' && $subsidiary_fobj->short!='EX CDI'){continue;}
    $subsidiary_obj=new stdClass();
    $subsidiary_obj->name=$subsidiary_fobj->name;
    $subsidiary_obj->description=$subsidiary_fobj->description;
    $subsidiary_obj->short=$subsidiary_fobj->short;
    $subsidiaries_array[$subsidiary_fobj->id]=$subsidiary_obj;
    if($subsidiary_fobj->typology=='commercial'){$commercial_subsidiaries_array[]=$subsidiary_fobj->id;}
    if($subsidiary_fobj->typology=='productive'){$productive_subsidiaries_array[]=$subsidiary_fobj->id;}
  }
  $data['subsidiaries']=$subsidiaries_array;
  $data['subsidiaries_commercial']=$commercial_subsidiaries_array;
  $data['subsidiaries_productive']=$productive_subsidiaries_array;
  //api_dump($subsidiaries_array,'Subsidiaries');
  // check subsidiaries
  if(!count($subsidiaries_array)){api_alerts_add(api_text("cSubsidiariesReportsReport-alert-denied"),"warning");api_redirect(api_url(["scr"=>"dossier"]));}
  // get units
  $units_array=array();
  foreach(cSubsidiariesReportsUnit::availables() as $unit_fobj){$units_array[$unit_fobj->id]=$unit_fobj->code;}
  $data['units']=$units_array;
  //api_dump($units_array,'units of measurements');
  // get templates
  $templates_array=array();
  $templates_array[7]=(new cSubsidiariesReportsTemplate(7))->name;
  $data['templates']=$templates_array;
  //api_dump($templates_array,'templates');
  // get templates entries
  $templates_entries_array=array();
  foreach(array_keys($templates_array) as $template_id){
    foreach((new cSubsidiariesReportsTemplate($template_id))->getEntries() as $entry_fobj){
      //api_dump($entry_fobj);
      $entry=new stdClass();
      $entry->name=$entry_fobj->name;
      $entry->typology=$entry_fobj->typology;
      $entry->unit_1=$data['units'][$entry_fobj->fkUnit];
      $entry->unit_2=$data['units'][$entry_fobj->fkUnit_2];
      $entry->unit_3=$data['units'][$entry_fobj->fkUnit_3];
      $entry->unit_4=$data['units'][$entry_fobj->fkUnit_4];
      $templates_entries_array[$template_id][$entry_fobj->id]=$entry;
    }
  }
  // keys: template, entry
  $data['templates_entries']=$templates_entries_array;
  //api_dump($templates_entries_array,'template entries');
  // get reports
  $reports_array=array();
  foreach($subsidiaries_array as $subsidiary_id=>$subsidiary_obj){
    foreach($templates_array as $template_id=>$template_name){
      foreach(cSubsidiariesReportsReport::availables(false,['fkSubsidiary'=>$subsidiary_id,'fkTemplate'=>$template_id,'year'=>$year]) as $report_fobj){
        $reports_array[$subsidiary_id][$template_id][$report_fobj->id]=$subsidiary_obj->name.' - '.$template_name.' '.$year;
      }
    }
  }
  // keys: subsidiaries, template, report
  $data['reports']=$reports_array;
  //api_dump($reports_array,'reports');
  // get reports entries
  $reports_entries_array=array();
  foreach(array_keys($subsidiaries_array) as $subsidiary_id){
    foreach(array_keys($templates_array) as $template_id){
      if(is_array($reports_array[$subsidiary_id][$template_id])){
        foreach(array_keys($reports_array[$subsidiary_id][$template_id]) as $report_id){
          foreach((new cSubsidiariesReportsReport($report_id))->getDatas() as $template_entry_id=>$template_entry_datas){
            foreach($template_entry_datas as $period=>$data_fobj){
              //var_dump($data_fobj);
              $reports_entries_array[$subsidiary_id][$template_id][$template_entry_id][$period]=(object)array(
                'budget_1'=>$data_fobj->budget,
                'budget_2'=>$data_fobj->budget_2,
                'budget_3'=>$data_fobj->budget_3,
                'budget_4'=>$data_fobj->budget_4,
                'actual_1'=>$data_fobj->value,
                'actual_2'=>$data_fobj->value_2,
                'actual_3'=>$data_fobj->value_3,
                'actual_4'=>$data_fobj->value_4
              );
            }
          }
        }
      }
    }
  }
  // keys: subsidiaries, template, entry, period
  $data['reports_entries']=$reports_entries_array;
  //api_dump($reports_entries_array,'report entries datas');
  // total data debug
  //api_dump($data,'data');
  //var_dump($data);
  // include exporter
  include(DIR."modules/subsidiaries_reports/dossier-sales-export.inc.php");
}
