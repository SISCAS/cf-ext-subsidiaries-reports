<?php
/**
 * Subsidiaries Reports - Dashboard
 *
 * @package Coordinator\Modules\SubsidiariesReports
 * @company Cogne Acciai Speciali s.p.a
 */

// redirect to reports list
api_redirect(api_url(["scr"=>"reports_list"]));