<?php
/**
 * Subsidiaries Reports -COGE Dossier
 *
 * @package Coordinator\Modules\SubsidiariesReports
 * @company Cogne Acciai Speciali s.p.a
 */

function dossier_generator($year,$month,$subsidiary){
  // build data
  global $data;
  $data=array();
  // set period
  $data['year']=$year;
  $data['month']=$month;
  $data['period']=$year.str_pad($month,2,'0',STR_PAD_LEFT);
  $data['month_name']=ucfirst(api_calendar_months()[$month]);
  //api_dump($p_year.$p_month,'Period');
  // get subsidiaries
  $subsidiaries_array=array();
  $commercial_subsidiaries_array=array();
  $productive_subsidiaries_array=array();
  //var_dump($subsidiary);
  foreach(cSubsidiariesSubsidiary::availables() as $subsidiary_fobj){
    if($subsidiary!='*' && $subsidiary!=$subsidiary_fobj->id){continue;}
    if($subsidiary=='*' && !api_checkAuthorization('subsidiaries_reports-view_all')){continue;}
    if(!$subsidiary_fobj->hasMember($GLOBALS['session']->user->id) && !api_checkAuthorization('subsidiaries_reports-view_all')){continue;}
    if($subsidiary_fobj->getTypology()->code=='administrative' && $subsidiary_fobj->short!='EX CDI'){continue;}
    $subsidiary_obj=new stdClass();
    $subsidiary_obj->name=$subsidiary_fobj->name;
    $subsidiary_obj->description=$subsidiary_fobj->description;
    $subsidiary_obj->short=$subsidiary_fobj->short;
    $subsidiaries_array[$subsidiary_fobj->id]=$subsidiary_obj;
    if($subsidiary_fobj->typology=='commercial'){$commercial_subsidiaries_array[]=$subsidiary_fobj->id;}
    if($subsidiary_fobj->typology=='productive'){$productive_subsidiaries_array[]=$subsidiary_fobj->id;}
  }
  $data['subsidiaries']=$subsidiaries_array;
  $data['subsidiaries_commercial']=$commercial_subsidiaries_array;
  $data['subsidiaries_productive']=$productive_subsidiaries_array;
  //api_dump($subsidiaries_array);
  // check subsidiaries
  if(!count($subsidiaries_array)){api_alerts_add(api_text("cSubsidiariesReportsReport-alert-denied"),"warning");api_redirect(api_url(["scr"=>"dossier"]));}
  // get units
  $units_array=array();
  foreach(cSubsidiariesReportsUnit::availables() as $unit_fobj){$units_array[$unit_fobj->id]=$unit_fobj->code;}
  $data['units']=$units_array;
  //api_dump($units_array);
  // get templates
  $templates_array=array();
  foreach(cSubsidiariesReportsTemplate::availables() as $template_fobj){$templates_array[$template_fobj->id]=$template_fobj->name;}
  $data['templates']=$templates_array;
  //api_dump($templates_array);
  // get templates entries
  $templates_entries_array=array();
  foreach(array_keys($templates_array) as $template_id){
    foreach((new cSubsidiariesReportsTemplate($template_id))->getEntries() as $entry_fobj){
      //api_dump($entry_fobj);
      $entry=new stdClass();
      $entry->name=$entry_fobj->name;
      $entry->typology=$entry_fobj->typology;
      $entry->unit=$data['units'][$entry_fobj->fkUnit];
      $templates_entries_array[$template_id][$entry_fobj->id]=$entry;
    }
  }
  // keys: template, entry
  $data['templates_entries']=$templates_entries_array;
  //api_dump($templates_entries_array);
  // get reports
  $reports_array=array();
  foreach($subsidiaries_array as $subsidiary_id=>$subsidiary_obj){
    foreach($templates_array as $template_id=>$template_name){
      foreach(cSubsidiariesReportsReport::availables(false,['fkSubsidiary'=>$subsidiary_id,'fkTemplate'=>$template_id,'year'=>$year]) as $report_fobj){
        $reports_array[$subsidiary_id][$template_id][$report_fobj->id]=$subsidiary_obj->name.' - '.$template_name.' '.$year;
      }
    }
  }
  // keys: subsidiaries, template, report
  $data['reports']=$reports_array;
  //api_dump($reports_array);
  // get reports entries
  $reports_entries_array=array();
  foreach(array_keys($subsidiaries_array) as $subsidiary_id){
    foreach(array_keys($templates_array) as $template_id){
      if(is_array($reports_array[$subsidiary_id][$template_id])){
        foreach(array_keys($reports_array[$subsidiary_id][$template_id]) as $report_id){
          foreach((new cSubsidiariesReportsReport($report_id))->getDatas() as $template_entry_id=>$template_entry_datas){
            foreach($template_entry_datas as $period=>$data_fobj){
              //api_dump($data_fobj);
              //$reports_entries_array[$subsidiary_id][$template_id][$report_id][$template_entry_id][$period]=array('budget'=>$data_fobj->budget,'value'=>$data_fobj->value);
              $reports_entries_array[$subsidiary_id][$template_id][$template_entry_id][$period]=(object)array('budget'=>$data_fobj->budget,'actual'=>$data_fobj->value);
            }
          }
        }
      }
    }
  }
  // keys: subsidiaries, template, entry, period
  $data['reports_entries']=$reports_entries_array;
  //api_dump($templates_entries_array);

  // @todo dati anni precedenti  -->  successivamente sar� da calcolare con la media dei dati caricati

  // ex_cdi
  $data['reports_entries'][16][4][59][2020]=(object)array('budget'=>0,'actual'=>727);
  $data['reports_entries'][16][4][59][2021]=(object)array('budget'=>0,'actual'=>815);
  $data['reports_entries'][16][4][59][2022]=(object)array('budget'=>0,'actual'=>769);
  $data['reports_entries'][16][4][59][2023]=(object)array('budget'=>0,'actual'=>683);
  // fra
  $data['reports_entries'][1][2][38][2020]=(object)array('budget'=>0,'actual'=>292);
  $data['reports_entries'][1][2][38][2021]=(object)array('budget'=>0,'actual'=>353);
  $data['reports_entries'][1][2][38][2022]=(object)array('budget'=>0,'actual'=>409);
  $data['reports_entries'][1][2][38][2023]=(object)array('budget'=>0,'actual'=>336);
  // uk
  $data['reports_entries'][2][2][38][2020]=(object)array('budget'=>0,'actual'=>359);
  $data['reports_entries'][2][2][38][2021]=(object)array('budget'=>0,'actual'=>417);
  $data['reports_entries'][2][2][38][2022]=(object)array('budget'=>0,'actual'=>397);
  $data['reports_entries'][2][2][38][2023]=(object)array('budget'=>0,'actual'=>352);
  // ede
  $data['reports_entries'][3][2][38][2020]=(object)array('budget'=>0,'actual'=>1541);
  $data['reports_entries'][3][2][38][2021]=(object)array('budget'=>0,'actual'=>2281);
  $data['reports_entries'][3][2][38][2022]=(object)array('budget'=>0,'actual'=>2428);
  $data['reports_entries'][3][2][38][2023]=(object)array('budget'=>0,'actual'=>1894);
  // met
  $data['reports_entries'][4][2][38][2020]=(object)array('budget'=>0,'actual'=>347);
  $data['reports_entries'][4][2][38][2021]=(object)array('budget'=>0,'actual'=>466);
  $data['reports_entries'][4][2][38][2022]=(object)array('budget'=>0,'actual'=>389);
  $data['reports_entries'][4][2][38][2023]=(object)array('budget'=>0,'actual'=>332);
  // usa
  $data['reports_entries'][5][2][38][2020]=(object)array('budget'=>0,'actual'=>491);
  $data['reports_entries'][5][2][38][2021]=(object)array('budget'=>0,'actual'=>565);
  $data['reports_entries'][5][2][38][2022]=(object)array('budget'=>0,'actual'=>667);
  $data['reports_entries'][5][2][38][2023]=(object)array('budget'=>0,'actual'=>722);
  // kor
  $data['reports_entries'][6][2][38][2020]=(object)array('budget'=>0,'actual'=>207);
  $data['reports_entries'][6][2][38][2021]=(object)array('budget'=>0,'actual'=>210);
  $data['reports_entries'][6][2][38][2022]=(object)array('budget'=>0,'actual'=>168);
  $data['reports_entries'][6][2][38][2023]=(object)array('budget'=>0,'actual'=>104);
  // tur
  $data['reports_entries'][7][2][38][2020]=(object)array('budget'=>0,'actual'=>86);
  $data['reports_entries'][7][2][38][2021]=(object)array('budget'=>0,'actual'=>103);
  $data['reports_entries'][7][2][38][2022]=(object)array('budget'=>0,'actual'=>101);
  $data['reports_entries'][7][2][38][2023]=(object)array('budget'=>0,'actual'=>97);
  // csb
  $data['reports_entries'][8][2][38][2020]=(object)array('budget'=>0,'actual'=>579);
  $data['reports_entries'][8][2][38][2021]=(object)array('budget'=>0,'actual'=>983);
  $data['reports_entries'][8][2][38][2022]=(object)array('budget'=>0,'actual'=>1156);
  $data['reports_entries'][8][2][38][2023]=(object)array('budget'=>0,'actual'=>747);
  // sbs
  $data['reports_entries'][9][2][38][2020]=(object)array('budget'=>0,'actual'=>0);
  $data['reports_entries'][9][2][38][2021]=(object)array('budget'=>0,'actual'=>0);
  $data['reports_entries'][9][2][38][2022]=(object)array('budget'=>0,'actual'=>0);
  $data['reports_entries'][9][2][38][2023]=(object)array('budget'=>0,'actual'=>0);
  // dcsp
  $data['reports_entries'][10][2][38][2020]=(object)array('budget'=>0,'actual'=>863);
  $data['reports_entries'][10][2][38][2021]=(object)array('budget'=>0,'actual'=>905);
  $data['reports_entries'][10][2][38][2022]=(object)array('budget'=>0,'actual'=>709);
  $data['reports_entries'][10][2][38][2023]=(object)array('budget'=>0,'actual'=>526);
  // mes
  $data['reports_entries'][11][2][38][2020]=(object)array('budget'=>0,'actual'=>31);
  $data['reports_entries'][11][2][38][2021]=(object)array('budget'=>0,'actual'=>35);
  $data['reports_entries'][11][2][38][2022]=(object)array('budget'=>0,'actual'=>39);
  $data['reports_entries'][11][2][38][2023]=(object)array('budget'=>0,'actual'=>81);
  // sgp
  $data['reports_entries'][14][2][38][2020]=(object)array('budget'=>0,'actual'=>0);
  $data['reports_entries'][14][2][38][2021]=(object)array('budget'=>0,'actual'=>0);
  $data['reports_entries'][14][2][38][2022]=(object)array('budget'=>0,'actual'=>0);
  $data['reports_entries'][14][2][38][2023]=(object)array('budget'=>0,'actual'=>0);
  // dlp
  $data['reports_entries'][17][2][38][2023]=(object)array('budget'=>0,'actual'=>2684);
  // smp
  $data['reports_entries'][18][2][38][2023]=(object)array('budget'=>0,'actual'=>327);

  include(DIR."modules/subsidiaries_reports/dossier-coge-export.inc.php");

  // debug
  //api_dump($data,'data');

}
